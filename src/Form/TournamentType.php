<?php

namespace App\Form;

use App\Entity\Tournament;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TournamentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label' => "Nombre Torneo: "
            ])
            ->add('type',ChoiceType::class,[
                'choices'  => [
                    Tournament::TYPE_LEAGUE => 1,
                    Tournament::TYPE_LEAGUE_ELIMINATION => 2,
                    Tournament::TYPE_ELIMINATION => 3,
                ],
                'label' => 'Tipo del torneo: '
            ])
            ->add('numberOfGroupRounds', IntegerType::class, [
                'label' => "Cantidad de rondas de grupos: ",
                'mapped' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tournament::class,
        ]);
    }
}
