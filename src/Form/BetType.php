<?php

namespace App\Form;

use App\Entity\Bet;
use App\Entity\Tournament;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;

class BetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('expirationRate', ChoiceType::class, [
                'choices'  => [
                    Bet::EXPIRATION_DATE => 1,
                    Bet::EXPIRATION_GAME => 2
                ]
            ])
            ->add('pointsForExactGames')
            ->add('pointsByResultOnly')
            ->add('pointsPerMissedGame')
            ->add('dateExtraLimitAt')
            ->add('symbolicBet')
            ->add('tournament');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bet::class,
        ]);
    }
}
