<?php

namespace App\Logic\Abstract;

use App\Entity\Round;
use App\Entity\RoundElimination;
use App\Entity\RoundLeague;

/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
abstract class HelperRound
{
    abstract public function hasDateBack(Round $round): bool;

    abstract public function getNumberDateBack(Round $round, int $numberDate): int;

    abstract public function getAmountOfFirstDate(RoundElimination|RoundLeague $round): int;

    abstract public function getAmountOfAllDate(RoundElimination|RoundLeague $round): int;

    abstract public function getNumberOfGroup(Round $round): int;
}
