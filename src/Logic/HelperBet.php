<?php

namespace App\Logic;

use App\Entity\Bet;
use App\Entity\User;

/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class HelperBet
{

    public function IsAParticipantInTheTournament(User $user, Bet $bet, $em): bool
    {

        $participant = $em->getRepository(BettingPositions::class)
            ->findBy(["bet" => $bet, "participant" => $user]);

        return ($participant);
    }
}
