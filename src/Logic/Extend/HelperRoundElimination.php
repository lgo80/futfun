<?php

namespace App\Logic\Extend;

use App\Entity\Round;
use App\Entity\RoundElimination;
use App\Entity\RoundLeague;
use App\Logic\Abstract\HelperRound as LogicHelperRound;
use JetBrains\PhpStorm\Pure;

/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class HelperRoundElimination extends LogicHelperRound
{

    /**
     * @param Round $round
     * @return bool
     */
    #[Pure] public function hasDateBack(Round $round): bool
    {

        return $round->getRoundElimination()->getIsRoundTrip();
    }

    /**
     * @param Round $round
     * @param int $numberDate
     * @return int
     */
    public function getNumberDateBack(Round $round, int $numberDate): int
    {

        return $numberDate + 1;
    }

    /**
     * @param RoundElimination|RoundLeague $round
     * @return int
     */
    #[Pure] public function getAmountOfFirstDate(RoundElimination|RoundLeague $round): int
    {
        return $round->getNumberOfEliminations();
    }

    /**
     * @param RoundElimination|RoundLeague $round
     * @return int
     */
    #[Pure] public function getAmountOfAllDate(RoundElimination|RoundLeague $round): int
    {
        return $this->getAmountOfFirstDate($round);
    }

    /**
     * @param Round $round
     * @return int
     */
    public function getNumberOfGroup(Round $round): int
    {
        return 1;
    }
}
