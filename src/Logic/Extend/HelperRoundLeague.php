<?php

namespace App\Logic\Extend;

use App\Entity\Round;
use App\Entity\RoundElimination;
use App\Entity\RoundLeague;
use App\Logic\Abstract\HelperRound as LogicHelperRound;
use JetBrains\PhpStorm\Pure;

/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class HelperRoundLeague extends LogicHelperRound
{

    /**
     * @param Round $round
     * @return bool
     */
    #[Pure] public function hasDateBack(Round $round): bool
    {

        return $round->getRoundLeague()->getIsRoundTrip();
    }

    /**
     * @param $round
     * @param $numberDate
     * @return int
     */
    #[Pure] public function getNumberDateBack($round, $numberDate): int
    {

        return match ($round->getRoundLeague()->getReturnOfTheGroup()) {
            1 => $this->getAmountOfFirstDate($round->getRoundLeague()) + $numberDate,
            2 => $this->getAmountOfAllDate($round->getRoundLeague()) - ($numberDate - 1),
            default => null,
        };
    }

    /**
     * @param RoundElimination|RoundLeague $round
     * @return int
     */
    #[Pure] public function getAmountOfFirstDate(RoundElimination|RoundLeague $round): int
    {
        $cantidadEquipos = ($round->getAmountOfTeamsByGroups())
            ? $round->getAmountOfTeamsByGroups()
            : $round->getRound()->getNumberOfEquipament();

        return ($cantidadEquipos % 2 == 0) ? $cantidadEquipos - 1 : $cantidadEquipos;
    }

    /**
     * @param RoundElimination|RoundLeague $round
     * @return int
     */
    #[Pure] public function getAmountOfAllDate(RoundElimination|RoundLeague $round): int
    {
        return ($round->getIsRoundTrip())
            ? $this->getAmountOfFirstDate($round) * 2
            : $this->getAmountOfFirstDate($round);
    }

    /**
     * @param Round $round
     * @return int
     */
    #[Pure] public function getNumberOfGroup(Round $round): int
    {
        return $round->getRoundLeague()->getAmountOfGroups();
    }
}
