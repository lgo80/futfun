<?php

namespace App\Controller;

use App\Entity\Bet;
use App\Entity\BetExtra;
use App\Form\BetExtraType;
use App\Repository\BetExtraRepository;
use App\Validation\BetExtraValidation;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/betextra')]
class BetExtraController extends AbstractController
{

    #[Route('/user/new/{bet}', name: 'bet_extra_new', methods: ['GET', 'POST'])]
    public function new(Request            $request, RequestStack $requestStack,
                        BetExtraRepository $betExtraRepository,
                        BetExtraValidation $betExtraValidation,
                        Bet                $bet): Response
    {
        $isError = $betExtraValidation->validateIncome($this->getUser(), $bet, 'new');
        if ($isError)
            return $this->redirectToRoute($isError, [], Response::HTTP_SEE_OTHER);

        $betExtra = new BetExtra();
        return $this->updateBetExtra($betExtra, $bet, $request, $betExtraRepository);
    }

    #[Route('/user/edit/{bet}/{betExtra}', name: 'bet_extra_edit', methods: ['GET', 'POST'])]
    public function edit(Request            $request, RequestStack $requestStack,
                         BetExtraValidation $betExtraValidation,
                         BetExtraRepository $betExtraRepository,
                         Bet                $bet, BetExtra $betExtra = null): Response
    {
        $isError = $betExtraValidation->validateIncome($this->getUser(), $bet, 'edit');
        if ($isError)
            return $this->redirectToRoute($isError, [], Response::HTTP_SEE_OTHER);

        $betExtra = ($betExtra) ? $betExtra : new BetExtra();
        return $this->updateBetExtra($betExtra, $bet, $request, $betExtraRepository);
    }

    #[Route('/user/{betExtra}', name: 'bet_extra_delete', methods: ['POST'])]
    public function delete(Request $request, BetExtra $betExtra): Response
    {
        if ($this->isCsrfTokenValid('delete' . $betExtra->getId(),
            $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($betExtra);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bet_extra_new',
            ['bet' => $betExtra->getBet()->getId()], Response::HTTP_SEE_OTHER);
    }

    /**
     * @param BetExtra $betExtra
     * @param Bet $bet
     * @param Request $request
     * @param BetExtraRepository $betExtraRepository
     * @return Response
     */
    private function updateBetExtra(BetExtra           $betExtra, Bet $bet,
                                    Request            $request,
                                    BetExtraRepository $betExtraRepository): Response
    {

        $betExtra->setBet($bet);
        $form = $this->createForm(BetExtraType::class, $betExtra);
        $form->handleRequest($request);
        $betExtras = $bet->getBetExtras();

        if ($form->isSubmitted() && $form->isValid()) {

            $betExtraRepository->store($betExtra);

            return $this->redirectToRoute('bet_extra_new',
                ['bet' => $bet->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bet_extra/new.html.twig', [
            'bet_extra' => $betExtra,
            'bet_extras' => $betExtras,
            'bet' => $bet,
            'form' => $form,
        ]);
    }
}
