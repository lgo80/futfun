<?php

namespace App\Controller\Admin;

use App\Entity\Round;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class RoundCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Round::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('number'),
            ChoiceField::new('type')
                ->setChoices([
                    Round::TYPE_TOURNAMENT => 1,
                    Round::TYPE_TIEBREAK => 2
                ]),
            AssociationField::new('tournament'),
            IntegerField::new('numberOfEquipament'),
            AssociationField::new('dates')
        ];
    }

}
