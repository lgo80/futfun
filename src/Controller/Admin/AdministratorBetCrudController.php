<?php

namespace App\Controller\Admin;

use App\Entity\AdministratorBet;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class AdministratorBetCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return AdministratorBet::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('role'),
            AssociationField::new('user'),
            AssociationField::new('bet')
        ];
    }


}
