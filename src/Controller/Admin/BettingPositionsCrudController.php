<?php

namespace App\Controller\Admin;

use App\Entity\BettingPositions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class BettingPositionsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BettingPositions::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('bet'),
            AssociationField::new('participant'),
            IntegerField::new('points'),
            IntegerField::new('resultsEmbodiesExacts'),
            IntegerField::new('resultsEmbodiesOnly'),
            IntegerField::new('resultsFailed'),
            IntegerField::new('pointsExtras'),
            BooleanField::new('isActive'),
            IntegerField::new('numberOfDatesWon')
        ];
    }
}
