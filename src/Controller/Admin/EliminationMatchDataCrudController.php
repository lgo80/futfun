<?php

namespace App\Controller\Admin;

use App\Entity\EliminationMatchData;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class EliminationMatchDataCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return EliminationMatchData::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('tournament'),
            AssociationField::new('game'),
            TextField::new('typeLocal'),
            IntegerField::new('LocalMatchNumber'),
            TextField::new('typeAway'),
            IntegerField::new('awayMatchNumber')
        ];
    }
}
