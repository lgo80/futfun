<?php

namespace App\Controller\Admin;

use App\Entity\RoundElimination;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class RoundEliminationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RoundElimination::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ChoiceField::new('beginningOfElimination')
                ->setChoices([
                    RoundElimination::ROUND_OCTAVOS => 1,
                    RoundElimination::ROUND_CUARTOS => 2,
                    RoundElimination::ROUND_SEMI => 3,
                    RoundElimination::ROUND_FINAL => 4,
                    RoundElimination::ROUND_CUSTOMIZED => 5
                ]),
            BooleanField::new('isRoundTrip'),
            BooleanField::new('isAwayGoal'),
            BooleanField::new('isMatchForThirdPlace'),
            BooleanField::new('isRoundTripEnd'),
            AssociationField::new('round'),
            BooleanField::new('hasEnd'),
            IntegerField::new('numberOfEliminations')
        ];
    }
}
