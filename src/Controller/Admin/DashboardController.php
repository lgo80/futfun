<?php

namespace App\Controller\Admin;

use App\Entity\Bet;
use App\Entity\Date;
use App\Entity\Game;
use App\Entity\Team;
use App\Entity\User;
use App\Entity\Group;
use App\Entity\Round;
use App\Entity\BetDate;
use App\Entity\BetGame;
use App\Entity\BetExtra;
use App\Entity\DateGroup;
use App\Entity\Tournament;
use App\Entity\RoundLeague;
use App\Entity\BetExtraUser;
use App\Entity\LeaguePositions;
use App\Entity\BettingPositions;
use App\Entity\RoundElimination;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\Admin\TournamentCrudController;
use App\Entity\AdministratorBet;
use App\Entity\AdministratorTournament;
use App\Entity\EliminationMatchData;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use Symfony\Component\Security\Core\User\UserInterface;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        // redirect to some CRUD controller
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(TournamentCrudController::class)->generateUrl());

        // you can also redirect to different pages depending on the current user
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // you can also render some template to display a proper Dashboard
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Futfun')
            ->setFaviconPath('favicon.svg')
            ->setTranslationDomain('futfun')
            ->setTextDirection('ltr');
        //->renderContentMaximized()
        // ->renderSidebarMinimized();
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Dashboard', 'fa fa-home'),

            MenuItem::section('Tournaments'),
            MenuItem::subMenu('Tournaments', 'fa fa-article')->setSubItems([
                MenuItem::linkToCrud('Tournament', 'fa fa-tags', Tournament::class),
                MenuItem::linkToCrud('Round', 'fa fa-file-text', Round::class),
                MenuItem::linkToCrud('Round elimination', 'fa fa-cubes', RoundElimination::class),
                MenuItem::linkToCrud('Round league', 'fa fa-comment', RoundLeague::class),
                MenuItem::linkToCrud('Position', 'fa fa-comment', LeaguePositions::class),
                MenuItem::linkToCrud('Administrator', 'fa fa-comment', AdministratorTournament::class)
            ]),
            MenuItem::subMenu('Date', 'fa fa-article')->setSubItems([
                MenuItem::linkToCrud('Date', 'fa fa-tags', Date::class),
                MenuItem::linkToCrud('Date group', 'fa fa-file-text', DateGroup::class),
                MenuItem::linkToCrud('Game', 'fa fa-cubes', Game::class),
                MenuItem::linkToCrud('Group', 'fa fa-comment', Group::class),
                MenuItem::linkToCrud('Team', 'fa fa-comment', Team::class),
                MenuItem::linkToCrud('Elimination match data', 'fa fa-comment', EliminationMatchData::class)
            ]),

            MenuItem::section('Bets'),
            MenuItem::subMenu('Bets', 'fa fa-article')->setSubItems([
                MenuItem::linkToCrud('Bet', 'fa fa-tags', Bet::class),
                MenuItem::linkToCrud('Date', 'fa fa-file-text', BetDate::class),
                MenuItem::linkToCrud('Extra', 'fa fa-cubes', BetExtra::class),
                MenuItem::linkToCrud('Extra User', 'fa fa-comment', BetExtraUser::class),
                MenuItem::linkToCrud('Game', 'fa fa-comment', BetGame::class),
                MenuItem::linkToCrud('Position', 'fa fa-comment', BettingPositions::class),
                MenuItem::linkToCrud('Administrator', 'fa fa-comment', AdministratorBet::class)
            ]),

            MenuItem::section('Users'),
            MenuItem::linkToCrud('Users', 'fa fa-user', User::class),
        ];
        // yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        // yield MenuItem::linkToCrud('Tournament', 'fas fa-list', Tournament::class);
        // yield MenuItem::linkToCrud('Round', 'fas fa-list', Round::class);
    }

    // public function configureUserMenu(UserInterface $user): UserMenu
    // {
    //     // Usually it's better to call the parent method because that gives you a
    //     // user menu with some menu items already created ("sign out", "exit impersonation", etc.)
    //     // if you prefer to create the user menu from scratch, use: return UserMenu::new()->...
    //     return parent::configureUserMenu($user)
    //         // use the given $user object to get the user name
    //         ->setName($user->getUsername())
    //         // use this method if you don't want to display the name of the user
    //         ->displayUserName(false)

    //         // you can return an URL with the avatar image
    //         // ->setAvatarUrl('https://...')
    //         // ->setAvatarUrl($user->getProfileImageUrl())
    //         // use this method if you don't want to display the user image
    //         ->displayUserAvatar(false)
    //         // you can also pass an email address to use gravatar's service
    //         // ->setGravatarEmail($user->getMainEmailAddress())

    //         // you can use any type of menu item, except submenus
    //         ->addMenuItems([
    //             MenuItem::linkToRoute('My Profile', 'fa fa-id-card', '...', ['...' => '...']),
    //             MenuItem::linkToRoute('Settings', 'fa fa-user-cog', '...', ['...' => '...']),
    //             MenuItem::section(),
    //             MenuItem::linkToLogout('Logout', 'fa fa-sign-out'),
    //         ]);
    // }
}
