<?php

namespace App\Controller\Admin;

use App\Entity\AdministratorTournament;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class AdministratorTournamentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return AdministratorTournament::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('role'),
            AssociationField::new('user'),
            AssociationField::new('tournament')
        ];
    }
}
