<?php

namespace App\Controller\Admin;

use DateTime;
use App\Entity\Bet;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BetCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Bet::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('tournament'),
            AssociationField::new('champion'),
            TextField::new('name'),
            ChoiceField::new('expirationRate')
                ->setChoices([
                    Bet::EXPIRATION_DATE => 1,
                    Bet::EXPIRATION_GAME => 2
                ]),
            IntegerField::new('pointsForExactGames'),
            IntegerField::new('pointsByResultOnly'),
            IntegerField::new('pointsPerMissedGame'),
            MoneyField::new('symbolicBet')->setCurrency('USD'),
            DateTimeField::new('dateExtraLimitAt')
        ];
    }
}
