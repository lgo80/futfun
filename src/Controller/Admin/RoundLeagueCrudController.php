<?php

namespace App\Controller\Admin;

use App\Entity\RoundLeague;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class RoundLeagueCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RoundLeague::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('amountOfGroups'),
            IntegerField::new('amountOfTeamsByGroups'),
            BooleanField::new('isRoundTrip'),
            ChoiceField::new('returnOfTheGroup')
            ->setChoices([
                RoundLeague::FORM_IGUAL_IDA => 1,
                RoundLeague::FORM_INVERTIDA_IDA => 2,
                RoundLeague::FORM_SIN_DEFINIR => 3
            ]),
            IntegerField::new('classifiedByGroup'),
            IntegerField::new('additionalClassifieds'),
            TextField::new('typeClassifieds'),
            IntegerField::new('additionalDate'),
            IntegerField::new('pointsForGamesWon'),
            IntegerField::new('pointsForTiedGames'),
            IntegerField::new('pointsForGamesLost'),
            AssociationField::new('round')
        ];
    }
}
