<?php

namespace App\Controller\Admin;

use App\Entity\BetExtraUser;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class BetExtraUserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BetExtraUser::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('participante'),
            AssociationField::new('betExtra'),
            TextField::new('detail'),
            BooleanField::new('isCorrect'),
            BooleanField::new('isActive')
        ];
    }
}
