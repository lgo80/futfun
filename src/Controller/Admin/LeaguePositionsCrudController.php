<?php

namespace App\Controller\Admin;

use App\Entity\LeaguePositions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class LeaguePositionsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return LeaguePositions::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('team'),
            AssociationField::new('groupName'),
            AssociationField::new('tournament'),
            IntegerField::new('points'),
            IntegerField::new('goalsScored'),
            IntegerField::new('goalsAgainst'),
            IntegerField::new('goalDifference'),
            IntegerField::new('gamesWon'),
            IntegerField::new('tiedGames'),
            IntegerField::new('gamesLost'),
            AssociationField::new('round')
        ];
    }
}
