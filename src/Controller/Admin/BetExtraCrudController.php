<?php

namespace App\Controller\Admin;

use App\Entity\BetExtra;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class BetExtraCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BetExtra::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('bet'),
            TextField::new('detail'),
            TextField::new('description'),
            BooleanField::new('isComplete'),
            IntegerField::new('value'),
            TextField::new('valueDetail')
        ];
    }
}
