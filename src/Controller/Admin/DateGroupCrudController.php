<?php

namespace App\Controller\Admin;

use App\Entity\DateGroup;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DateGroupCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DateGroup::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('date'),
            AssociationField::new('groupName'),
            AssociationField::new('freeTeam'),
            AssociationField::new('games')
        ];
    }
}
