<?php

namespace App\Controller\Admin;

use App\Entity\Date;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class DateCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Date::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('number'),
            TextField::new('name'),
            DateTimeField::new('startDate'),
            ChoiceField::new('type')
                ->setChoices([
                    Date::TYPE_TOURNAMENT => 1,
                    Date::TYPE_TIEBREAKER => 2
                ]),
            AssociationField::new('tournament'),
            AssociationField::new('dateGroups'),
            AssociationField::new('round')
        ];
    }
}
