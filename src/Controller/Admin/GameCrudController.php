<?php

namespace App\Controller\Admin;

use App\Entity\Game;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class GameCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Game::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('local'),
            AssociationField::new('away'),
            AssociationField::new('dateGroup'),
            IntegerField::new('number'),
            IntegerField::new('localValue'),
            IntegerField::new('awayValue'),
            IntegerField::new('localDefinitionValue'),
            IntegerField::new('awayDefinitionValue'),
            DateTimeField::new('dateMatch_At'),
            ChoiceField::new('type')
                ->setChoices([
                    Game::TYPE_TOURNAMENT => 1,
                    Game::TYPE_TIEBREAKER => 2
                ])
        ];
    }
}
