<?php

namespace App\Controller\Admin;

use App\Entity\Bet;
use App\Entity\BetDate;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;

class BetDateCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BetDate::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('user'),
            AssociationField::new('bet'),
            DateTimeField::new('ModificationDateAt'),
            AssociationField::new('date'),
            BooleanField::new('isDateWin')
        ];
    }
}
