<?php

namespace App\Controller\Admin;

use App\Entity\BetGame;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class BetGameCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BetGame::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('game'),
            AssociationField::new('bet'),
            IntegerField::new('localResult'),
            IntegerField::new('awayResult'),
            IntegerField::new('points')
        ];
    }
}
