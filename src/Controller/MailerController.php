<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Security\Core\User\UserInterface;

class MailerController extends AbstractController
{
    private string $PATHURL = "http://127.0.0.1:8000/";
    private string $ROUTE_USER_CONFIRMATION = "reset-password/confirmuserregistration/";

    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/email', name: 'email')]
    public function sendEmail(MailerInterface $mailer, UserInterface $user, $subject, $token)
    {

        $url = $this->PATHURL . $this->ROUTE_USER_CONFIRMATION . $user->getId() . "/" . $token . "/" . User::ACTIVACION_USUARIO;

        $email = (new TemplatedEmail())
            ->from('leolma1980@gmail.com')
            ->to($user->getEmail())
            ->subject($subject)
            ->htmlTemplate('mailer/index.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'token' => $url
            ]);
        $mailer->send($email);
    }

    #[Route('/avisoEmail', name: 'avisoEmail')]
    public function emailWarning(): Response
    {
        return $this->render('mailer/avisoEmail.html.twig', [
            'controller_name' => 'MailerController'
        ]);
    }

}
