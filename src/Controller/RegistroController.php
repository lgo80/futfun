<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\RegistroService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegistroController extends AbstractController
{
    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/registro', name: 'registro')]
    public function index(Request                     $request,
                          UserPasswordHasherInterface $passwordHasher,
                          MailerInterface             $mailer,
                          RegistroService             $registroService,
                          UserRepository              $userRepository): Response
    {

        if ($this->getUser())
            return $this->redirectToRoute('home');

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository
                ->store($form['password']->getData(), $user, $passwordHasher);
            $registroService->generateForConfirmationUser($user, $mailer);

            return $this->redirectToRoute('avisoEmail');
        }

        return $this->render('registro/index.html.twig', [
            'controller_name' => 'RegistroController',
            'form' => $form->createView()
        ]);
    }
}
