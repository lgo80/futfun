<?php

namespace App\Controller;

use App\Entity\AdministratorBet;
use App\Entity\Bet;
use App\Entity\BetExtra;
use App\Entity\BettingPositions;
use App\Form\BetType;
use App\Repository\BettingPositionsRepository;
use App\Service\BetService;
use App\Validation\BetValidation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/bet')]
class BetController extends AbstractController
{

    #[Route('/user/new/{bet}', name: 'bet_new', requirements: ["id" => "\d+"], methods: ['GET', 'POST'])]
    public function new(Request       $request, RequestStack $requestStack,
                        BetValidation $betValidation,
                        Bet           $bet = null): Response
    {

        $em = $this->getDoctrine()->getManager();
        $bet = ($bet) ? $bet : new Bet();

        $isError = $betValidation->validateIncome(
            $this->getUser(), $bet, 'new');
        if ($isError)
            return $this->redirectToRoute($isError, [], Response::HTTP_SEE_OTHER);

        $betsInActive = $em->getRepository(Bet::class)
            ->findBetInactive($this->getUser());

        $form = $this->createForm(BetType::class, $bet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $bet = $form->getData();
            $em->getRepository(Bet::class)
                ->store($bet);

            $em->getRepository(AdministratorBet::class)
                ->processingStore($this->getUser(), $bet);

            return $this->redirectToRoute('bet_extra_new',
                ['bet' => $bet->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bet/new.html.twig', [
            'bet' => $bet,
            'form' => $form,
            'betsInActive' => $betsInActive
        ]);
    }

    #[Route('/{bet}', name: 'bet_show', methods: ['GET'])]
    public function show(BetValidation $betValidation, Bet $bet = null): Response
    {
        $isError = $betValidation
            ->validateIncome($this->getUser(), $bet, 'show');
        if ($isError)
            return $this->redirectToRoute($isError, [],
                Response::HTTP_SEE_OTHER);

        return $this->render('bet/show.html.twig', [
            'bet' => $bet,
        ]);
    }

    #[Route('/user/confirm/{bet}', name: 'bet_confirm', methods: ['GET', 'POST'])]
    public function confirmBet(Request       $request, RequestStack $requestStack,
                               BetValidation $betValidation, Bet $bet): Response
    {
        $isError = $betValidation->validateIncome(
            $this->getUser(), $bet, 'confirmBet');
        if ($isError)
            return $this->redirectToRoute($isError, [], Response::HTTP_SEE_OTHER);

        return $this->renderForm('bet/confirm.html.twig', [
            'bet' => $bet,
            'bet_extras' => $bet->getBetExtras()
        ]);
    }

    #[Route('/user/store/{bet}', name: 'bet_store', methods: ['GET', 'POST'])]
    public function storeBet(Request $request, RequestStack $requestStack, Bet $bet): Response
    {

        $em = $this->getDoctrine()->getManager();
        $bet->setIsActive(true);
        $em->getRepository(Bet::class)
            ->store($bet);
        $em->getRepository(BetExtra::class)
            ->processingUpdating($bet->getBetExtras());

        return $this->redirectToRoute('home', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/user/inscription/{bet}', name: 'bet_inscription', methods: ['GET', 'POST'])]
    public function inscription(Request                    $request, RequestStack $requestStack,
                                BettingPositionsRepository $bettingPositionsRepository,
                                Bet                        $bet): RedirectResponse
    {
        $bettingPositionsRepository->preparingStore($this->getUser(), $bet);

        return $this->redirectToRoute('bet_extra_user_new',
            ['bet' => $bet->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/user/{bet}', name: 'bet_delete', methods: ['POST'])]
    public function delete(Request $request, Bet $bet): Response
    {
        if ($this->isCsrfTokenValid('delete' . $bet->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($bet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bet_new', [], Response::HTTP_SEE_OTHER);
    }
}
