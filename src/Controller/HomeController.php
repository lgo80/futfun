<?php

namespace App\Controller;

use App\Entity\Bet;
use App\Entity\Date;
use App\Entity\Game;
use App\Entity\Round;
use App\Entity\Tournament;
use App\Repository\BetRepository;
use App\Repository\DateRepository;
use App\Repository\RoundRepository;
use App\Repository\TournamentRepository;
use App\Service\CalendarService;
use App\Service\DateService;
use App\Service\GameService;
use App\Service\RoundService;
use DateTime;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    const NOW = '2022-02-07 15:03:01 America/Argentina/Buenos_Aires';
    const BEFORE = '2022-02-07 15:03:00 America/Argentina/Buenos_Aires';
    const AFTER = '2022-02-07 15:03:02 America/Argentina/Buenos_Aires';

    /**
     * @throws Exception
     */
    #[Route('/', name: 'home')]
    public function index(CalendarService      $calendar, BetRepository $betRepo,
                          TournamentRepository $torneoRepo, GameService $gameService,
                          DateService          $dateService, RoundService $roundService,
                          RoundRepository      $roundRepository, DateRepository $dateRepository): Response
    {

        $round = $roundRepository->find(16);
        $date = $dateRepository->find(34);
        dd($roundService->hasDateBack($round, $date));
        /* $betsInActive = $em->getRepository(Bet::class)
             ->find(1);

         $torneo_id = $em->getRepository(Tournament::class)
             ->find(1);*/
        /*$betsInActive = $betRepo->find(1);
        $torneo_id = $torneoRepo->find(1);
        $lala = $objectManager->getRepository(Date::class)->find(1);

        dd('La apuesta de nombre ' . $betsInActive->getName() . ' - Torneo n° ' . $torneo_id->getName());*/
        /*$dd_prueba_before = new DateTime(self::BEFORE);
        $dd_prueba_now = new DateTime(self::NOW);
        $dd_prueba_after = new DateTime(self::AFTER);

        dump($dd_prueba_now);
        dump($dd_prueba_before);
        dump($dd_prueba_after);
        dump($dd_prueba_before > $dd_prueba_now);
        dump($dd_prueba_after > $dd_prueba_now);*/

        /*$em = $this->getDoctrine()->getManager();
        $torneo = $em->getRepository(Tournament::class)
            ->find(1);
        dd($torneo);*/

//        dd($calendar->getSpecificDate(1, 2, 2022, 1, 1, 23));
        //  dd($calendar->getSumHour(1));
        //dd($calendar->getSumDays(1));

        $em = $this->getDoctrine()->getManager();
        $bets = $em->getRepository(Bet::class)
            ->findBy(["state" => Bet::STATE_INSCRIPTION]);
        /*$game = new Game();
        $game->setDateMatchAt($calendar->getSubtractHour(1));
        $game->setLocalValue(2);
        $game->setAwayValue(1);*/

        /*$date = new Date();
        $round = new Round();
        $date->setRound($round);
        dd($dateService->isAllGroupCompleted($date, []));*/


        //dd($gameService->isDateGameUnExpired($game));
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'bets' => $bets
        ]);
    }
}
