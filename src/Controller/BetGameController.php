<?php

namespace App\Controller;

use App\Entity\Bet;
use App\Entity\BetDate;
use App\Entity\BetGame;
use App\Entity\Date;
use App\Entity\User;
use App\Form\BetGameType;
use App\Repository\BetDateRepository;
use App\Repository\BetGameRepository;
use App\Service\BetGameService;
use App\Validation\BetGameValidation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route('/betgame')]
class BetGameController extends AbstractController
{
    #[Route('/index/{bet}/{numberDate}/{user}', name: 'bet_game_index', methods: ['GET'])]
    public function index(BetGameRepository $betGameRepository,
                          BetDateRepository $betDateRepository,
                          BetGameValidation $betGameValidation,
                          BetGameService    $servBetGame,
                          Bet               $bet = null, int $numberDate = 0,
                          UserInterface     $user = null): Response
    {

        if ($betGameValidation->validateValue($bet))
            return $this->redirectToRoute('home', [], Response::HTTP_SEE_OTHER);

        $userLogin = ($user) ? $user : $this->getUser();
        if ($betGameValidation->validateValue($userLogin))
            return $this->redirectToRoute('home', [], Response::HTTP_SEE_OTHER);

        $date = $servBetGame->generateCurrentDate($bet, $numberDate);
        if ($betGameValidation->validateValue($date))
            return $this->redirectToRoute('home', [], Response::HTTP_SEE_OTHER);

        $betDate = $betDateRepository
            ->findOneBy(['bet' => $bet, 'user' => $userLogin, 'date' => $date]);
        if ($betGameValidation->validateValue($betDate))
            return $this->redirectToRoute('home', [], Response::HTTP_SEE_OTHER);

        $betGames = $betGameRepository->findBy(['betDate' => $betDate]);

        return $this->render('bet_game/index.html.twig', [
            'betGames' => $betGames,
            'bet' => $bet,
            'betDate' => $betDate,
            'date' => $date
        ]);
    }

    #[Route('/user/new/{bet}/{numberDate}', name: 'bet_game_new', methods: ['GET', 'POST'])]
    public function new(Request           $request, BetGameRepository $betGamesRepo,
                        BetGameRepository $betGameRepository,
                        BetGameValidation $betGameValidation,
                        BetGameService    $servBetGame,
                        Bet               $bet = null,
                        int               $numberDate = 0): Response
    {

        $userLogin = $this->getUser();
        $isError = $betGameValidation->validateIncome($userLogin, $bet, 'new');
        if ($isError)
            return $this->redirectToRoute($isError, [], Response::HTTP_SEE_OTHER);

        $date = $servBetGame->generateCurrentDate($bet, $numberDate);
        if ($betGameValidation->validateValue($date))
            return $this->redirectToRoute('home', [], Response::HTTP_SEE_OTHER);

        $betDate = $servBetGame->generateBetDate($bet, $userLogin, $date);

        $game = $servBetGame->determineCurrentParty($date, $betDate);
        if ($betGameValidation->validateValue($game))
            return $this->redirectToRoute('bet_game_index',
                ['bet' => $bet->getId(), 'numberDate' => $date->getNumber()]
                , Response::HTTP_SEE_OTHER);

        $betGames = $betGamesRepo->findBy(['betDate' => $betDate]);

        $betGame = $servBetGame->generateNewBetGame($betDate, $game);
        $form = $this->createForm(BetGameType::class, $betGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $betGameRepository
                ->store($betGame);

            return $this->redirectToRoute('bet_game_new',
                ['bet' => $bet->getId(), 'numberDate' => $numberDate],
                Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bet_game/new.html.twig', [
            'bet_game' => $betGame,
            'form' => $form,
            'bet' => $bet,
            'betGames' => $betGames,
            'date' => $date
        ]);
    }

    #[Route('/user/edit/{betGame}', name: 'bet_game_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, BetGameValidation $betGameValidation,
                         BetGame $betGame = null): Response
    {

        $isError = $betGameValidation
            ->validateIncome($this->getUser(), $betGame, 'edit');
        if ($isError)
            return $this->redirectToRoute($isError, [], Response::HTTP_SEE_OTHER);

        $form = $this->createForm(BetGameType::class, $betGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute(
                'bet_game_index',
                [
                    'bet' => $betGame->getBetDate()->getBet()->getId(),
                    'numberDate' => $betGame->getBetDate()->getDate()->getNumber()
                ],
                Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bet_game/edit.html.twig', [
            'betGame' => $betGame,
            'form' => $form,
        ]);
    }
}
