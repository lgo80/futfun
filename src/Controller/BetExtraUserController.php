<?php

namespace App\Controller;

use App\Entity\Bet;
use App\Entity\BetExtraUser;
use App\Entity\BettingPositions;
use App\Entity\User;
use App\Form\BetExtraUserType;
use App\Repository\BetExtraUserRepository;
use App\Repository\BettingPositionsRepository;
use App\Service\BetExtraUserService;
use App\Validation\BetExtraUserValidation;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route('/betextrauser')]
class BetExtraUserController extends AbstractController
{
    #[Route('/{bet}/{user}', name: 'bet_extra_user_index', requirements: ["bet" => "\d+"], methods: ['GET'])]
    public function index(BetExtraUserRepository     $betExtraUserRepository,
                          BettingPositionsRepository $bettingPositionsRepository,
                          Bet                        $bet, UserInterface $user = null): Response
    {

        $user = ($user) ? $user : $this->getUser();

        $betExtraUser = $betExtraUserRepository
            ->findByIsExtraComplete($user, $bet, true);

        $participants = $bettingPositionsRepository
            ->findBy(['bet' => $bet]);

        return $this->render('bet_extra_user/index.html.twig', [
            'bet_extra_users' => $betExtraUser,
            'bet' => $bet,
            'participants' => $participants
        ]);
    }

    /**
     * @throws NonUniqueResultException
     */
    #[Route('/user/new/{bet}', name: 'bet_extra_user_new', methods: ['GET', 'POST'])]
    public function new(Request                $request,
                        BetExtraUserRepository $betExtraUserRepository,
                        BetExtraUserValidation $betExtraUserValidation,
                        BetExtraUserService    $betExtraUserService,
                        Bet                    $bet): Response
    {

        $user = $this->getUser();
        $isError = $betExtraUserValidation
            ->validateIncome($this->getUser(), $bet, 'new');
        if ($isError)
            return $this->redirectToRoute($isError, [], Response::HTTP_SEE_OTHER);

        $betExtra = $betExtraUserService
            ->getBetExtraWithoutComplete($user, $bet);
        if (!$betExtra)
            return $this->redirectToRoute('bet_extra_user_index',
                ['bet' => $bet->getId()], Response::HTTP_SEE_OTHER);

        $betExtraUser = $betExtraUserService
            ->generateBetExtraNew($user, $betExtra);
        $betExtrasUser = $betExtraUserRepository
            ->findByIsExtraComplete($user, $bet, true);

        $form = $this->createForm(BetExtraUserType::class, $betExtraUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $betExtraUserService
                ->saveExtraUser($user, $bet, $betExtraUser);

            return $this->redirectToRoute('bet_extra_user_new', ['bet' => $bet->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bet_extra_user/new.html.twig', [
            'bet_extra_user' => $betExtraUser,
            'form' => $form,
            'bet' => $bet,
            'betExtrasUser' => $betExtrasUser
        ]);
    }

    #[Route('/{betExtraUser}', name: 'bet_extra_user_show', methods: ['GET'])]
    public function show(BetExtraUser $betExtraUser): Response
    {
        return $this->render('bet_extra_user/show.html.twig', [
            'bet_extra_user' => $betExtraUser,
        ]);
    }

    #[Route('/user/edit/{betExtraUser}', name: 'bet_extra_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, BetExtraUser $betExtraUser): Response
    {
        $form = $this->createForm(BetExtraUserType::class, $betExtraUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bet_extra_user_index',
                ['bet' => $betExtraUser->getBetExtra()->getBet()->getId()],
                Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bet_extra_user/edit.html.twig', [
            'bet_extra_user' => $betExtraUser,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'bet_extra_user_delete', methods: ['POST'])]
    public function delete(Request $request, BetExtraUser $betExtraUser): Response
    {
        if ($this->isCsrfTokenValid('delete' . $betExtraUser->getId(),
            $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($betExtraUser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bet_extra_user_index',
            ['bet' => $betExtraUser->getBetExtra()->getBet()->getId()],
            Response::HTTP_SEE_OTHER);
    }
}
