<?php

namespace App\Controller;

use App\Entity\Bet;
use App\Entity\BettingPositions;
use App\Entity\Date;
use App\Repository\BettingPositionsRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\BettingPositionsService;

#[Route('/bettingPositions')]
class BettingPositionsController extends AbstractController
{
    /**
     * @throws NonUniqueResultException
     */
    #[Route('/{bet}/{date}', name: 'betting_positions_index')]
    public function index(BettingPositionsRepository $betExtraUserRepository,
                          BettingPositionsService    $bettingPositionsService,
                          Bet                        $bet, ?Date $date = null): Response
    {
        if ($date) $bettingPositionsService
            ->getPositionsDateShow($bet, $date);

        return $this->render('betting_positions/index.html.twig', [
            'controller_name' => 'BettingPositionsController',
            'bet' => $bet,
            'bettingPositions' => $bet->getBettingPositions()
        ]);
    }
}
