<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetGame;
use Symfony\Component\Security\Core\User\UserInterface;

class BetExtraUserValidation extends AbstractValidation
{

    protected BettingPositionsValidation $bettingPositionsValidation;

    public function __construct(
        BettingPositionsValidation $bettingPositionsValidation)
    {
        $this->bettingPositionsValidation = $bettingPositionsValidation;
    }

    public function validateIncome(UserInterface $user,
                                   Bet|BetGame   $bet, string $method): ?string
    {
        return ($method == 'new') ?
            $this->validarNew($user, $bet) : null;
    }

    private function validarNew(UserInterface $user, Bet $bet): ?string
    {
        return (!$bet->getIsActive() ||
            !$this->bettingPositionsValidation
                ->IsAParticipantInTheTournament($user, $bet))
            ? 'home' : null;
    }

}