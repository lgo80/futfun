<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetGame;
use App\Repository\AdministratorBetRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class AdministratorBetValidation extends AbstractValidation
{

    protected AdministratorBetRepository $administratorBetRepository;

    public function __construct(
        AdministratorBetRepository $administratorBetRepository)
    {
        $this->administratorBetRepository = $administratorBetRepository;
    }

    public function validateIncome(UserInterface $user, Bet|BetGame $bet, string $method)
    {
        // TODO: Implement validateIncome() method.
    }

    public function validateAdministrator(UserInterface $user, Bet $bet): ?bool
    {
        $administrator = $this->administratorBetRepository
            ->findBy(["user" => $user, "bet" => $bet]);
        return ($administrator != null);
    }
}