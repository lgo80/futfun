<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetGame;
use App\Entity\Round;
use Doctrine\DBAL\Exception;
use Symfony\Component\Security\Core\User\UserInterface;

class RoundValidation extends AbstractValidation
{

    public function validateIncome(UserInterface $user, Bet|BetGame $bet, string $method)
    {
    }

    /**
     * @throws Exception
     */
    public function isValidateRoundNeed(Round $round): void
    {
        if (!$round->getRoundLeague() && !$round->getRoundElimination())
            throw new Exception('No se ingreso ni ronda de liga ni ronda de eliminacion');
    }

    /**
     * @param Round $round
     * @return bool
     */
    public function hasRoundLeague(Round $round): bool
    {
        return (bool)$round->getRoundLeague();
    }

}