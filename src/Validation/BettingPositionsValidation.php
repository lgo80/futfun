<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetGame;
use App\Repository\BettingPositionsRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class BettingPositionsValidation extends AbstractValidation
{

    protected BettingPositionsRepository $bettingPositionsRepository;

    public function __construct(
        BettingPositionsRepository $bettingPositionsRepository)
    {
        $this->bettingPositionsRepository = $bettingPositionsRepository;
    }

    public function validateIncome(UserInterface $user, Bet|BetGame $bet, string $method)
    {
        return null;
    }

    public function IsAParticipantInTheTournament(UserInterface $user, Bet $bet): bool
    {
        $participant = $this->bettingPositionsRepository
            ->findBy(["bet" => $bet, "participant" => $user]);
        return (count($participant) > 0);
    }

}