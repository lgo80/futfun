<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetGame;
use Symfony\Component\Security\Core\User\UserInterface;

class BetExtraValidation extends AbstractValidation
{

    protected AdministratorBetValidation $administratorBetValidation;

    public function __construct(
        AdministratorBetValidation $administratorBetValidation)
    {
        $this->administratorBetValidation = $administratorBetValidation;
    }

    public function validateIncome(UserInterface $user, Bet|BetGame $bet, string $method): ?string
    {
        return match ($method) {
            'new', 'edit' => $this->validateNew($user, $bet),
            default => null,
        };
    }

    public function validateNew(UserInterface $user, Bet $bet): ?string
    {

        if (!$this->administratorBetValidation
            ->validateAdministrator($user, $bet))
            return 'bet_new';

        return ($bet->getIsActive()) ? 'home' : null;
    }
}