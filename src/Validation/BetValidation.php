<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetGame;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\User\UserInterface;

class BetValidation extends AbstractValidation
{

    protected AdministratorBetValidation $administratorBetValidation;

    public function __construct(
        AdministratorBetValidation $administratorBetValidation)
    {
        $this->administratorBetValidation = $administratorBetValidation;
    }

    public function validateIncome(UserInterface $user, Bet|BetGame $bet, string $method): ?bool
    {
        return match ($method) {
            'new' => $this->validarNew($bet),
            'confirmBet' => $this->validarConfirmBet($user, $bet),
            'show' => $this->validarShow($bet),
            default => null,
        };
    }

    /**
     * @param Bet $bet
     * @return string|null
     */
    private function validarNew(Bet $bet): ?string
    {
        return ($bet->getIsActive()) ? "home" : null;
    }

    /**
     * @param UserInterface $user
     * @param Bet $bet
     * @return string|null
     */
    private function validarConfirmBet(
        UserInterface $user, Bet $bet): ?string
    {
        if (!$this->administratorBetValidation->validateAdministrator($user, $bet))
            return 'bet_new';
        return ($bet->getIsActive()) ? 'home' : null;
    }

    /**
     * @param Bet $bet
     * @return string|null
     */
    private function validarShow(Bet $bet): ?string
    {
        return ($bet == null) ? "home" : null;
    }
}