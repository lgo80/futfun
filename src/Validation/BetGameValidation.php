<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetDate;
use App\Entity\BetGame;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\User\UserInterface;

class BetGameValidation extends AbstractValidation
{

    protected AdministratorBetValidation $administratorBetValidation;
    protected BetDateValidation $betDateValidation;

    public function __construct(
        AdministratorBetValidation $administratorBetValidation,
        BetDateValidation          $betDateValidation)
    {
        $this->administratorBetValidation = $administratorBetValidation;
        $this->betDateValidation = $betDateValidation;
    }

    /**
     * @param UserInterface $user
     * @param Bet|BetGame $bet
     * @param string $method
     * @return string|null
     */
    public function validateIncome(UserInterface $user, Bet|BetGame $bet,
                                   string        $method): ?string
    {
        return match ($method) {
            'new' => $this->validateNew($user, $bet),
            'edit' => $this->validateEdit($user, $bet),
            default => null,
        };
    }

    /**
     * @param UserInterface $user
     * @param Bet|null $bet
     * @return string|null
     */
    public function validateNew(UserInterface $user,
                                Bet           $bet = null): ?string
    {
        if (!$bet || !$bet->getIsActive())
            return 'home';

        return (!$this->administratorBetValidation
            ->validateAdministrator($user, $bet))
            ? 'bet_new' : null;
    }

    /**
     * @param UserInterface $user
     * @param BetGame|null $betGame
     * @return string|null
     */
    public function validateEdit(UserInterface $user,
                                 BetGame       $betGame = null): ?string
    {
        return (!$betGame ||
            !$this->betDateValidation->validateUser(
                $betGame->getBetDate()->getUser(), $user))
            ? 'home' : null;
    }

    /**
     * @param UserInterface $user
     * @param BetDate $betDate
     * @return string|null
     */
    public function validateBetDateUser(UserInterface $user,
                                        BetDate       $betDate): ?string
    {
        return ($user !== $betDate->getUser())
            ? "home" : null;
    }
}