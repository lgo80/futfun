<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetGame;
use App\Entity\Date;
use Doctrine\DBAL\Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\User\UserInterface;

class DateValidation extends AbstractValidation
{

    protected RoundValidation $roundValidation;

    #[Pure] public function __construct(
        RoundValidation $roundValidation)
    {
        $this->roundValidation = $roundValidation;
    }

    public function validateIncome(UserInterface $user, Bet|BetGame $bet, string $method)
    {
        // TODO: Implement validateIncome() method.
    }

    /**
     * @throws Exception
     */
    public function isValidateRound(Date $date)
    {
        if (!$date->getRound())
            throw new Exception('No se ingreso ninguna ronda');
        $this->roundValidation->isValidateRoundNeed($date->getRound());
    }
}