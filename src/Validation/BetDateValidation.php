<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetGame;
use Symfony\Component\Security\Core\User\UserInterface;

class BetDateValidation extends AbstractValidation
{

    public function validateIncome(UserInterface $user, Bet|BetGame $bet, string $method)
    {
        // TODO: Implement validateIncome() method.
    }

    /**
     * @param UserInterface $userBet
     * @param UserInterface $userLogin
     * @return bool
     */
    public function validateUser(
        UserInterface $userBet, UserInterface $userLogin): bool
    {
        return $userBet === $userLogin;
    }
}