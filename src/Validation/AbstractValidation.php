<?php

namespace App\Validation;

use App\Entity\Bet;
use App\Entity\BetGame;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractValidation
{

    public abstract function validateIncome(UserInterface $user, Bet|BetGame $bet, string $method);

    /**
     * @param $value
     * @return bool|null
     */
    public function validateValue($value): ?bool
    {
        return !$value;
    }

}