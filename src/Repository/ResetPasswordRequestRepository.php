<?php

namespace App\Repository;

use App\Entity\ResetPasswordRequest;
use App\Entity\User;
use App\Service\FunctionUtilsService;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordRequestInterface;
use SymfonyCasts\Bundle\ResetPassword\Persistence\Repository\ResetPasswordRequestRepositoryTrait;
use SymfonyCasts\Bundle\ResetPassword\Persistence\ResetPasswordRequestRepositoryInterface;

/**
 * @method ResetPasswordRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResetPasswordRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResetPasswordRequest[]    findAll()
 * @method ResetPasswordRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResetPasswordRequestRepository extends ServiceEntityRepository implements ResetPasswordRequestRepositoryInterface
{
    use ResetPasswordRequestRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResetPasswordRequest::class);
    }

    public function createResetPasswordRequest(object $user, DateTimeInterface $expiresAt,
                                               string $selector,
                                               string $hashedToken): ResetPasswordRequestInterface
    {
        return new ResetPasswordRequest($user, $expiresAt, $selector, $hashedToken);
    }

    public function store(
        UserInterface $user
    ): string
    {

        $resetPassword = $this->generarRecupero($user);

        $this->getEntityManager()->persist($resetPassword);
        $this->getEntityManager()->flush();

        return $resetPassword->getHashedToken();
    }

    public function destroy(
        ResetPasswordRequest $resetPassword
    )
    {

        $this->getEntityManager()->remove($resetPassword);
        $this->getEntityManager()->flush();
    }

    public function generarRecupero(UserInterface $user): ResetPasswordRequestInterface
    {

        $serviceUtil = new FunctionUtilsService();

        $salt = $serviceUtil->generateRandomString(5);
        $token = md5($salt . $user->getId());
        $fecha = new \DateTimeImmutable();
        $fecha = $fecha->add(new \DateInterval('PT30M'));

        return $this->createResetPasswordRequest($user, $fecha, $salt, $token);
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findByRecoverUser(User $user, $token, $dateExpire): array
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.user = :id')
            ->andWhere('r.hashedToken = :token')
            ->andWhere('r.expiresAt >= :fecha_expire')
            ->setParameter('id', $user)
            ->setParameter('token', $token)
            ->setParameter('fecha_expire', $dateExpire)
            ->getQuery()
            ->getResult();
    }
}
