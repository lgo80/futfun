<?php

namespace App\Repository;

use App\Entity\LeaguePositions;
use App\Entity\Team;
use App\Entity\Tournament;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LeaguePositions|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeaguePositions|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeaguePositions[]    findAll()
 * @method LeaguePositions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeaguePositionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeaguePositions::class);
    }

    /**
     * @return null
     */
    public function store(LeaguePositions $leaguePositions)
    {
        $em = $this->getEntityManager();
        $em->persist($leaguePositions);
        $em->flush();
    }

    // /**
    //  * @return LeaguePositions[] Returns an array of LeaguePositions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findOneBySomeField(Team $team, Tournament $tournament): ?LeaguePositions
    {
        return $this->createQueryBuilder('lp')
            ->andWhere('lp.team = :team')
            ->andWhere('lp.tournament = :tournament')
            ->setParameter('team', $team)
            ->setParameter('tournament', $tournament)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
