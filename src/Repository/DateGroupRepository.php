<?php

namespace App\Repository;

use App\Entity\DateGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DateGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method DateGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method DateGroup[]    findAll()
 * @method DateGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DateGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DateGroup::class);
    }

    /**
     * @return null
     */
    public function store(DateGroup $dateGroup)
    {
        $em = $this->getEntityManager();
        $em->persist($dateGroup);
        $em->flush();
    }

    // /**
    //  * @return DateGroup[] Returns an array of DateGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySomeField($date, $groupName): ?DateGroup
    {
        $qb = $this->createQueryBuilder('dg')
            ->andWhere('dg.date = :date')
            ->setParameter('date', $date);

        if ($groupName) {
            $qb
                ->andWhere('dg.groupName = :groupName')
                ->setParameter('groupName', $groupName);
        }
        return $qb->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
