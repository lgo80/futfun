<?php

namespace App\Repository;

use App\Entity\AdministratorTournament;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdministratorTournament|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdministratorTournament|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdministratorTournament[]    findAll()
 * @method AdministratorTournament[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdministratorTournamentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdministratorTournament::class);
    }

    // /**
    //  * @return AdministratorTournament[] Returns an array of AdministratorTournament objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdministratorTournament
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
