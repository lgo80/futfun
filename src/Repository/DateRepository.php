<?php

namespace App\Repository;

use App\Entity\Date;
use App\Entity\Round;
use App\Entity\Tournament;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use \Datetime;

/**
 * @method Date|null find($id, $lockMode = null, $lockVersion = null)
 * @method Date|null findOneBy(array $criteria, array $orderBy = null)
 * @method Date[]    findAll()
 * @method Date[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Date::class);
    }

    /**
     * @return null
     */
    public function store(Date $date)
    {
        $em = $this->getEntityManager();
        $em->persist($date);
        $em->flush();
    }

    // /**
    //  * @return Date[] Returns an array of Date objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findOneBySomeField($number, Tournament $tournament, Round $round): ?Date
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.number = :number')
            ->andWhere('d.tournament = :tournament')
            ->andWhere('d.round = :round')
            ->setParameter('number', $number)
            ->setParameter('tournament', $tournament)
            ->setParameter('round', $round)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByNextDateActive(Tournament $tournament): ?Date
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.tournament = :tournament')
            ->andWhere('d.startDate > :dateToday')
            ->orderBy('d.startDate', 'ASC')
            ->setParameter('tournament', $tournament)
            ->setParameter('dateToday', new DateTime())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
