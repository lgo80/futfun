<?php

namespace App\Repository;

use App\Entity\Bet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Bet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bet[]    findAll()
 * @method Bet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bet::class);
    }

    /**
     * @return null
     */
    public function store(Bet $bet)
    {
        $em = $this->getEntityManager();
        $em->persist($bet);
        $em->flush();
    }

    public function activeBet(Bet $bet)
    {
        $entityManager = $this->getEntityManager();
        $bet = $entityManager->getRepository(Bet::class)->find($bet->getId());

        if (!$bet) {
            throw $this->createNotFoundException(
                'No product found for id '
            );
        }

        $bet->setIsActive(true);
        $entityManager->flush();
    }

    /**
     * @return Bet[] Returns an array of Bet objects
     */
    public function findBetInactive($user)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT b, ab
            FROM App\Entity\Bet b
            INNER JOIN b.administratorBets ab
            WHERE b.isActive = :isActive AND ab.user = :user'
        )
            ->setParameter('isActive', false)
            ->setParameter('user', $user);

        return $query->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Bet
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
