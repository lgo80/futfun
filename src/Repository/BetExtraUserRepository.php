<?php

namespace App\Repository;

use App\Entity\Bet;
use App\Entity\BetExtra;
use App\Entity\BetExtraUser;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method BetExtraUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method BetExtraUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method BetExtraUser[]    findAll()
 * @method BetExtraUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetExtraUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BetExtraUser::class);
    }

    /**
     * @param BetExtraUser $betExtraUser
     * @return void
     */
    public function store(BetExtraUser $betExtraUser): void
    {
        $em = $this->getEntityManager();
        $em->persist($betExtraUser);
        $em->flush();
    }

    /**
     * @param BetExtra $betExtra
     * @param UserInterface $user
     * @return void
     */
    public function preparingStore(BetExtra $betExtra, UserInterface $user): void
    {
        $betExtraUserNew = new BetExtraUser();
        $betExtraUserNew->setParticipante($user);
        $betExtraUserNew->setBetExtra($betExtra);
        $this->store($betExtraUserNew);
    }

    /**
     * @return null
     * @throws NonUniqueResultException
     */
    public function updatingExtrasWithoutComplete($betExtraQueNoSeCompleta, UserInterface $user): void
    {

        /*$betExtraQueNoSeCompleta = array_filter($betExtras, function ($extra) {
            return !$extra->getIsComplete();
        });*/

        foreach ($betExtraQueNoSeCompleta as $betExtra) {

            $betExtraUser = $this->findByExtraCompleteForParticipant($user, $betExtra);

            if (!$betExtraUser)
                $this->preparingStore($betExtra, $user);
        }

    }

    /**
     * @return BetExtraUser[] Returns an array of BetExtraUser objects
     */
    public function findByIsExtraComplete(UserInterface $user, Bet $bet, bool $isComplete): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT beu, be
            FROM App\Entity\BetExtraUser beu
            INNER JOIN beu.betExtra be
            WHERE be.bet = :bet 
            AND beu.participante = :user
            AND be.isComplete = :isComplete'
        )
            ->setParameter('bet', $bet)
            ->setParameter('user', $user)
            ->setParameter('isComplete', $isComplete);

        return $query->getResult();
    }

    /**
     * @return BetExtraUser Returns an array of BetExtraUser objects
     * @throws NonUniqueResultException
     */
    public function findByExtraCompleteForParticipant(UserInterface $participant, BetExtra $betExtra): BetExtraUser
    {
        return $this->createQueryBuilder('beu')
            ->andWhere('beu.participante = :participante')
            ->andWhere('beu.betExtra = :betExtra')
            ->setParameter('participante', $participant)
            ->setParameter('betExtra', $betExtra)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
