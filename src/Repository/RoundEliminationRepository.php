<?php

namespace App\Repository;

use App\Entity\RoundElimination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RoundElimination|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoundElimination|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoundElimination[]    findAll()
 * @method RoundElimination[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoundEliminationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoundElimination::class);
    }

    // /**
    //  * @return RoundElimination[] Returns an array of RoundElimination objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoundElimination
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
