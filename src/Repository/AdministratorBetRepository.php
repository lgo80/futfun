<?php

namespace App\Repository;

use App\Entity\AdministratorBet;
use App\Entity\Bet;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdministratorBet|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdministratorBet|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdministratorBet[]    findAll()
 * @method AdministratorBet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdministratorBetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdministratorBet::class);
    }

    /**
     * @return null
     */
    public function store(AdministratorBet $administratorBet)
    {
        $em = $this->getEntityManager();
        $em->persist($administratorBet);
        $em->flush();
    }

    public function processingStore(User $user, Bet $bet)
    {

        $administratorBet = new AdministratorBet();
        $administratorBet->setBet($bet);
        $administratorBet->setUser($user);
        $administratorBet->setRole('ROLE_ADMIN');
        $this->store($administratorBet);
    }

    // public function findByAdministrador(User $user, Bet $bet): ?AdministratorBet
    // {
    //     return $this->createQueryBuilder('a')
    //         ->andWhere('a.user = :user')
    //         ->andWhere('a.bet = :bet')
    //         ->setParameter('user', $user)
    //         ->setParameter('bet', $bet)
    //         ->setMaxResults(10)
    //         ->getQuery()
    //         ->getResult();
    // }
}
