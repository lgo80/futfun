<?php

namespace App\Repository;

use App\Entity\DataTiebreaker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DataTiebreaker|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataTiebreaker|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataTiebreaker[]    findAll()
 * @method DataTiebreaker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataTiebreakerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataTiebreaker::class);
    }

    // /**
    //  * @return DataTiebreaker[] Returns an array of DataTiebreaker objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataTiebreaker
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
