<?php

namespace App\Repository;

use App\Entity\Bet;
use App\Entity\BetExtra;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method BetExtra|null find($id, $lockMode = null, $lockVersion = null)
 * @method BetExtra|null findOneBy(array $criteria, array $orderBy = null)
 * @method BetExtra[]    findAll()
 * @method BetExtra[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetExtraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BetExtra::class);
    }

    /**
     * @return null
     */
    public function store(BetExtra $betExtra)
    {
        $em = $this->getEntityManager();
        $em->persist($betExtra);
        $em->flush();
    }

    public function processingStore($betExtras, Bet $bet)
    {
        foreach ($betExtras as $betExtra) {

            $betExtra->setBet($bet);
            $this->store($betExtra);
        }
    }

    public function processingUpdating($betExtras)
    {
        foreach ($betExtras as $betExtra) {

            $betExtra->setIsActive(true);
            $this->store($betExtra);
        }
    }

    /**
     * @return BetExtra[] Returns an array of BetExtra objects
     */
    public function findByExampleField(Bet $bet)
    {
        return $this->createQueryBuilder('be')
            ->andWhere('be.bet = :bet')
            ->setParameter('bet', $bet)
            ->orderBy('be.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?BetExtra
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
