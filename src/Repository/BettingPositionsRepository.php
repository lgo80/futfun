<?php

namespace App\Repository;

use App\Entity\Bet;
use App\Entity\BettingPositions;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method BettingPositions|null find($id, $lockMode = null, $lockVersion = null)
 * @method BettingPositions|null findOneBy(array $criteria, array $orderBy = null)
 * @method BettingPositions[]    findAll()
 * @method BettingPositions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BettingPositionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BettingPositions::class);
    }

    /**
     * @return null
     */
    public function store(BettingPositions $bettingPosition)
    {
        $em = $this->getEntityManager();
        $em->persist($bettingPosition);
        $em->flush();
    }

    /**
     * @return null
     */
    public function preparingStore(UserInterface $user, Bet $bet)
    {
        $bettingPosition = new BettingPositions();
        $bettingPosition->setParticipant($user);
        $bettingPosition->setBet($bet);
        $this->store($bettingPosition);
    }

    // /**
    //  * @return BettingPositions[] Returns an array of BettingPositions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BettingPositions
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
