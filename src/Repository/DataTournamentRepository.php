<?php

namespace App\Repository;

use App\Entity\DataTournament;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DataTournament|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataTournament|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataTournament[]    findAll()
 * @method DataTournament[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataTournamentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataTournament::class);
    }

    // /**
    //  * @return DataTournament[] Returns an array of DataTournament objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataTournament
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
