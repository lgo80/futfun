<?php

namespace App\Repository;

use App\Entity\BetGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BetGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method BetGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method BetGame[]    findAll()
 * @method BetGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetGameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BetGame::class);
    }

     /**
     * @return null
     */
    public function store(BetGame $betGame)
    {
        $em = $this->getEntityManager();
        $em->persist($betGame);
        $em->flush();
    }

    // /**
    //  * @return BetGame[] Returns an array of BetGame objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BetGame
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
