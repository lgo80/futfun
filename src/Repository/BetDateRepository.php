<?php

namespace App\Repository;

use App\Entity\Bet;
use App\Entity\BetDate;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method BetDate|null find($id, $lockMode = null, $lockVersion = null)
 * @method BetDate|null findOneBy(array $criteria, array $orderBy = null)
 * @method BetDate[]    findAll()
 * @method BetDate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetDateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BetDate::class);
    }

    /**
     * @param BetDate $betDate
     * @return void
     */
    public function store(BetDate $betDate): void
    {
        $em = $this->getEntityManager();
        $em->persist($betDate);
        $em->flush();
    }

    // /**
    //  * @return BetDateFixtures[] Returns an array of BetDateFixtures objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @throws NonUniqueResultException
     */
    public function findByDateActive(Bet $bet, $dateToday, UserInterface $user): ?BetDate
    {

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT bd
            FROM App\Entity\BetDate bd
            INNER JOIN bd.date d
            WHERE bd.bet = :bet AND d.startDate <= :dateToday AND bd.user = :user
            ORDER BY d.startDate DESC'
        )
            ->setParameter('bet', $bet)
            ->setParameter('dateToday', $dateToday)
            ->setParameter('user', $user)
            ->setMaxResults(1);
        return $query->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByDateEntered(Bet $bet, $numberDate): ?BetDate
    {

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT bd
            FROM App\Entity\BetDate bd
            INNER JOIN b.date d
            WHERE bd.bet = :bet AND d.number <= :numberDate'
        )
            ->setParameter('bet', $bet)
            ->setParameter('numberDate', $numberDate);
        return $query->getOneOrNullResult();
    }
}
