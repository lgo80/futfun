<?php

namespace App\Repository;

use App\Entity\RoundLeague;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RoundLeague|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoundLeague|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoundLeague[]    findAll()
 * @method RoundLeague[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoundLeagueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoundLeague::class);
    }

    // /**
    //  * @return RoundLeague[] Returns an array of RoundLeague objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoundLeague
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
