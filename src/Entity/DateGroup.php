<?php

namespace App\Entity;

use App\Repository\DateGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * @ORM\Entity(repositoryClass=DateGroupRepository::class)
 */
class DateGroup
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Date::class, inversedBy="dateGroups", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Date $date;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="dateGroups")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Group $groupName;

    /**
     * @ORM\ManyToOne(targetEntity=Team::class, inversedBy="dateGroups")
     */
    private ?Team $freeTeam;

    /**
     * @ORM\OneToMany(targetEntity=Game::class, mappedBy="dateGroup", orphanRemoval=true)
     */
    private Collection $games;

    #[Pure] public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?Date
    {
        return $this->date;
    }

    public function setDate(?Date $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getGroupName(): ?Group
    {
        return $this->groupName;
    }

    public function setGroupName(?Group $groupName): self
    {
        $this->groupName = $groupName;

        return $this;
    }

    public function getFreeTeam(): ?Team
    {
        return $this->freeTeam;
    }

    public function setFreeTeam(?Team $freeTeam): self
    {
        $this->freeTeam = $freeTeam;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->setDateGroup($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->removeElement($game)) {
            // set the owning side to null (unless already changed)
            if ($game->getDateGroup() === $this) {
                $game->setDateGroup(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->date->getTournament() . " - " . $this->date->getRound() . " - Fecha n° " . $this->date->getName() . " - Grupo " . $this->groupName;
    }

    public function isThereAnyPartyEntered(): bool
    {
        return (count($this->games) > 0);
    }

    public function listGamesActives(): array
    {
        return array_filter($this->games->toArray(), function ($game) {
            return $game->isComplete();
        });
    }

}
