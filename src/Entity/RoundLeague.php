<?php

namespace App\Entity;

use App\Repository\RoundLeagueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoundLeagueRepository::class)
 */
class RoundLeague
{

    const FORM_SIN_DEFINIR = "Definir manualmente";
    const FORM_IGUAL_IDA = "Igual a la ida invirtiendo las localias";
    const FORM_INVERTIDA_IDA = "Invirtiendo los partidos de ida y las localias";
    const FORM_SIN_DEFINIR_ID = 3;
    const FORM_IGUAL_IDA_ID = 1;
    const FORM_INVERTIDA_IDA_ID = 2;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $amountOfGroups;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $amountOfTeamsByGroups;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isRoundTrip;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $returnOfTheGroup;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $classifiedByGroup;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $additionalClassifieds;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private ?string $typeClassifieds;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $additionalDate;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $pointsForGamesWon;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $pointsForTiedGames;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $pointsForGamesLost;

    /**
     * @ORM\OneToOne(targetEntity=Round::class, inversedBy="roundLeague", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Round $round;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmountOfGroups(): ?int
    {
        return $this->amountOfGroups;
    }

    public function setAmountOfGroups(int $amountOfGroups): self
    {
        $this->amountOfGroups = $amountOfGroups;

        return $this;
    }

    public function getAmountOfTeamsByGroups(): ?int
    {
        return $this->amountOfTeamsByGroups;
    }

    public function setAmountOfTeamsByGroups(?int $amountOfTeamsByGroups): self
    {
        $this->amountOfTeamsByGroups = $amountOfTeamsByGroups;

        return $this;
    }

    public function getClassifiedByGroup(): ?int
    {
        return $this->classifiedByGroup;
    }

    public function setClassifiedByGroup(?int $classifiedByGroup): self
    {
        $this->classifiedByGroup = $classifiedByGroup;

        return $this;
    }

    public function getAdditionalClassifieds(): ?int
    {
        return $this->additionalClassifieds;
    }

    public function setAdditionalClassifieds(?int $additionalClassifieds): self
    {
        $this->additionalClassifieds = $additionalClassifieds;

        return $this;
    }

    public function getTypeClassifieds(): ?string
    {
        return $this->typeClassifieds;
    }

    public function setTypeClassifieds(?string $typeClassifieds): self
    {
        $this->typeClassifieds = $typeClassifieds;

        return $this;
    }

    public function getAdditionalDate(): ?int
    {
        return $this->additionalDate;
    }

    public function setAdditionalDate(?int $additionalDate): self
    {
        $this->additionalDate = $additionalDate;

        return $this;
    }

    public function getPointsForGamesWon(): ?int
    {
        return $this->pointsForGamesWon;
    }

    public function setPointsForGamesWon(int $pointsForGamesWon): self
    {
        $this->pointsForGamesWon = $pointsForGamesWon;

        return $this;
    }

    public function getPointsForTiedGames(): ?int
    {
        return $this->pointsForTiedGames;
    }

    public function setPointsForTiedGames(int $pointsForTiedGames): self
    {
        $this->pointsForTiedGames = $pointsForTiedGames;

        return $this;
    }

    public function getPointsForGamesLost(): ?int
    {
        return $this->pointsForGamesLost;
    }

    public function setPointsForGamesLost(int $pointsForGamesLost): self
    {
        $this->pointsForGamesLost = $pointsForGamesLost;

        return $this;
    }

    public function getRound(): ?Round
    {
        return $this->round;
    }

    public function setRound(Round $round): self
    {
        $this->round = $round;

        return $this;
    }

    public function getIsRoundTrip(): ?bool
    {
        return $this->isRoundTrip;
    }

    public function setIsRoundTrip(bool $isRoundTrip): self
    {
        $this->isRoundTrip = $isRoundTrip;

        return $this;
    }

    public function getReturnOfTheGroup(): ?int
    {
        return $this->returnOfTheGroup;
    }

    public function setReturnOfTheGroup(?int $returnOfTheGroup): self
    {
        $this->returnOfTheGroup = $returnOfTheGroup;

        return $this;
    }
}
