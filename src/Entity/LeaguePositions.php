<?php

namespace App\Entity;

use App\Repository\LeaguePositionsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LeaguePositionsRepository::class)
 */
class LeaguePositions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Team::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Team $team;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class)
     */
    private ?Group $groupName;

    /**
     * @ORM\ManyToOne(targetEntity=Tournament::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Tournament $tournament;

    /**
     * @ORM\Column(type="integer")
     */
    private int $points;

    /**
     * @ORM\Column(type="integer")
     */
    private int $goalsScored;

    /**
     * @ORM\Column(type="integer")
     */
    private int $goalsAgainst;

    /**
     * @ORM\Column(type="integer")
     */
    private int $goalDifference;

    /**
     * @ORM\Column(type="integer")
     */
    private int $gamesWon;

    /**
     * @ORM\Column(type="integer")
     */
    private int $tiedGames;

    /**
     * @ORM\Column(type="integer")
     */
    private int $gamesLost;

    /**
     * @ORM\ManyToOne(targetEntity=Round::class, inversedBy="leaguePositions")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Round $round;

    public function __construct()
    {
        $this->points = 0;
        $this->goalsScored = 0;
        $this->goalsAgainst = 0;
        $this->goalDifference = 0;
        $this->gamesWon = 0;
        $this->tiedGames = 0;
        $this->gamesLost = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getGroupName(): ?Group
    {
        return $this->groupName;
    }

    public function setGroupName(?Group $groupName): self
    {
        $this->groupName = $groupName;

        return $this;
    }

    public function getTournament(): ?Tournament
    {
        return $this->tournament;
    }

    public function setTournament(?Tournament $tournament): self
    {
        $this->tournament = $tournament;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getGoalsScored(): ?int
    {
        return $this->goalsScored;
    }

    public function setGoalsScored(int $goalsScored): self
    {
        $this->goalsScored = $goalsScored;

        return $this;
    }

    public function getGoalsAgainst(): ?int
    {
        return $this->goalsAgainst;
    }

    public function setGoalsAgainst(int $goalsAgainst): self
    {
        $this->goalsAgainst = $goalsAgainst;

        return $this;
    }

    public function getGoalDifference(): ?int
    {
        return $this->goalDifference;
    }

    public function setGoalDifference(int $goalDifference): self
    {
        $this->goalDifference = $goalDifference;

        return $this;
    }

    public function getGamesWon(): ?int
    {
        return $this->gamesWon;
    }

    public function setGamesWon(int $gamesWon): self
    {
        $this->gamesWon = $gamesWon;

        return $this;
    }

    public function getTiedGames(): ?int
    {
        return $this->tiedGames;
    }

    public function setTiedGames(int $tiedGames): self
    {
        $this->tiedGames = $tiedGames;

        return $this;
    }

    public function getGamesLost(): ?int
    {
        return $this->gamesLost;
    }

    public function setGamesLost(int $gamesLost): self
    {
        $this->gamesLost = $gamesLost;

        return $this;
    }

    public function getRound(): ?Round
    {
        return $this->round;
    }

    public function setRound(?Round $round): self
    {
        $this->round = $round;

        return $this;
    }
}
