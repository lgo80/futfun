<?php

namespace App\Entity;

use App\Repository\BetDateRepository;
use App\Service\CalendarService;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=BetDateRepository::class)
 */
class BetDate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private UserInterface $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $modificationDateAt;

    /**
     * @ORM\ManyToOne(targetEntity=Date::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Date $date;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isDateWin;

    /**
     * @ORM\OneToMany(targetEntity=BetGame::class, mappedBy="betDate", orphanRemoval=true)
     */
    private Collection $betGames;

    /**
     * @ORM\ManyToOne(targetEntity=Bet::class, inversedBy="betDate")
     * @ORM\JoinColumn(nullable=false)
     */
    private Bet $bet;

    public function __construct()
    {
        $this->modificationDateAt = new DateTime();
        $this->isDateWin = false;
        $this->betGames = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDate(): ?Date
    {
        return $this->date;
    }

    public function setDate(?Date $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIsDateWin(): ?bool
    {
        return $this->isDateWin;
    }

    public function setIsDateWin(bool $isDateWin): self
    {
        $this->isDateWin = $isDateWin;

        return $this;
    }

    public function getModificationDateAt(): ?DateTimeInterface
    {
        return $this->modificationDateAt;
    }

    public function setModificationDateAt(DateTimeInterface $modificationDateAt): self
    {
        $this->modificationDateAt = $modificationDateAt;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getBetGames(): Collection
    {
        return $this->betGames;
    }

    public function addBetGame(BetGame $betGame): self
    {
        if (!$this->betGames->contains($betGame)) {
            $this->betGames[] = $betGame;
            $betGame->setBetDate($this);
        }

        return $this;
    }

    public function removeBetGame(BetGame $betGame): self
    {
        if ($this->betGames->removeElement($betGame)) {
            // set the owning side to null (unless already changed)
            if ($betGame->getBetDate() === $this) {
                $betGame->setBetDate(null);
            }
        }

        return $this;
    }

    public function getBet(): ?Bet
    {
        return $this->bet;
    }

    public function setBet(Bet $bet): self
    {
        $this->bet = $bet;

        return $this;
    }

}
