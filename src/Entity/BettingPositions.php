<?php

namespace App\Entity;

use App\Repository\BettingPositionsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=BettingPositionsRepository::class)
 */
class BettingPositions
{
    private BetDate|null $betDate = null;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?UserInterface $participant;

    /**
     * @ORM\Column(type="integer")
     */
    private int $points;

    /**
     * @ORM\Column(type="integer")
     */
    private int $resultsEmbodiesExacts;

    /**
     * @ORM\Column(type="integer")
     */
    private int $resultsEmbodiesOnly;

    /**
     * @ORM\Column(type="integer")
     */
    private int $resultsFailed;

    /**
     * @ORM\Column(type="integer")
     */
    private int $pointsExtras;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="integer")
     */
    private int $numberOfDatesWon;

    /**
     * @ORM\ManyToOne(targetEntity=Bet::class, inversedBy="bettingPositions")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Bet $bet;

    public function __construct()
    {
        $this->points = 0;
        $this->resultsEmbodiesExacts = 0;
        $this->resultsEmbodiesOnly = 0;
        $this->resultsFailed = 0;
        $this->pointsExtras = 0;
        $this->numberOfDatesWon = 0;
        $this->isActive = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParticipant(): ?UserInterface
    {
        return $this->participant;
    }

    public function setParticipant(?UserInterface $participant): self
    {
        $this->participant = $participant;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getResultsEmbodiesExacts(): ?int
    {
        return $this->resultsEmbodiesExacts;
    }

    public function setResultsEmbodiesExacts(int $resultsEmbodiesExacts): self
    {
        $this->resultsEmbodiesExacts = $resultsEmbodiesExacts;

        return $this;
    }

    public function getResultsEmbodiesOnly(): ?int
    {
        return $this->resultsEmbodiesOnly;
    }

    public function setResultsEmbodiesOnly(int $resultsEmbodiesOnly): self
    {
        $this->resultsEmbodiesOnly = $resultsEmbodiesOnly;

        return $this;
    }

    public function getResultsFailed(): ?int
    {
        return $this->resultsFailed;
    }

    public function setResultsFailed(int $resultsFailed): self
    {
        $this->resultsFailed = $resultsFailed;

        return $this;
    }

    public function getPointsExtras(): ?int
    {
        return $this->pointsExtras;
    }

    public function setPointsExtras(int $pointsExtras): self
    {
        $this->pointsExtras = $pointsExtras;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getNumberOfDatesWon(): ?int
    {
        return $this->numberOfDatesWon;
    }

    public function setNumberOfDatesWon(int $numberOfDatesWon): self
    {
        $this->numberOfDatesWon = $numberOfDatesWon;

        return $this;
    }

    public function getBet(): ?Bet
    {
        return $this->bet;
    }

    public function setBet(?Bet $bet): self
    {
        $this->bet = $bet;

        return $this;
    }

    public function changePositionData(BetDate $betDate, $betGames)
    {

        $this->betDate = $betDate;

        $listOfPoints = $this->returnPointList($betGames);

        $this->points = array_sum($listOfPoints);
        $this->resultsEmbodiesExacts = $this->numberOfResults($this->pointsResultsExacts(), $listOfPoints);
        $this->resultsEmbodiesOnly = $this->numberOfResults($this->pointsResultsOnly(), $listOfPoints);
        $this->resultsFailed = $this->numberOfResults($this->pointsResultsFailures(), $listOfPoints);

        $this->numberOfDatesWon = ($betDate->getIsDateWin()) ? 1 : 0;
    }

    public function returnPointList($betGames)
    {
        return array_map(fn($betGame) => $betGame->getPoints(), $betGames);
    }

    public function numberOfResults($points, $listOfPoints): int
    {
        return count($this->filterBy($points, $listOfPoints));
    }

    public function filterBy($resultValue, $listOfPoints): array
    {
        return array_filter($listOfPoints, fn($point) => $point == $resultValue);
    }

    public function pointsResultsExacts(): ?int
    {
        return $this->bet->getPointsForExactGames();
    }

    public function pointsResultsOnly(): ?int
    {
        return $this->bet->getPointsByResultOnly();
    }

    public function pointsResultsFailures(): ?int
    {
        return $this->bet->getPointsPerMissedGame();
    }

    public function isChampion(User $user): bool
    {
        return ($this->bet->getChampion() === $user);
    }

    public function isDateWin(): bool
    {
        return ($this->betDate && $this->betDate->getIsDateWin());
    }
}
