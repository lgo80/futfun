<?php

namespace App\Entity;

use App\Repository\RoundEliminationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoundEliminationRepository::class)
 */
class RoundElimination
{
    const ROUND_CUSTOMIZED = "Personalizada";
    const ROUND_OCTAVOS = "Octavos de final";
    const ROUND_CUARTOS = "Cuartos de final";
    const ROUND_SEMI = "Semifinal";
    const ROUND_FINAL = "Final";
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $beginningOfElimination;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isRoundTrip;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $isAwayGoal;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isMatchForThirdPlace;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $isRoundTripEnd;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $hasEnd;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $numberOfEliminations;

    /**
     * @ORM\OneToOne(targetEntity=Round::class, inversedBy="roundElimination", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Round $round;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsMatchForThirdPlace(): ?bool
    {
        return $this->isMatchForThirdPlace;
    }

    public function setIsMatchForThirdPlace(bool $isMatchForThirdPlace): self
    {
        $this->isMatchForThirdPlace = $isMatchForThirdPlace;

        return $this;
    }

    public function getIsAwayGoal(): ?bool
    {
        return $this->isAwayGoal;
    }

    public function setIsAwayGoal(bool $isAwayGoal): self
    {
        $this->isAwayGoal = $isAwayGoal;

        return $this;
    }

    public function getRound(): ?Round
    {
        return $this->round;
    }

    public function setRound(Round $round): self
    {
        $this->round = $round;

        return $this;
    }

    public function getIsRoundTrip(): ?bool
    {
        return $this->isRoundTrip;
    }

    public function setIsRoundTrip(bool $isRoundTrip): self
    {
        $this->isRoundTrip = $isRoundTrip;

        return $this;
    }

    public function getIsRoundTripEnd(): ?bool
    {
        return $this->isRoundTripEnd;
    }

    public function setIsRoundTripEnd(bool $isRoundTripEnd): self
    {
        $this->isRoundTripEnd = $isRoundTripEnd;

        return $this;
    }

    public function getBeginningOfElimination(): ?int
    {
        return $this->beginningOfElimination;
    }

    public function setBeginningOfElimination(int $beginningOfElimination): self
    {
        $this->beginningOfElimination = $beginningOfElimination;

        return $this;
    }

    public function getHasEnd(): ?bool
    {
        return $this->hasEnd;
    }

    public function setHasEnd(bool $hasEnd): self
    {
        $this->hasEnd = $hasEnd;

        return $this;
    }

    public function getNumberOfEliminations(): ?int
    {
        return $this->numberOfEliminations;
    }

    public function setNumberOfEliminations(int $numberOfEliminations): self
    {
        $this->numberOfEliminations = $numberOfEliminations;

        return $this;
    }

    public function isRoundTripInSomeDate(): bool
    {
        return $this->isRoundTrip || $this->isRoundTripEnd;
    }
}
