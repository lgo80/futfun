<?php

namespace App\Entity;

use App\Repository\EliminationMatchDataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EliminationMatchDataRepository::class)
 */
class EliminationMatchData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Tournament::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Tournament $tournament;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Game $game;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $typeLocal;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $LocalMatchNumber;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private ?string $typeAway;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $awayMatchNumber;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTournament(): ?Tournament
    {
        return $this->tournament;
    }

    public function setTournament(?Tournament $tournament): self
    {
        $this->tournament = $tournament;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getTypeLocal(): ?string
    {
        return $this->typeLocal;
    }

    public function setTypeLocal(string $typeLocal): self
    {
        $this->typeLocal = $typeLocal;

        return $this;
    }

    public function getLocalMatchNumber(): ?int
    {
        return $this->LocalMatchNumber;
    }

    public function setLocalMatchNumber(int $LocalMatchNumber): self
    {
        $this->LocalMatchNumber = $LocalMatchNumber;

        return $this;
    }

    public function getTypeAway(): ?string
    {
        return $this->typeAway;
    }

    public function setTypeAway(string $typeAway): self
    {
        $this->typeAway = $typeAway;

        return $this;
    }

    public function getAwayMatchNumber(): ?int
    {
        return $this->awayMatchNumber;
    }

    public function setAwayMatchNumber(int $awayMatchNumber): self
    {
        $this->awayMatchNumber = $awayMatchNumber;

        return $this;
    }
}
