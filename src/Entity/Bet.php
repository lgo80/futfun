<?php

namespace App\Entity;

use App\Repository\BetRepository;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BetRepository::class)
 */
class Bet
{
    const STATE_INSCRIPTION = 1;
    const STATE_BEGIN = 2;
    const STATE_FINISHED = 3;
    const EXPIRATION_DATE = "Date";
    const EXPIRATION_GAME = "Game";
    const EXPIRATION_DATE_ID = 1;
    const EXPIRATION_GAME_ID = 2;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Tournament::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Tournament $tournament;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private ?User $champion;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private string $name;

    /**
     * @ORM\Column(type="integer")
     */
    private int $expirationRate;

    /**
     * @ORM\Column(type="integer")
     */
    private int $pointsForExactGames;

    /**
     * @ORM\Column(type="integer")
     */
    private int $pointsByResultOnly;

    /**
     * @ORM\Column(type="integer")
     */
    private int $pointsPerMissedGame;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="integer", length=30)
     */
    private int $state;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeInterface $dateCreationAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $dateExtraLimitAt;

    /**
     * @ORM\OneToMany(targetEntity=AdministratorBet::class, mappedBy="bet", orphanRemoval=true)
     */
    private Collection $administratorBets;

    /**
     * @ORM\Column(type="float",nullable=true)
     */
    private ?float $symbolicBet;

    /**
     * @ORM\OneToMany(targetEntity=BetExtra::class, mappedBy="bet", orphanRemoval=true)
     */
    private Collection $betExtras;

    /**
     * @ORM\OneToMany(targetEntity=BettingPositions::class, mappedBy="bet", orphanRemoval=true)
     */
    private Collection $bettingPositions;

    /**
     * @ORM\OneToMany(targetEntity=BetDate::class, mappedBy="bet", orphanRemoval=true)
     */
    private Collection $betDate;

    public function __construct()
    {
        $this->administratorBets = new ArrayCollection();
        $this->dateCreationAt = new DateTimeImmutable();
        $this->isActive = false;
        $this->state = self::STATE_INSCRIPTION;
        $this->betExtras = new ArrayCollection();
        $this->bettingPositions = new ArrayCollection();
        $this->betDate = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTournament(): ?Tournament
    {
        return $this->tournament;
    }

    public function setTournament(Tournament $tournament): self
    {
        $this->tournament = $tournament;

        return $this;
    }

    public function getChampion(): ?User
    {
        return $this->champion;
    }

    public function setChampion(?User $champion): self
    {
        $this->champion = $champion;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPointsForExactGames(): ?int
    {
        return $this->pointsForExactGames;
    }

    public function setPointsForExactGames(int $pointsForExactGames): self
    {
        $this->pointsForExactGames = $pointsForExactGames;

        return $this;
    }

    public function getPointsByResultOnly(): ?int
    {
        return $this->pointsByResultOnly;
    }

    public function setPointsByResultOnly(int $pointsByResultOnly): self
    {
        $this->pointsByResultOnly = $pointsByResultOnly;

        return $this;
    }

    public function getPointsPerMissedGame(): ?int
    {
        return $this->pointsPerMissedGame;
    }

    public function setPointsPerMissedGame(int $pointsPerMissedGame): self
    {
        $this->pointsPerMissedGame = $pointsPerMissedGame;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getDateExtraLimitAt(): ?DateTimeInterface
    {
        return $this->dateExtraLimitAt;
    }

    public function setDateExtraLimitAt(DateTimeInterface $dateExtraLimitAt): self
    {
        $this->dateExtraLimitAt = $dateExtraLimitAt;

        return $this;
    }

    public function getDateCreationAt(): ?DateTimeInterface
    {
        return $this->dateCreationAt;
    }

    public function setDateCreationAt(DateTimeInterface $dateCreationAt): self
    {
        $this->dateCreationAt = $dateCreationAt;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getAdministratorBets(): Collection
    {
        return $this->administratorBets;
    }

    public function addAdministratorBet(AdministratorBet $administratorBet): self
    {
        if (!$this->administratorBets->contains($administratorBet)) {
            $this->administratorBets[] = $administratorBet;
            $administratorBet->setBet($this);
        }

        return $this;
    }

    public function removeAdministratorBet(AdministratorBet $administratorBet): self
    {
        if ($this->administratorBets->removeElement($administratorBet)) {
            // set the owning side to null (unless already changed)
            if ($administratorBet->getBet() === $this) {
                $administratorBet->setBet(null);
            }
        }

        return $this;
    }

    public function getSymbolicBet(): ?float
    {
        return $this->symbolicBet;
    }

    public function setSymbolicBet(float $symbolicBet): self
    {
        $this->symbolicBet = $symbolicBet;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getExpirationRate(): ?int
    {
        return $this->expirationRate;
    }

    public function setExpirationRate(int $expirationRate): self
    {
        $this->expirationRate = $expirationRate;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection
     */
    public function getBetExtras(): Collection
    {
        return $this->betExtras;
    }

    public function addBetExtra(BetExtra $betExtra): self
    {
        if (!$this->betExtras->contains($betExtra)) {
            $this->betExtras[] = $betExtra;
            $betExtra->setBet($this);
        }

        return $this;
    }

    public function removeBetExtra(BetExtra $betExtra): self
    {
        if ($this->betExtras->removeElement($betExtra)) {
            // set the owning side to null (unless already changed)
            if ($betExtra->getBet() === $this) {
                $betExtra->setBet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getBettingPositions(): Collection
    {
        return $this->bettingPositions;
    }

    public function addBettingPosition(BettingPositions $bettingPosition): self
    {
        if (!$this->bettingPositions->contains($bettingPosition)) {
            $this->bettingPositions[] = $bettingPosition;
            $bettingPosition->setBet($this);
        }

        return $this;
    }

    public function removeBettingPosition(BettingPositions $bettingPosition): self
    {
        if ($this->bettingPositions->removeElement($bettingPosition)) {
            // set the owning side to null (unless already changed)
            if ($bettingPosition->getBet() === $this) {
                $bettingPosition->setBet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getBetDate(): Collection
    {
        return $this->betDate;
    }

    public function addBetDate(BetDate $betDate): self
    {
        if (!$this->betDate->contains($betDate)) {
            $this->betDate[] = $betDate;
            $betDate->setBet($this);
        }

        return $this;
    }

    public function removeBetDate(BetDate $betDate): self
    {
        if ($this->betDate->removeElement($betDate)) {
            // set the owning side to null (unless already changed)
            if ($betDate->getBet() === $this) {
                $betDate->setBet(null);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isEditedByDate(): bool
    {
        return ($this->expirationRate == 1);
    }

    /**
     * @return bool
     */
    public function isBetNew(): bool
    {
        return isset($this->id);
    }
}
