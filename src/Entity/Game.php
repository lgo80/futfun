<?php

namespace App\Entity;

use App\Repository\GameRepository;
use App\Service\CalendarService;
use DateTimeInterface;
use Doctrine\DBAL\Exception;
use JetBrains\PhpStorm\Pure;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{

    const TYPE_TOURNAMENT = "Tournament";
    const TYPE_TIEBREAKER = "Tiebreaker";
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Team::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Team $local;

    /**
     * @ORM\ManyToOne(targetEntity=Team::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Team $away;

    /**
     * @ORM\ManyToOne(targetEntity=DateGroup::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?DateGroup $dateGroup;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $number;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $localValue;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $awayValue;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $localDefinitionValue;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $awayDefinitionValue;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $dateMatch_At;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocal(): ?Team
    {
        return $this->local;
    }

    public function setLocal(?Team $local): self
    {
        $this->local = $local;

        return $this;
    }

    public function getAway(): ?Team
    {
        return $this->away;
    }

    public function setAway(?Team $away): self
    {
        $this->away = $away;

        return $this;
    }

    public function getDateGroup(): ?DateGroup
    {
        return $this->dateGroup;
    }

    public function setDateGroup(?DateGroup $dateGroup): self
    {
        $this->dateGroup = $dateGroup;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getLocalValue(): ?int
    {
        return $this->localValue;
    }

    public function setLocalValue(?int $localValue): self
    {
        $this->localValue = $localValue;

        return $this;
    }

    public function getAwayValue(): ?int
    {
        return $this->awayValue;
    }

    public function setAwayValue(?int $awayValue): self
    {
        $this->awayValue = $awayValue;

        return $this;
    }

    public function getLocalDefinitionValue(): ?int
    {
        return $this->localDefinitionValue;
    }

    public function setLocalDefinitionValue(?int $localDefinitionValue): self
    {
        $this->localDefinitionValue = $localDefinitionValue;

        return $this;
    }

    public function getAwayDefinitionValue(): ?int
    {
        return $this->awayDefinitionValue;
    }

    public function setAwayDefinitionValue(?int $awayDefinitionValue): self
    {
        $this->awayDefinitionValue = $awayDefinitionValue;

        return $this;
    }

    public function getDateMatchAt(): ?DateTimeInterface
    {
        return $this->dateMatch_At;
    }

    public function setDateMatchAt(?DateTimeInterface $dateMatch_At): self
    {
        $this->dateMatch_At = $dateMatch_At;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function __toString()
    {
        return "Partido: " . $this->local . " vs " . $this->away;
    }

    #[Pure] public function isComplete(): bool
    {
        return $this->isNumericAndPositive($this->localValue)
            && $this->isNumericAndPositive($this->awayValue);
    }

    private function isNumericAndPositive(?int $valor): bool
    {
        return is_numeric($valor) && $valor >= 0;
    }

}
