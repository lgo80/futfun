<?php

namespace App\Entity;

use App\Repository\BetExtraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * @ORM\Entity(repositoryClass=BetExtraRepository::class)
 */
class BetExtra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private ?string $detail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isComplete;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $value;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private ?string $valueDetail;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\ManyToOne(targetEntity=Bet::class, inversedBy="betExtra")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Bet $bet;

    /**
     * @ORM\OneToMany(targetEntity=BetExtraUser::class, mappedBy="betExtra", orphanRemoval=true)
     */
    private Collection $betExtraUser;

    #[Pure] public function __construct()
    {
        $this->isActive = false;
        $this->detail = null;
        $this->betExtraUser = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(string $detail): self
    {
        $this->detail = $detail;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsComplete(): ?bool
    {
        return $this->isComplete;
    }

    public function setIsComplete(bool $isComplete): self
    {
        $this->isComplete = $isComplete;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getValueDetail(): ?string
    {
        return $this->valueDetail;
    }

    public function setValueDetail(string $valueDetail): self
    {
        $this->valueDetail = $valueDetail;

        return $this;
    }

    public function __toString()
    {
        return $this->detail;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getBet(): ?Bet
    {
        return $this->bet;
    }

    public function setBet(?Bet $bet): self
    {
        $this->bet = $bet;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getBetExtraUser(): Collection
    {
        return $this->betExtraUser;
    }

    public function addBetExtraUser(BetExtraUser $betExtraUser): self
    {
        if (!$this->betExtraUser->contains($betExtraUser)) {
            $this->betExtraUser[] = $betExtraUser;
            $betExtraUser->setBetExtra($this);
        }

        return $this;
    }

    public function removeBetExtraUser(BetExtraUser $betExtraUser): self
    {
        if ($this->betExtraUser->removeElement($betExtraUser)) {
            // set the owning side to null (unless already changed)
            if ($betExtraUser->getBetExtra() === $this) {
                $betExtraUser->setBetExtra(null);
            }
        }

        return $this;
    }
}
