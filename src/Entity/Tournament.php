<?php

namespace App\Entity;

use App\Repository\TournamentRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TournamentRepository::class)
 */
class Tournament
{

    const CLASS_OFFICIAL = 1;
    const CLASS_PRIVATE = 2;
    const STATE_PROGRESS = 1;
    const STATE_FINALIZED = 2;
    const TYPE_LEAGUE = "Liga";
    const TYPE_LEAGUE_ELIMINATION = "Liga y eliminación";
    const TYPE_ELIMINATION = "Eliminación";
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="smallint")
     */
    private int $class;

    /**
     * @ORM\Column(type="smallint")
     */
    private int $state;

    /**
     * @ORM\Column(type="smallint")
     */
    private ?int $type;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeInterface $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=Round::class, mappedBy="tournament", orphanRemoval=true)
     */
    private ?Collection $rounds;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isTiebreaker;

    /**
     * @ORM\OneToMany(targetEntity=Date::class, mappedBy="tournament", orphanRemoval=true)
     */
    private ?Collection $dates;

    /**
     * @ORM\ManyToOne(targetEntity=Team::class)
     */
    private ?Team $champion;

    /**
     * @ORM\OneToMany(targetEntity=AdministratorTournament::class, mappedBy="tournament")
     */
    private ?Collection $administratorTournaments;

    public function __construct()
    {
        $this->class = $this::CLASS_OFFICIAL;
        $this->state = $this::STATE_PROGRESS;
        $this->isActive = true;
        $this->createdAt = new DateTimeImmutable();
        $this->rounds = new ArrayCollection();
        $this->dates = new ArrayCollection();
        $this->administratorTournaments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getClass(): ?int
    {
        return $this->class;
    }

    public function setClass(int $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsTiebreaker(): ?bool
    {
        return $this->isTiebreaker;
    }

    public function setIsTiebreaker(bool $isTiebreaker): self
    {
        $this->isTiebreaker = $isTiebreaker;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getRounds(): Collection
    {
        return $this->rounds;
    }

    public function addRound(Round $round): self
    {
        if (!$this->rounds->contains($round)) {
            $this->rounds[] = $round;
            $round->setTournament($this);
        }

        return $this;
    }

    public function removeRound(Round $round): self
    {
        if ($this->rounds->removeElement($round)) {
            // set the owning side to null (unless already changed)
            if ($round->getTournament() === $this) {
                $round->setTournament(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getDates(): Collection
    {
        return $this->dates;
    }

    public function addDate(Date $date): self
    {
        if (!$this->dates->contains($date)) {
            $this->dates[] = $date;
            $date->setTournament($this);
        }

        return $this;
    }

    public function removeDate(Date $date): self
    {
        if ($this->dates->removeElement($date)) {
            // set the owning side to null (unless already changed)
            if ($date->getTournament() === $this) {
                $date->setTournament(null);
            }
        }

        return $this;
    }

    public function getChampion(): ?Team
    {
        return $this->champion;
    }

    public function setChampion(?Team $champion): self
    {
        $this->champion = $champion;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getAdministratorTournaments(): Collection
    {
        return $this->administratorTournaments;
    }

    public function addAdministratorTournament(AdministratorTournament $administratorTournament): self
    {
        if (!$this->administratorTournaments->contains($administratorTournament)) {
            $this->administratorTournaments[] = $administratorTournament;
            $administratorTournament->setTournament($this);
        }

        return $this;
    }

    public function removeAdministratorTournament(AdministratorTournament $administratorTournament): self
    {
        if ($this->administratorTournaments->removeElement($administratorTournament)) {
            // set the owning side to null (unless already changed)
            if ($administratorTournament->getTournament() === $this) {
                $administratorTournament->setTournament(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return "Tournament " . $this->name;
    }

}
