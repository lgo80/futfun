<?php

namespace App\Entity;

use App\Repository\BetGameRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass=BetGameRepository::class)
 */
class BetGame
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Game $game;

    /**
     * @ORM\Column(type="integer")
     */
    private int $localResult;

    /**
     * @ORM\Column(type="integer")
     */
    private int $awayResult;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private ?int $points;

    /**
     * @ORM\ManyToOne(targetEntity=BetDate::class, inversedBy="betGame")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?BetDate $betDate;

    public function __construct()
    {
        $this->points = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getLocalResult(): ?int
    {
        return $this->localResult;
    }

    public function setLocalResult(int $localResult): self
    {
        $this->localResult = $localResult;

        return $this;
    }

    public function getAwayResult(): ?int
    {
        return $this->awayResult;
    }

    public function setAwayResult(int $awayResult): self
    {
        $this->awayResult = $awayResult;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getBetDate(): ?BetDate
    {
        return $this->betDate;
    }

    public function setBetDate(?BetDate $betDate): self
    {
        $this->betDate = $betDate;

        return $this;
    }

    /**
     * @param Game $game
     * @return bool
     */
    public function isSomeGame(Game $game): bool
    {
        return $game === $this->game;
    }
}
