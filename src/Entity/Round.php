<?php

namespace App\Entity;

use App\Repository\RoundRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * @ORM\Entity(repositoryClass=RoundRepository::class)
 */
class Round
{

    const TYPE_TOURNAMENT = "Torneo";
    const TYPE_TIEBREAK = "Desempate";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="integer", length=15)
     */
    private ?string $type;

    /**
     * @ORM\ManyToOne(targetEntity=Tournament::class, inversedBy="rounds")
     * @ORM\JoinColumn(nullable=false)
     */
    private Tournament $tournament;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $number;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $numberOfEquipament;

    /**
     * @ORM\OneToOne(targetEntity=RoundLeague::class, mappedBy="round", cascade={"persist", "remove"})
     */
    private ?RoundLeague $roundLeague;

    /**
     * @ORM\OneToOne(targetEntity=RoundElimination::class, mappedBy="round", cascade={"persist", "remove"})
     */
    private ?RoundElimination $roundElimination;

    /**
     * @ORM\OneToMany(targetEntity=Date::class, mappedBy="round", orphanRemoval=true)
     */
    private Collection $dates;

    /**
     * @ORM\OneToMany(targetEntity=LeaguePositions::class, mappedBy="round", orphanRemoval=true)
     */
    private Collection $leaguePositions;

    #[Pure] public function __construct()
    {
        $this->dates = new ArrayCollection();
        $this->leaguePositions = new ArrayCollection();
        $this->roundElimination = null;
        $this->roundLeague = null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTournament(): ?Tournament
    {
        return $this->tournament;
    }

    public function setTournament(Tournament $tournament): self
    {
        $this->tournament = $tournament;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getNumberOfEquipament(): ?int
    {
        return $this->numberOfEquipament;
    }

    public function setNumberOfEquipament(int $numberOfEquipament): self
    {
        $this->numberOfEquipament = $numberOfEquipament;

        return $this;
    }

    public function getRoundLeague(): ?RoundLeague
    {
        return $this->roundLeague;
    }

    public function setRoundLeague(RoundLeague $roundLeague): self
    {
        // set the owning side of the relation if necessary
        if ($roundLeague->getRound() !== $this) {
            $roundLeague->setRound($this);
        }

        $this->roundLeague = $roundLeague;

        return $this;
    }

    public function getRoundElimination(): ?RoundElimination
    {
        return $this->roundElimination;
    }

    public function setRoundElimination(RoundElimination $roundElimination): self
    {
        // set the owning side of the relation if necessary
        if ($roundElimination->getRound() !== $this) {
            $roundElimination->setRound($this);
        }

        $this->roundElimination = $roundElimination;

        return $this;
    }

    public function __toString()
    {
        return $this->number . '° Round';
    }

    /**
     * @return Collection
     */
    public function getDates(): Collection
    {
        return $this->dates;
    }

    public function addDate(Date $date): self
    {
        if (!$this->dates->contains($date)) {
            $this->dates[] = $date;
            $date->setRound($this);
        }

        return $this;
    }

    public function removeDate(Date $date): self
    {
        if ($this->dates->removeElement($date)) {
            // set the owning side to null (unless already changed)
            if ($date->getRound() === $this) {
                $date->setRound(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getLeaguePositions(): Collection
    {
        return $this->leaguePositions;
    }

    public function addLeaguePosition(LeaguePositions $leaguePosition): self
    {
        if (!$this->leaguePositions->contains($leaguePosition)) {
            $this->leaguePositions[] = $leaguePosition;
            $leaguePosition->setRound($this);
        }

        return $this;
    }

    public function removeLeaguePosition(LeaguePositions $leaguePosition): self
    {
        if ($this->leaguePositions->removeElement($leaguePosition)) {
            // set the owning side to null (unless already changed)
            if ($leaguePosition->getRound() === $this) {
                $leaguePosition->setRound(null);
            }
        }

        return $this;
    }

    /**
     * @return RoundLeague|RoundElimination
     */
    #[Pure] public function getRoundNeed(): RoundLeague|RoundElimination
    {
        return ($this->roundLeague)
            ? $this->roundLeague
            : $this->roundElimination;
    }
}
