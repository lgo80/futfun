<?php

namespace App\Entity;

use App\Repository\BetExtraUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=BetExtraUserRepository::class)
 */
class BetExtraUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?UserInterface $participante;

    /**
     * @ORM\Column(type="string", length=150,nullable=true)
     */
    private ?string $detail;

    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private ?bool $isCorrect;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @ORM\ManyToOne(targetEntity=BetExtra::class, inversedBy="betExtraUser")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?BetExtra $betExtra;


    public function __construct()
    {
        $this->isCorrect = null;
        $this->isActive = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParticipante(): ?UserInterface
    {
        return $this->participante;
    }

    public function setParticipante(?UserInterface $participante): self
    {
        $this->participante = $participante;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(string $detail): self
    {
        $this->detail = $detail;

        return $this;
    }

    public function getIsCorrect(): ?bool
    {
        return $this->isCorrect;
    }

    public function setIsCorrect(bool $isCorrect): self
    {
        $this->isCorrect = $isCorrect;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getBetExtra(): ?BetExtra
    {
        return $this->betExtra;
    }

    public function setBetExtra(?BetExtra $betExtra): self
    {
        $this->betExtra = $betExtra;

        return $this;
    }
}
