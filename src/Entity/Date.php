<?php

namespace App\Entity;

use App\Repository\DateRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * @ORM\Entity(repositoryClass=DateRepository::class)
 */
class Date
{

    const TYPE_TOURNAMENT = "Tournament";
    const TYPE_TIEBREAKER = "Tiebreaker";
    const DATE_ACTUAL = "actual";
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $number;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $startDate;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $type;

    /**
     * @ORM\ManyToOne(targetEntity=Tournament::class, inversedBy="dates")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Tournament $tournament;

    /**
     * @ORM\OneToMany(targetEntity=DateGroup::class, mappedBy="date", orphanRemoval=true)
     */
    private Collection $dateGroups;

    /**
     * @ORM\ManyToOne(targetEntity=Round::class, inversedBy="dates")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Round $round;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $numberOfGames;

    #[Pure] public function __construct()
    {
        $this->dateGroups = new ArrayCollection();
        $this->round = null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getTournament(): ?Tournament
    {
        return $this->tournament;
    }

    public function setTournament(?Tournament $tournament): self
    {
        $this->tournament = $tournament;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getDateGroups(): Collection
    {
        return $this->dateGroups;
    }

    public function addDateGroup(DateGroup $dateGroup): self
    {
        if (!$this->dateGroups->contains($dateGroup)) {
            $this->dateGroups[] = $dateGroup;
            $dateGroup->setDate($this);
        }

        return $this;
    }

    public function removeDateGroup(DateGroup $dateGroup): self
    {
        if ($this->dateGroups->removeElement($dateGroup)) {
            // set the owning side to null (unless already changed)
            if ($dateGroup->getDate() === $this) {
                $dateGroup->setDate(null);
            }
        }

        return $this;
    }

    /**
     * @return Round|null
     */
    public function getRound(): ?Round
    {
        return $this->round;
    }

    public function setRound(?Round $round): self
    {
        $this->round = $round;

        return $this;
    }

    public function getNumberOfGames(): ?int
    {
        return $this->numberOfGames;
    }

    public function setNumberOfGames(?int $numberOfGames): self
    {
        $this->numberOfGames = $numberOfGames;

        return $this;
    }

    public function __toString()
    {
        return $this->tournament . " - " . $this->round . " - Fecha n° " . $this->name;
    }

    public function isThereAnyPartyEntered(): bool
    {
        return (count($this->dateGroups) > 0);
    }
}
