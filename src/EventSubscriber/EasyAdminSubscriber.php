<?php

namespace App\EventSubscriber;

use App\Entity\Date;
use App\Entity\DateGroup;
use App\Entity\Game;
use App\Repository\DateGroupRepository;
use App\Repository\DateRepository;
use App\Repository\GameRepository;
use App\Service\DateGroupService;
use App\Service\DateService;
use App\Service\GameService;
use App\Service\PositionsService;
use App\Service\RoundService;
use App\Validation\RoundValidation;
use Doctrine\ORM\NonUniqueResultException;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\Form\Type\DoctrineType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;

class EasyAdminSubscriber extends AbstractController implements EventSubscriberInterface
{
    private RoundService $roundService;
    private PositionsService $positionsService;
    private DateGroupService $dateGroupService;
    private GameService $gameService;
    private RoundValidation $roundValidation;
    private DateRepository $dateRepository;
    private DateGroupRepository $dateGroupRepository;
    private GameRepository $gameRepository;
    private DateService $dateService;

    #[Pure] public function __construct(
        RoundService        $roundService,
        PositionsService    $positionsService,
        DateGroupService    $dateGroupService,
        GameService         $gameService,
        RoundValidation     $roundValidation,
        DateRepository      $dateRepository,
        GameRepository      $gameRepository,
        DateGroupRepository $dateGroupRepository,
        DateService         $dateService)
    {
        $this->roundService = $roundService;
        $this->positionsService = $positionsService;
        $this->dateGroupService = $dateGroupService;
        $this->gameService = $gameService;
        $this->roundValidation = $roundValidation;
        $this->dateRepository = $dateRepository;
        $this->dateGroupRepository = $dateGroupRepository;
        $this->gameRepository = $gameRepository;
        $this->dateService = $dateService;
    }

    #[ArrayShape([AfterEntityPersistedEvent::class => "string[]"])] public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => ['setBlogPostSlug'],
        ];
    }

    /**
     * @throws NonUniqueResultException
     */
    public function setBlogPostSlug(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof Date) {
            $this->generateDateBack($entity);
        } else if ($entity instanceof DateGroup) {
            $this->generateDateGroupBack($entity);
        } else if ($entity instanceof Game) {
            $this->generateGameBack($entity);
            $this->generateLeaguePositions($entity);
        }
    }

    /**
     * @param Date $date
     * @return void
     */
    private function generateDateBack(Date $date): void
    {
        if (!$this->roundService->hasDateBack($date->getRound()))
            return;
        $dateBack = $this->dateService
            ->generateDateBack($date);
        $this->dateRepository
            ->store($dateBack);
    }

    /**
     * @param DateGroup $dateGroup
     * @return void
     */
    private function generateDateGroupBack(DateGroup $dateGroup): void
    {
        if (!$this->roundService->hasDateBack(
            $dateGroup->getDate()->getRound()))
            return;
        $dateBack = $this->dateService->getDateBack($dateGroup->getDate());
        $dateGroupBack = $this->dateGroupService
            ->generateDateGroupBack($dateGroup, $dateBack);
        $this->dateGroupRepository
            ->store($dateGroupBack);
    }

    /**
     * @throws NonUniqueResultException
     */
    private function generateGameBack(Game $game): void
    {
        if (!$this->roundService->hasDateBack(
            $game->getDateGroup()->getDate()->getRound()))
            return;
        $dateBack = $this->dateService
            ->getDateBack($game->getDateGroup()->getDate());
        $dateGroupBack = $this->dateGroupService
            ->getDateGroupBack(
                $dateBack,
                $game->getDateGroup()->getGroupName()
            );
        $gameBack = $this->gameService
            ->generateGameBack($dateGroupBack, $game);
        $this->gameRepository
            ->store($gameBack);
    }

    /**
     * @param Game $game
     * @return void
     */
    private function generateLeaguePositions(Game $game): void
    {
        if (!$this->roundValidation->hasRoundLeague(
            $game->getDateGroup()->getDate()->getRound()))
            return;
        $this->positionsService
            ->generateLeaguePositions($game);
    }
}
