<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 *
 */
class SessionService
{

    const SESSION_BET = 'bet';
    const SESSION_BET_EXTRA = 'betExtra';
    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack) //
    {
        $this->requestStack = $requestStack;
        // session_start();
    }

    public function setSessionArray($attribute, $value)
    {
        $session = $this->requestStack->getSession();

        $array = ($this->getSession($attribute) && is_array($this->getSession($attribute)))
            ? $this->getSession($attribute) : [];

        $array[] = $value;

        // stores an attribute in the session for later reuse
        $session->set($attribute, $array);

        // $array = ($_SESSION[$attribute] && is_array($_SESSION[$attribute]))
        //     ? $_SESSION[$attribute] : [];

        // $array[] = $value;
        // $_SESSION[$attribute] = $value;
    }

    public function setSessionObject($attribute, $value)
    {
        $session = $this->requestStack->getSession();

        // stores an attribute in the session for later reuse
        $session->set($attribute, $value);
        // $_SESSION[$attribute] = $value;
    }

    public function getSession($attribute)
    {
        $session = $this->requestStack->getSession();

        // gets an attribute by name
        return $session->get($attribute);
        // return $_SESSION[$attribute];
    }

    public function remove($attribute)
    {

        $session = $this->requestStack->getSession();
        $session->set($attribute, null);
        // unset($_SESSION[$attribute]);
    }

    // public function someMethod()
    // {
    //     $session = $this->requestStack->getSession();

    //     // stores an attribute in the session for later reuse
    //     $session->set('attribute-name', 'attribute-value');

    //     // gets an attribute by name
    //     $foo = $session->get('foo');

    //     // the second argument is the value returned when the attribute doesn't exist
    //     $filters = $session->get('filters', []);

    //     // ...
    // }

}
