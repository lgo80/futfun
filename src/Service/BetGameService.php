<?php

namespace App\Service;

use App\Entity\Bet;
use App\Entity\BetDate;
use App\Entity\BetGame;
use App\Entity\Date;
use App\Entity\Game;
use App\Repository\BetDateRepository;
use App\Repository\BetGameRepository;
use App\Repository\DateRepository;
use App\Validation\BetDateValidation;
use Doctrine\DBAL\Exception;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 *
 */
class BetGameService extends AbstractBetService
{

    private BetDateService $betDateService;
    private BetDateRepository $betDateRepository;
    private DateRepository $dateRepository;
    private BetGameRepository $betGameRepository;
    private BetDateValidation $betDateValidation;
    private GameService $gameService;

    public function __construct(BetDateRepository $betDateRepository,
                                DateRepository    $dateRepository,
                                BetDateService    $betDateService,
                                BetGameRepository $betGameRepository,
                                BetDateValidation $betDateValidation,
                                GameService       $gameService)
    {
        $this->betDateRepository = $betDateRepository;
        $this->betDateService = $betDateService;
        $this->dateRepository = $dateRepository;
        $this->betGameRepository = $betGameRepository;
        $this->betDateValidation = $betDateValidation;
        $this->gameService = $gameService;
    }

    public function generateBetDate(
        Bet $bet, UserInterface $userLogin, Date $date): BetDate
    {

        $betDate = $this->betDateRepository
            ->findOneBy(['bet' => $bet, 'user' => $userLogin, 'date' => $date]);

        return ($betDate) ? $betDate :
            $this->generateNewBetDate($bet, $userLogin, $date);
    }

    public function generateCurrentDate(Bet $bet, int $numberDate): ?Date
    {
        return ($numberDate == 0) ?
            $this->dateRepository
                ->findByNextDateActive($bet->getTournament()) :
            $this->dateRepository
                ->findOneBy(['tournament' => $bet->getTournament(), 'number' => $numberDate]);
    }

    public function determineCurrentParty($date, $betDate): ?Game  //--> falta Testear
    {

        $dateGroups = $date->getDateGroups();
        foreach ($dateGroups as $dateGroup) {
            $games = $dateGroup->getGames();
            foreach ($games as $game) {
                $betGame = $this->betGameRepository
                    ->findOneBy(['game' => $game, 'betDate' => $betDate]);

                if (!$betGame)
                    return $game;
            }
        }

        return null;
    }

    public function generateNewBetGame(BetDate $betDate, Game $game): BetGame
    {
        $betGame = new BetGame();
        $betGame->setBetDate($betDate);
        $betGame->setGame($game);
        return $betGame;
    }

    private function generateNewBetDate(Bet $bet, UserInterface $userLogin, Date $date): BetDate
    {
        $betDate = new BetDate();
        $betDate->setDate($date);
        $betDate->setUser($userLogin);
        $betDate->setBet($bet);
        $this->betDateRepository
            ->store($betDate);

        return $betDate;
    }

    /**
     * @throws Exception
     */
    public function canBeModified(UserInterface $user, BetDate $betDate, Game $game): bool
    {

        return $this->betDateValidation->validateUser($betDate->getUser(), $user)
            && !$game->isComplete()
            && $this->checkTimeToEdit($betDate, $game);
    }

    /**
     * @throws Exception
     * @throws \Exception
     */
    public function checkTimeToEdit(BetDate $betDate, Game $game): bool
    {
        return $this->betDateService->isModifiable($betDate)
            || $this->isModifiable($betDate, $game);
    }

    /**
     * @throws Exception
     * @throws \Exception
     */
    private function isModifiable(BetDate $betDate, Game $game): bool
    {
        return !$betDate->getBet()->isEditedByDate()
            && $this->checkDateGame($game);
    }

    /**
     * @throws \Exception
     */
    private function checkDateGame(Game $game): bool
    {
        return !($game->getDateMatchAt())
            || $this->gameService->isDateGameUnExpired($game);
    }

}
