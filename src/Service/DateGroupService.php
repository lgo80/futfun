<?php

namespace App\Service;

use App\Entity\Date;
use App\Entity\DateGroup;
use App\Repository\DateGroupRepository;
use App\Validation\DateValidation;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use JetBrains\PhpStorm\Pure;

/**
 *
 */
class DateGroupService extends AbstractBetService
{
    private DateGroupRepository $dateGroupRepository;

    public function __construct(DateGroupRepository $dateGroupRepository)
    {
        $this->dateGroupRepository = $dateGroupRepository;
    }

    public function generateDateGroupBack(DateGroup $dateGroup, Date $dateBack): ?DateGroup
    {
        $dateGroupBack = new DateGroup();
        $dateGroupBack->setDate($dateBack);
        $dateGroupBack->setFreeTeam($dateGroup->getFreeTeam());
        $dateGroupBack->setGroupName($dateGroup->getGroupName());
        return $dateGroupBack;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getDateGroupBack(Date $date, $groupName): ?DateGroup
    {
        return $this->dateGroupRepository
            ->findOneBySomeField($date, $groupName);
    }
}
