<?php

namespace App\Service;

use App\Entity\Date;
use App\Entity\RoundLeague;
use JetBrains\PhpStorm\Pure;

class RoundLeagueService extends AbstractBetService
{

    /**
     * @param RoundLeague $roundLeague
     * @param Date $date
     * @return bool
     */
    public function hasDateBack(RoundLeague $roundLeague, Date $date): bool
    {
        if (!$roundLeague->getIsRoundTrip())
            return false;
        return $this->verifyHasDateBack($roundLeague, $date);
    }

    /**
     * @param RoundLeague $roundLeague
     * @param Date $date
     * @return bool
     */
    private function verifyHasDateBack(
        RoundLeague $roundLeague, Date $date): bool
    {
        return $roundLeague->getReturnOfTheGroup() != RoundLeague::FORM_SIN_DEFINIR_ID &&
            $date->getNumber() <= $this->getAmountOfFirstDate($roundLeague);
    }

    /**
     * @param RoundLeague $roundLeague
     * @param int $numberDate
     * @return int
     */
    public function getNumberDateBack(RoundLeague $roundLeague, int $numberDate): int
    {
        return match ($roundLeague->getReturnOfTheGroup()) {
            1 => $this->getAmountOfFirstDate($roundLeague) + $numberDate,
            2 => $this->getAmountOfAllDate($roundLeague) - ($numberDate - 1),
            default => null,
        };
    }

    /**
     * @param RoundLeague $roundLeague
     * @return int
     */
    public function getAmountOfFirstDate(RoundLeague $roundLeague): int
    {
        $cantidadEquipos = ($roundLeague->getAmountOfTeamsByGroups())
            ? $roundLeague->getAmountOfTeamsByGroups()
            : $roundLeague->getRound()->getNumberOfEquipament();

        return $this->checkEden($cantidadEquipos) ? $cantidadEquipos - 1 : $cantidadEquipos;
    }

    /**
     * @param RoundLeague $roundLeague
     * @return int
     */
    public function getAmountOfAllDate(RoundLeague $roundLeague): int
    {
        return ($roundLeague->getIsRoundTrip())
            ? $this->getAmountOfFirstDate($roundLeague) * 2
            : $this->getAmountOfFirstDate($roundLeague);
    }

    /**
     * @param RoundLeague $roundLeague
     * @return int
     */
    #[Pure] public function getNumberOfGroup(RoundLeague $roundLeague): int
    {
        return $roundLeague->getAmountOfGroups();
    }

    /**
     * @param RoundLeague $roundLeague
     * @return int
     */
    #[Pure] public function quantityOfDates(RoundLeague $roundLeague): int
    {
        return $this->quantityOfDatesOnlyGo($roundLeague)
            * $this->isCheckDates($roundLeague->getIsRoundTrip())
            + $this->generateAdditionalDate($roundLeague);
    }

    /**
     * @param RoundLeague $roundLeague
     * @return int
     */
    #[Pure] public function quantityOfDatesOnlyGo(RoundLeague $roundLeague): int
    {
        return ($this->checkEden($roundLeague->getAmountOfTeamsByGroups()))
            ? $roundLeague->getAmountOfTeamsByGroups() - 1
            : $roundLeague->getAmountOfTeamsByGroups();
    }

    /**
     * @param RoundLeague $roundLeague
     * @return int
     */
    #[Pure] public function generateAdditionalDate(RoundLeague $roundLeague): int
    {
        return ($roundLeague->getAdditionalDate())
            ? $roundLeague->getAdditionalDate()
            : 0;
    }

    /**
     * @param bool $isRoundTrip
     * @return int
     */
    public function isCheckDates(bool $isRoundTrip): int
    {
        return ($isRoundTrip) ? 2 : 1;
    }

}