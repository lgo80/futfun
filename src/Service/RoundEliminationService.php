<?php

namespace App\Service;

use App\Entity\Date;
use App\Entity\RoundElimination;
use JetBrains\PhpStorm\Pure;

class RoundEliminationService extends AbstractBetService
{

    /**
     * @param RoundElimination $roundElimination
     * @param Date $date
     * @return bool
     */
    public function hasDateBack(
        RoundElimination $roundElimination, Date $date): bool
    {
        return $roundElimination->isRoundTripInSomeDate()
            && $this->checkIfTheDateHasASecondLeg($roundElimination, $date)
            && $this->checkIfItIsAReturnDate($roundElimination, $date);
    }

    /**
     * @param RoundElimination $roundElimination
     * @param int $numberDate
     * @return int
     */
    public function getNumberDateBack(RoundElimination $roundElimination, int $numberDate): int
    {
        return $numberDate + 1;
    }

    /**
     * @param RoundElimination $roundElimination
     * @return int
     */
    #[Pure] public function getAmountOfFirstDate(RoundElimination $roundElimination): int
    {
        return $roundElimination->getNumberOfEliminations();
    }

    /**
     * @param RoundElimination $roundElimination
     * @return int
     */
    #[Pure] public function getAmountOfAllDate(RoundElimination $roundElimination): int
    {
        return $this->getAmountOfFirstDate($roundElimination);
    }

    /**
     * @param RoundElimination $roundElimination
     * @return int
     */
    public function getNumberOfGroup(RoundElimination $roundElimination): int
    {
        return 1;
    }

    /**
     * @param RoundElimination $roundElimination
     * @return int
     */
    #[Pure] public function quantityOfDates(RoundElimination $roundElimination): int
    {
        return $roundElimination->getNumberOfEliminations();
    }

    /**
     * @param RoundElimination $roundElimination
     * @param Date $date
     * @return bool
     */
    private function checkIfTheDateHasASecondLeg(
        RoundElimination $roundElimination, Date $date): bool
    {
        //dd($this->isDateEnd($roundElimination, $date));
        return ($this->isDateEnd($roundElimination, $date) && $roundElimination->getIsRoundTripEnd()) ||
            (!$this->isDateEnd($roundElimination, $date) && $roundElimination->getIsRoundTrip());
    }

    /**
     * @param RoundElimination $roundElimination
     * @param Date $date
     * @return bool
     */
    private function checkIfItIsAReturnDate(
        RoundElimination $roundElimination, Date $date): bool
    {
        if ($this->isDateEnd($roundElimination, $date))
            return $this->quantityOfDates($roundElimination) != $date->getNumber();
        return $roundElimination->getIsRoundTrip() && !$this->checkEden($date->getNumber());
    }

    /**
     * @param RoundElimination $roundElimination
     * @param Date $date
     * @return bool
     */
    private function isDateEnd(RoundElimination $roundElimination, Date $date): bool
    {
        return $roundElimination->getHasEnd() &&
            $this->numberOfDatesForTheFirstLegFinal($roundElimination) <= $date->getNumber();
    }

    /**
     * @param RoundElimination $roundElimination
     * @return int
     */
    #[Pure] private function numberOfDatesForTheFirstLegFinal(
        RoundElimination $roundElimination): int
    {
        return $this->quantityOfDates($roundElimination) - (($roundElimination->getIsRoundTripEnd()) ? 1 : 0);
    }

}