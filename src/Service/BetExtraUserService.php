<?php

namespace App\Service;

use App\Entity\Bet;
use App\Entity\BetExtra;
use App\Entity\BetExtraUser;
use App\Repository\BetExtraRepository;
use App\Repository\BetExtraUserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 *
 */
class BetExtraUserService extends AbstractBetService
{
    protected BetExtraRepository $betExtraRepository;
    protected BetExtraUserRepository $betExtraUserRepository;

    public function __construct(BetExtraRepository     $betExtraRepository,
                                BetExtraUserRepository $betExtraUserRepository)
    {
        $this->betExtraRepository = $betExtraRepository;
        $this->betExtraUserRepository = $betExtraUserRepository;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getBetExtraWithoutComplete(UserInterface $user,
                                               Bet           $bet): ?BetExtra //->Falta testear
    {

        $betExtras = $this->betExtraRepository
            ->findBy(["bet" => $bet, "isComplete" => true]);

        foreach ($betExtras as $betExtra) {

            $betExtraUser = $this->betExtraUserRepository
                ->findByExtraCompleteForParticipant($user, $betExtra);

            if (!$betExtraUser)
                return $betExtra;
        }

        return null;
    }

    public function generateBetExtraNew(UserInterface $user,
                                        BetExtra      $betExtra): ?BetExtraUser
    {
        $betExtraUser = new BetExtraUser();
        $betExtraUser->setParticipante($user);
        $betExtraUser->setBetExtra($betExtra);
        return $betExtraUser;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function saveExtraUser(UserInterface $user, Bet $bet,
                                  betExtraUser  $betExtraUser): void
    {
        $this->betExtraUserRepository
            ->store($betExtraUser);
        $betExtras = $this->betExtraRepository
            ->findBy(['bet' => $bet, 'isComplete' => false]);
        $this->betExtraUserRepository
            ->updatingExtrasWithoutComplete($betExtras, $user);
    }
}
