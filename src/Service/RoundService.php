<?php

namespace App\Service;

use App\Entity\Date;
use App\Entity\Round;
use App\Repository\DateRepository;
use App\Validation\RoundValidation;
use Doctrine\DBAL\Exception;
use JetBrains\PhpStorm\Pure;

/**
 *
 */
class RoundService extends AbstractBetService
{

    protected RoundLeagueService $roundLeagueService;
    protected RoundEliminationService $roundEliminationService;
    protected RoundValidation $roundValidation;

    #[Pure] public function __construct(
        RoundLeagueService      $roundLeagueService,
        RoundEliminationService $roundEliminationService,
        RoundValidation         $roundValidation)
    {
        $this->roundLeagueService = $roundLeagueService;
        $this->roundEliminationService = $roundEliminationService;
        $this->roundValidation = $roundValidation;
    }

    /**
     * @param Round $round
     * @param Date $date
     * @return bool
     */
    public function hasDateBack(Round $round, Date $date): bool
    {
        $service = $this->getServiceRound($round);
        return $service->hasDateBack($round->getRoundNeed(), $date);
    }

    /**
     * @param Round $round
     * @param $numberDate
     * @return int|null
     */
    public function getNumberDateBack(Round $round, $numberDate): ?int
    {
        $service = $this->getServiceRound($round);
        return $service->getNumberDateBack($round->getRoundNeed(), $numberDate);
    }

    /**
     * @throws Exception
     */
    public function numberOfPartiesByGroup(Round $round): mixed
    {
        $this->roundValidation->isValidateRoundNeed($round);
        return ($round->getRoundLeague())
            ? floor($round->getRoundLeague()->getAmountOfTeamsByGroups() / 2)
            : throw new Exception('No se puede calcular los partidos de una eliminacion en la clase Round');
    }

    /**
     * @throws Exception
     */
    public function numberOfGroup(Round $round): int
    {
        $this->roundValidation->isValidateRoundNeed($round);
        $service = $this->getServiceRound($round);
        return $service->getNumberOfGroup($round->getRoundNeed());
    }

    /**
     * @throws Exception
     */
    public function quantityOfDates(Round $round): int
    {
        $this->roundValidation->isValidateRoundNeed($round);
        $service = $this->getServiceRound($round);
        return $service->quantityOfDates($round->getRoundNeed());
    }

    /**
     * @param Round $round
     * @return RoundLeagueService|RoundEliminationService
     */
    #[Pure] public function getServiceRound(Round $round): RoundLeagueService|RoundEliminationService
    {
        return ($round->getRoundLeague())
            ? $this->roundLeagueService
            : $this->roundEliminationService;
    }

}
