<?php

namespace App\Service;

use App\Entity\Date;
use App\Entity\DateGroup;
use App\Repository\DateGroupRepository;
use App\Repository\DateRepository;
use App\Validation\DateValidation;
use Doctrine\DBAL\Exception;
use JetBrains\PhpStorm\Pure;

class DateService extends AbstractBetService
{

    protected RoundService $roundService;
    protected CalendarService $calendarService;
    protected DateValidation $dateValidation;
    protected DateRepository $dateRepository;

    #[Pure] public function __construct(
        RoundService    $roundService,
        DateValidation  $dateValidation,
        DateRepository  $dateRepository,
        CalendarService $calendarService)
    {
        $this->roundService = $roundService;
        $this->calendarService = $calendarService;
        $this->dateValidation = $dateValidation;
        $this->dateRepository = $dateRepository;
    }

    /**
     * @param Date $date
     * @return Date|null
     */
    public function generateDateBack(Date $date): ?Date
    {
        $round = $date->getRound();

        $dateBack = new Date();
        $nameDate = $this->roundService
            ->getNumberDateBack($round, $date->getNumber());
        $dateBack->setNumber($nameDate);
        $dateBack->setName($nameDate);
        $dateBack->setType($date->getType());
        $dateBack->setTournament($date->getTournament());
        $dateBack->setRound($date->getRound());

        return $dateBack;
    }

    /**
     * @param Date $date
     * @return Date|null
     */
    public function getDateBack(Date $date): ?Date // -> falta testear
    {
        $numberDateBack = $this->roundService
            ->getNumberDateBack(
                $date->getRound(),
                $date->getNumber()
            );
        return $this->dateRepository
            ->findOneBySomeField($numberDateBack, $date->getTournament(), $date->getRound());
    }

    /**
     */
    public function thereIsSomeCompletedParty(Date $date): bool
    {
        return $date->isThereAnyPartyEntered()
            && $this->isListNotEmptied(
                $this->listGroupActiveWithSomeParty($date));
    }

    /**
     * @throws Exception
     */
    public function areAllTheCompleteGames(Date $date): bool
    {
        return $date->isThereAnyPartyEntered()
            && $this->isAllGroupCompleted($date,
                $this->listGroupActiveWithAllParty($date));
    }

    /**
     * @throws Exception
     */
    public function isAllGroupCompleted(Date $date, array $group): bool
    {
        $this->dateValidation->isValidateRound($date);
        return $this->verifyListEqualsFor($group,
            $this->roundService->numberOfGroup($date->getRound()));
    }

    /**
     * @param Date $date
     * @return array
     */
    public function listGroupActiveWithSomeParty(Date $date): array //-> Falta testear
    {
        return array_filter($date->getDateGroups()->toArray(),
            fn($dateGroups) => $this->
            thereIsSomeCompletedPartyInDateGroup($dateGroups));
    }

    /**
     * @param Date $date
     * @return array
     * @throws Exception
     */
    public function listGroupActiveWithAllParty(Date $date): array //-> Falta testear
    {
        return array_filter($date->getDateGroups()->toArray(),
            fn($dateGroups) => $this->
            areAllPartiesCompletedInDateGroup($dateGroups));
    }

    /**
     * @throws Exception
     */
    public function numberOfGamesForGroup(Date $date): ?int // -> falta testear
    {
        $this->dateValidation->isValidateRound($date);
        return $this->roundService->numberOfPartiesByGroup($date->getRound());
    }

    /**
     * @throws Exception
     * @throws \Exception
     */
    public function isDateUnExpired(Date $date): bool
    {
        return $this->calendarService->isTheDateGreaterThan(
            $this->calendarService->getDateToday(), $date->getStartDate());
    }

    /**
     * @param DateGroup $dateGroup
     * @return bool
     */
    public function thereIsSomeCompletedPartyInDateGroup(DateGroup $dateGroup): bool
    {
        return $dateGroup->isThereAnyPartyEntered() &&
            $this->isListNotEmptied($dateGroup->listGamesActives());
    }

    /**
     * @throws Exception
     */
    public function areAllPartiesCompletedInDateGroup(DateGroup $dateGroup): bool
    {
        return $dateGroup->isThereAnyPartyEntered() &&
            $this->verifyNumberOfPartiesByGroup(
                $dateGroup->listGamesActives(), $dateGroup);
    }

    /**
     * @throws Exception
     */
    public function verifyNumberOfPartiesByGroup(
        array $gameCompleted, DateGroup $dateGroup): bool //-> falta testear
    {
        return $this->verifyListEqualsFor($gameCompleted,
            $this->numberOfGamesForGroup($dateGroup->getDate()));
    }

}