<?php

namespace App\Service;

/**
 *
 */
abstract class AbstractBetService
{

    /**
     * @param int $number
     * @return bool
     */
    protected function checkEden(int $number): bool
    {
        return $number % 2 == 0;
    }

    /**
     * @param array $group
     * @return bool
     */
    protected function isListNotEmptied(array $group): bool
    {
        return (count($group) > 0);
    }

    /**
     * @param array $gameCompleted
     * @param int $cantidadDeRegistros
     * @return bool
     */
    protected function verifyListEqualsFor(
        array $gameCompleted, int $cantidadDeRegistros): bool
    {
        return count($gameCompleted) == $cantidadDeRegistros;
    }
}
