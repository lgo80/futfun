<?php

namespace App\Service;

use App\Controller\MailerController;
use App\Entity\User;
use App\Repository\ResetPasswordRequestRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class RegistroService extends AbstractBetService
{

    private ResetPasswordRequestRepository $passwordRequestRepository;

    public function __construct(ResetPasswordRequestRepository $passwordRequestRepository)
    {
        $this->passwordRequestRepository = $passwordRequestRepository;
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function generateForConfirmationUser(UserInterface $user, MailerInterface $mailer): string
    {

        $token = $this->passwordRequestRepository
            ->store($user);
        $sendEmail = new MailerController();
        $sendEmail->sendEmail($mailer, $user,
            User::SUBJECT_REGISTRO_CONFIRMACION, $token);

        $session = new Session();

        $session->getFlashBag()->add(
            'exito',
            User::REGISTRO_EXITOSO
        );
        $session->getFlashBag()->add(
            'avisoEmail',
            User::AVISO_EMAIL
        );

        return 'home';
    }
}
