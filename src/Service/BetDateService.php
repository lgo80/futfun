<?php

namespace App\Service;

use App\Entity\BetDate;
use Doctrine\DBAL\Exception as ExceptionAlias;

class BetDateService extends AbstractBetService
{

    private DateService $dateService;

    public function __construct(DateService $dateService)
    {
        $this->dateService = $dateService;
    }

    /**
     * @throws ExceptionAlias
     */
    public function isModifiable(BetDate $betDate): bool
    {
        return $betDate->getBet()->isEditedByDate() && $this->checkDateDate($betDate);
    }

    /**
     * @param BetDate $betDate
     * @return bool
     * @throws ExceptionAlias
     */
    public function checkDateDate(BetDate $betDate): bool // --> falta testear
    {
        return ($betDate->getDate()->getStartDate())
            ? $this->dateService->isDateUnExpired($betDate->getDate())
            : $this->checkGameComplete($betDate);
    }

    /**
     * @param BetDate $betDate
     * @return bool
     */
    private function checkGameComplete(BetDate $betDate): bool
    {
        return !$this->dateService
            ->thereIsSomeCompletedParty($betDate->getDate());
    }

    /**
     */
    public function checkTimeToEdit(): bool
    {
        return false;
    }
}