<?php

namespace App\Service;

use App\Entity\Bet;
use App\Entity\BetDate;
use App\Entity\Date;
use App\Repository\BetDateRepository;
use App\Repository\BetGameRepository;
use DateTimeImmutable;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 *
 */
class BettingPositionsService extends AbstractBetService
{

    private BetGameRepository $betGameRepository;
    private BetDateRepository $betDateRepository;

    public function __construct(BetGameRepository $betGameRepository,
                                BetDateRepository $betDateRepository)
    {
        $this->betGameRepository = $betGameRepository;
        $this->betDateRepository = $betDateRepository;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getPositionsDateShow(Bet $bet, $date): void
    {

        foreach ($bet->getBettingPositions() as $betPosition) {

            $user = $betPosition->getParticipant();

            $betDate = ($this->isCurrentDate($date))
                ? $this->getCurrentDate($bet, $user)
                : $this->getEnteredDate($bet, $date, $user);

            $betGames = $this->betGameRepository
                ->findBy(['betDate' => $betDate]);

            $betPosition->changePositionData($betDate, $betGames);
        }
    }


    private function isCurrentDate($date): bool
    {
        return $date == Date::DATE_ACTUAL;
    }

    private function getEnteredDate(Bet $bet, $date, UserInterface $user): ?BetDate
    {
        return $this->betDateRepository
            ->findOneBy(['bet' => $bet, 'date' => $date, 'user' => $user]);
    }

    /**
     * @throws NonUniqueResultException
     */
    private function getCurrentDate(Bet $bet, UserInterface $user): ?BetDate
    {

        return $this->betDateRepository
            ->findByDateActive($bet, new DateTimeImmutable(), $user);
    }
}
