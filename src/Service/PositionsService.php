<?php

namespace App\Service;

use App\Entity\Game;
use App\Entity\LeaguePositions;
use App\Entity\Team;
use App\Entity\Tournament;
use App\Repository\LeaguePositionsRepository;

/**
 *
 */
class PositionsService extends AbstractBetService
{

    private LeaguePositionsRepository $leaguePositionsRepository;

    public function __construct(LeaguePositionsRepository $leaguePositionsRepository)
    {
        $this->leaguePositionsRepository = $leaguePositionsRepository;
    }

    public function generateLeaguePositions(Game $game)
    {

        $teamLocal = $game->getLocal();
        $teamAway = $game->getAway();
        $tournament = $game->getDateGroup()->getDate()->getTournament();
        $positionsTeamLocal = $this->getTeam(
            $tournament,
            $teamLocal
        );
        $positionsTeamAway = $this->getTeam(
            $tournament,
            $teamAway
        );

        if (!$positionsTeamLocal)
            $this->savePosition($game, $teamLocal);

        if (!$positionsTeamAway)
            $this->savePosition($game, $teamAway);
    }

    private function getTeam(Tournament $tournament, Team $team): ?LeaguePositions
    {
        return $this->leaguePositionsRepository
            ->findOneBySomeField($team, $tournament);
    }

    private function savePosition(Game $game, Team $team): void
    {
        $leaguePositions = new LeaguePositions();
        $leaguePositions->setTeam($team);
        $leaguePositions->setGroupName($game->getDateGroup()->getGroupName());
        $leaguePositions->setTournament($game->getDateGroup()->getDate()->getTournament());
        $leaguePositions->setRound($game->getDateGroup()->getDate()->getRound());
        $this->leaguePositionsRepository
            ->store($leaguePositions);
    }
}
