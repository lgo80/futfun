<?php

namespace App\Service;

use App\Entity\Date;
use App\Entity\Game;
use App\Entity\Round;
use App\Entity\DateGroup;
use App\Entity\RoundLeague;
use App\Entity\RoundElimination;
use App\Logic\Extend\HelperRoundLeague;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ObjectManager;
use App\Logic\Extend\HelperRoundElimination;
use JetBrains\PhpStorm\Pure;

/**
 *
 */
class GameService extends AbstractBetService
{

    private CalendarService $calendarService;

    #[Pure] public function __construct(
        CalendarService $calendarService)
    {
        $this->calendarService = $calendarService;
    }

    /**
     * @throws \Exception
     */
    public function isDateGameUnExpired(Game $game): bool
    {
        return $this->calendarService->isTheDateGreaterThan(
            $game->getDateMatchAt(), $this->calendarService->getDateToday());
    }

    public function generateGameBack(DateGroup $dateGroupBack, Game $game): ?Game
    {
        $gameBack = new Game();
        $gameBack->setLocal($game->getAway());
        $gameBack->setAway($game->getLocal());
        $gameBack->setDateGroup($dateGroupBack);
        $gameBack->setNumber($game->getNumber());
        $gameBack->setType($game->getType());
        return $gameBack;
    }
}
