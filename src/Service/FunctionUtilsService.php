<?php

namespace App\Service;


use DateTime;

/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class FunctionUtilsService
{

    private function devolverNombresEspanol(): array
    {

        $array_Respuesta = [];
        $array_dia = [];
        $array_mes = [];

        $array_dia[1] = "Lunes";
        $array_dia[2] = "Martes";
        $array_dia[3] = utf8_encode("Mi�rcoles");
        $array_dia[4] = "Jueves";
        $array_dia[5] = "Viernes";
        $array_dia[6] = utf8_encode("S�bado");
        $array_dia[7] = "Domingo";

        $array_Respuesta["dia"] = $array_dia;

        $array_mes["01"] = "Enero";
        $array_mes["02"] = "Febrero";
        $array_mes["03"] = "Marzo";
        $array_mes["04"] = "Abril";
        $array_mes["05"] = "Mayo";
        $array_mes["06"] = "Junio";
        $array_mes["07"] = "Julio";
        $array_mes["08"] = "Agosto";
        $array_mes["09"] = "Septiembre";
        $array_mes["10"] = "Octubre";
        $array_mes["11"] = "Noviembre";
        $array_mes["12"] = "Diciembre";

        $array_Respuesta["mes"] = $array_mes;

        return $array_Respuesta;
    }

    private function generateDateSpanish(
        $diaSemana, $diaNumero, $mes, $anno, $hora): string
    {

        $array_spanish = $this->devolverNombresEspanol();

        return $array_spanish["dia"][$diaSemana] . " " .
            $diaNumero . " de " . $array_spanish["mes"][$mes]
            . " del " . $anno . " a las " . $hora;

    }

    public function formattedDateSpanishOnly($date): ?string
    {

        return ($date != null)
            ? $this->generateDateSpanish(
                $date->format('N'), $date->format('d'),
                $date->format('m'), $date->format('Y'),
                $date->format('H:i:s')
            ) : null;

    }

    public function generateRandomString($length): string
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

}