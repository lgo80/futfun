<?php

namespace App\Service;

use DateInterval;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Exception;

/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class CalendarService
{

    private DateTimeZone $dtz;

    function __construct()
    {
        $this->dtz = new DateTimeZone("America/Argentina/Buenos_Aires");
    }

    /**
     * @return DateTime
     * @throws Exception
     */
    public function getDateToday(): DateTime
    {
        return new DateTime("now", $this->dtz);
    }

    /**
     * @param int $day
     * @param int $month
     * @param int $year
     * @param int|null $hour
     * @param int|null $minute
     * @param int|null $second
     * @return DateTime
     * @throws Exception
     */
    public function getSpecificDate(int  $day, int $month, int $year,
                                    ?int $hour = 0, ?int $minute = 0, ?int $second = 0): DateTime
    {
        return new DateTime($day . '-' . $month . '-' . $year . 'T' . $hour . ':' . $minute . ':' . $second, $this->dtz);
    }

    /**
     * @throws Exception
     */
    public function getSumDays(int $day): DateTime
    {
        return $this->getDateToday()->add(new DateInterval('P' . $day . 'D'));
    }

    /**
     * @throws Exception
     */
    public function getSubtractDays(int $day): DateTime
    {
        return $this->getDateToday()->sub(new DateInterval('P' . $day . 'D'));
    }

    /**
     * @throws Exception
     */
    public function getSumMonth(int $month): DateTime
    {
        return $this->getDateToday()->add(new DateInterval('P' . $month . 'M'));
    }

    /**
     * @throws Exception
     */
    public function getSubtractMonth(int $month): DateTime
    {
        return $this->getDateToday()->sub(new DateInterval('P' . $month . 'M'));
    }

    /**
     * @throws Exception
     */
    public function getSumHour(int $hour): DateTime
    {
        return $this->getDateToday()->add(new DateInterval('PT' . $hour . 'H'));
    }

    /**
     * @throws Exception
     */
    public function getSubtractHour(int $hour): DateTime
    {
        return $this->getDateToday()->sub(new DateInterval('PT' . $hour . 'H'));
    }

    public function isTheDateGreaterThan(DateTimeInterface $pastDate, DateTimeInterface $nextDate): bool
    {
        return $pastDate->format('Y-m-d H:i:s') < $nextDate->format('Y-m-d H:i:s');
    }

    public function isDateNotNull(DateTimeInterface $date): DateTimeInterface
    {
        return $date;
    }
}
