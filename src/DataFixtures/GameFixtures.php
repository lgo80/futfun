<?php

namespace App\DataFixtures;

use App\Entity\DateGroup;
use App\Entity\Game;
use App\Entity\Team;
use App\Service\CalendarService;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class GameFixtures extends Fixture
{

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {

        $calendar = new CalendarService();
        /*
         * Es para la ronda con el torneo 2 ≥ 'Torneo tipo liga con fechas, pero no todas'
         * date = 1(Con fecha no vencida) - Fecha 1 , Solo un grupo (id = 1)
         * id = 1 y 2
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            1, 2, null, null,
            null, 1);
        $this->generateGame($manager, 1, 2, 3, 4,
            3, 0, null, null,
            null, 1);

        /*
         * Es para la ronda con el torneo 2 -> 'Torneo tipo liga con fechas pero no todas'
         * date = 2(Con fecha no vencida) - Fecha 2, Solo un grupo (id = 2)
         * id = 3 y 4
         */
        $this->generateGame($manager, 1, 1, 1, 3,
            1, 2, null, null,
            null, 2);
        $this->generateGame($manager, 1, 2, 2, 4,
            null, -1, null, null,
            null, 2);

        /*
         * Es para la ronda con el torneo 2 ≥ 'Torneo tipo liga con fechas pero no todas'
         * date = 3(Con fecha vencida) - Fecha 3, Solo un grupo (id = 3)
         * id = 5 y 6
         */
        $this->generateGame($manager, 1, 1, 1, 4,
            -1, -1, null, null,
            null, 3);
        $this->generateGame($manager, 1, 2, 2, 3,
            null, null, null, null,
            null, 3);

        /*
         * Es para el torneo 3 ≥ 'Torneo tipo liga con todas fechas'
         * date = 4(Sin fecha) - Fecha 1, Solo un grupo (id = 4)
         * id = 7 y 8
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            1, 2, null, null,
            null, 4);
        $this->generateGame($manager, 1, 2, 3, 4,
            3, 0, null, null,
            null, 4);

        /*
         * Es para el torneo 3 ≥ 'Torneo tipo liga con todas fechas'
         * date = 5(Con fecha vencida) - Fecha 2, Solo un grupo (id = 5)
         * id = 9 y 10
         */
        $this->generateGame($manager, 1, 1, 1, 3,
            1, 2, null, null,
            null, 5);
        $this->generateGame($manager, 1, 2, 2, 4,
            null, null, null, null,
            null, 5);

        /*
         * Es para el torneo 3 ≥ 'Torneo tipo liga con todas fechas'
         * date = 6(Sin fecha) - Fecha 3, Solo un grupo (id = 6)
         * id = 11 y 12
         */
        $this->generateGame($manager, 1, 1, 1, 4,
            null, null, null, null,
            null, 6);
        $this->generateGame($manager, 1, 2, 2, 3,
            null, null, null, null,
            null, 6);

        /*
         * Es para el torneo 4 ≥ 'Torneo tipo liga y eliminación sin todas fechas'
         * date = 7(Sin fecha) - Fecha 1, - Grupo A (id = 7)
         * id = 13 y 14
         */
        $this->generateGame($manager, 1, 1, 1, 4,
            2, 1, null, null,
            null, 7);
        $this->generateGame($manager, 1, 2, 2, 3,
            3, 0, null, null,
            null, 7);

        /*
         * Es para el torneo 4 ≥ 'Torneo tipo liga y eliminación sin todas fechas'
         * date = 7(Sin fecha) - Fecha 1, - Grupo B (id = 8)
         * id = 15 y 16
         */
        $this->generateGame($manager, 1, 1, 5, 8,
            2, 1, null, null,
            null, 8);
        $this->generateGame($manager, 1, 2, 6, 7,
            3, 0, null, null,
            null, 8);

        /*
         * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * date = 8(Sin fecha) - Fecha 2, - Grupo A (id = 9)
         * id = 17 y 18
         */
        $this->generateGame($manager, 1, 1, 1, 3,
            2, 1, null, null,
            null, 9);
        $this->generateGame($manager, 1, 2, 2, 4,
            2, 0, null, null,
            null, 9);

        /*
         * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * date = 8(Sin fecha) - Fecha 2, - Grupo B (id = 10)
         * id = 19 y 20
         */
        $this->generateGame($manager, 1, 1, 5, 7,
            null, null, null, null,
            null, 10);
        $this->generateGame($manager, 1, 2, 6, 8,
            null, null, null, null,
            null, 10);

        /*
         * Es para el torneo 4 ≥ 'Torneo tipo liga y eliminación sin todas fechas'
         * date = 9(Sin fecha) - Fecha 3, - Grupo A (id = 11)
         * id = 21 y 22
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            null, null, null, null,
            null, 11);
        $this->generateGame($manager, 1, 2, 4, 3,
            null, null, null, null,
            null, 11);

        /*
         * Es para el torneo 4 ≥ 'Torneo tipo liga y eliminación sin todas fechas'
         * date = 9(Sin fecha) - Fecha 3, - Grupo B (id = 12)
         * id = 23 y 24
         */
        $this->generateGame($manager, 1, 1, 5, 6,
            null, null, null, null,
            null, 12);
        $this->generateGame($manager, 1, 2, 7, 8,
            null, null, null, null,
            null, 12);

        /*
         * Es para el torneo 4 ≥ 'Torneo tipo liga y eliminación sin todas fechas'
         * date = 10(Sin fecha) - Fecha Semifinal ida, - 1 solo grupo (id = 13) - Ronda eliminación
         * id = 25 y 26
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            2, 1, null, null,
            null, 13);
        $this->generateGame($manager, 1, 2, 3, 4,
            3, 0, null, null,
            null, 13);

        /*
         * Es para el torneo 4 ≥ 'Torneo tipo liga y eliminación sin todas fechas'
         * date = 11(Sin fecha) - Fecha semifinal vuelta, - 1 solo grupo (id = 14) - Ronda eliminación
         * id = 27 y 28
         */
        $this->generateGame($manager, 1, 1, 2, 1,
            2, 1, null, null,
            null, 14);
        $this->generateGame($manager, 1, 2, 4, 3,
            3, 0, null, null,
            null, 14);

        /*
         * Es para el torneo 4 ≥ 'Torneo tipo liga y eliminación sin todas fechas'
         * date = 12(Sin fecha) - Fecha final ida, - 1 solo grupo (id = 15) - Ronda eliminación
         * id = 29 y 30
         */
        $this->generateGame($manager, 1, 1, 2, 3,
            2, 1, null, null,
            null, 15);
        $this->generateGame($manager, 1, 2, 4, 1,
            null, null, null, null,
            null, 15);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * date = 13(Sin fecha) - Fecha 1, - Grupo A (id = 16)
         * id = 31 y 32
         */
        $this->generateGame($manager, 1, 1, 1, 4,
            2, 1, null, null,
            null, 16);
        $this->generateGame($manager, 1, 2, 2, 3,
            3, 0, null, null,
            null, 16);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * date = 13(Sin fecha) - Fecha 1, - Grupo B (id = 17)
         * id = 33 y 34
         */
        $this->generateGame($manager, 1, 1, 5, 8,
            2, 1, null, null,
            null, 17);
        $this->generateGame($manager, 1, 2, 6, 7,
            3, 0, null, null,
            null, 17);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * date = 14(Sin fecha) - Fecha 2, - Grupo A (id = 18)
         * id = 35 y 36
         */
        $this->generateGame($manager, 1, 1, 1, 3,
            null, null, null, null,
            $calendar->getSumHour(1), 18);
        $this->generateGame($manager, 1, 2, 2, 4,
            null, null, null, null,
            $calendar->getSubtractHour(1), 18);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * date = 14(Sin fecha) - Fecha 2, - Grupo B (id = 19)
         * id = 37 y 38
         */
        $this->generateGame($manager, 1, 1, 5, 7,
            2, 1, null, null,
            $calendar->getSumHour(1), 19);
        $this->generateGame($manager, 1, 2, 6, 8,
            3, 0, null, null,
            $calendar->getSubtractHour(1), 19);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * date = 15(Sin fecha) - Fecha 3, - Grupo A (id = 20)
         * id = 39 y 40
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            null, null, null, null,
            null, 20);
        $this->generateGame($manager, 1, 2, 4, 3,
            3, 0, null, null,
            null, 20);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * date = 15(Sin fecha) - Fecha 3, - Grupo B (id = 21)
         * id = 41 y 42
         */
        $this->generateGame($manager, 1, 1, 5, 6,
            2, 1, null, null,
            null, 21);
        $this->generateGame($manager, 1, 2, 7, 8,
            3, 0, null, null,
            null, 21);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * date = 16(Sin fecha) - Fecha Semifinal solo ida, - 1 solo grupo (id = 22)
         * id = 43 y 44
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            2, 1, null, null,
            null, 22);
        $this->generateGame($manager, 1, 2, 3, 4,
            3, 0, null, null,
            null, 22);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * date = 17(Sin fecha) - Fecha final solo ida, - 1 solo grupo (id = 23)
         * id = 45
         */
        $this->generateGame($manager, 1, 1, 1, 3,
            2, 0, null, null,
            null, 23);

        /*
         * Es para la ronda con el torneo 6 ≥ 'Torneo tipo eliminación sin todas fechas'
         * date = 18(Sin fecha) - Fecha semifinal ida, - 1 solo grupo (id = 24)
         * id = 46 y 47
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            1, 2, null, null,
            null, 24);
        $this->generateGame($manager, 1, 2, 3, 4,
            3, 0, null, null,
            null, 24);

        /*
         * Es para la ronda con el torneo 6 ≥ 'Torneo tipo eliminación sin todas fechas'
         * date = 19(Sin fecha) - Fecha semifinal vuelta, - 1 solo grupo (id = 25)
         * id = 48 y 49
         */
        $this->generateGame($manager, 1, 1, 2, 1,
            1, 2, null, null,
            null, 25);
        $this->generateGame($manager, 1, 2, 4, 3,
            null, null, null, null,
            null, 25);

        /*
         * Es para la ronda con el torneo 7 ≥ 'Torneo tipo eliminación con todas fechas'
         * date = 20(Sin fecha) - Fecha semifinal solo ida, - 1 solo grupo (id = 26)
         * id = 50 y 51
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            1, 2, null, null,
            null, 26);
        $this->generateGame($manager, 1, 2, 3, 4,
            3, 0, null, null,
            null, 26);

        /*
         * Es para la ronda con el torneo 7 ≥ 'Torneo tipo eliminación con todas fechas'
         * date = 21(Sin fecha) - Fecha final solo ida, - 1 solo grupo (id = 27)
         * id = 52
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            1, 2, null, null,
            null, 27);

        /*
         * Es para la ronda con el torneo 8 → 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * date = 22(Sin fecha) - Fecha 1, - grupo A (id = 28)
         * id = 53 y 54
         */
        $this->generateGame($manager, 1, 1, 1, 2,
            1, 2, null, null,
            null, 28);
        $this->generateGame($manager, 1, 2, 3, 4,
            null, null, null, null,
            null, 28);

        /*
         * Es para la ronda con el torneo 8 → 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * date = 22(Sin fecha) - Fecha 1, - grupo B (id = 29)
         * id = 55 y 56
         */
        $this->generateGame($manager, 1, 1, 5, 6,
            null, null, null, null,
            null, 29);
        $this->generateGame($manager, 1, 2, 7, 8,
            null, null, null, null,
            null, 29);

        /*
         * Es para la ronda con el torneo 8 → 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * date = 22(Sin fecha) - Fecha 1, - grupo C (id = 30)
         * id = 57 y 58
         */
        $this->generateGame($manager, 1, 1, 9, 10,
            null, null, null, null,
            null, 30);
        $this->generateGame($manager, 1, 2, 11, 12,
            null, null, null, null,
            null, 30);

        /*
         * Es para la ronda con el torneo 8 → 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * date = 23(Sin fecha) - Fecha 1, - grupo C (id = 31)
         * id = 59
         */
        $this->generateGame($manager, 1, 1, 9, 10,
            null, null, null, null,
            null, 31);

        $manager->flush();
    }

    private function generateGame(ObjectManager $manager, int $type,
                                  int           $numberGame, int $teamLocal_id,
                                  int           $teamAway_id, ?int $localValue,
                                  ?int          $awayValue, ?int $localDefinitionValue,
                                  ?int          $awayDefinitionValue, ?DateTime $dateMatch,
                                  int           $dateGroup_id): void
    {
        $game = new Game();
        $game->setType($type);
        $game->setNumber($numberGame);
        $teamLocal = $manager->find(Team::class, $teamLocal_id);
        $game->setLocal($teamLocal);
        $teamAway = $manager->find(Team::class, $teamAway_id);
        $game->setAway($teamAway);
        $game->setLocalValue($localValue);
        $game->setAwayValue($awayValue);
        $game->setLocalDefinitionValue($localDefinitionValue);
        $game->setAwayDefinitionValue($awayDefinitionValue);
        $game->setDateMatchAt($dateMatch);
        $dateGroup = $manager->find(DateGroup::class, $dateGroup_id);
        $game->setDateGroup($dateGroup);
        $manager->persist($game);
    }
}