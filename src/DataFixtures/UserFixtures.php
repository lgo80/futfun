<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{

    private UserPasswordHasherInterface $hashed;

    public function __construct(UserPasswordHasherInterface $hashed)
    {
        $this->hashed = $hashed;
    }

    public function load(ObjectManager $manager): void
    {

        $this->generateUser($manager, 'lgo80', 'Boca_2018', User::ROLE_ADMIN, 'lgo80@yahoo.com.ar', true);
        $this->generateUser($manager, 'Iiporra', '1234', User::ROLE_USER, 'soliris14@hotmail.com', true);
        $this->generateUser($manager, 'mdo1978', '1234', User::ROLE_USER, 'mdo1978@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_1', '1234', User::ROLE_USER, 'user_1@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_2', '1234', User::ROLE_USER, 'user_2@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_3', '1234', User::ROLE_USER, 'user_3@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_4', '1234', User::ROLE_USER, 'user_4@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_5', '1234', User::ROLE_USER, 'user_5@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_6', '1234', User::ROLE_USER, 'user_6@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_7', '1234', User::ROLE_USER, 'user_7@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_8', '1234', User::ROLE_USER, 'user_8@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_9', '1234', User::ROLE_USER, 'user_9@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_10', '1234', User::ROLE_USER, 'user_10@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_11', '1234', User::ROLE_USER, 'user_11@yahoo.com.ar', true);
        $this->generateUser($manager, 'user_12', '1234', User::ROLE_USER, 'user_12@yahoo.com.ar', true);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param string $username
     * @param string $pass
     * @param string $role
     * @param string $email
     * @param bool $isActive
     * @return void
     */
    private function generateUser(ObjectManager $manager, string $username,
                                  string        $pass, string $role,
                                  string        $email, bool $isActive): void
    {
        $user = new User();
        $user->setUsername($username);
        $password = $this->hashed->hashPassword($user, $pass);
        $user->setPassword($password);
        $user->setRoles([$role]);
        $user->setEmail($email);
        $user->setIsActive($isActive);
        $manager->persist($user);
    }
}
