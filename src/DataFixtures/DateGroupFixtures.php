<?php

namespace App\DataFixtures;

use App\Entity\Date;
use App\Entity\DateGroup;
use App\Entity\Group;
use App\Entity\Team;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DateGroupFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        /*
         * Es para la ronda con el torneo 2 -> 'Torneo tipo liga con fechas pero no todas'
         * Para los date con id = 1, 2 y 3
         * partidos= 1, 2, 3, 4, 5 y 6
         * id = 1, 2 y 3
         * Es una liga - Cantidad de grupos = 1
         * El primer Date Todos los partidos con resultados el resto no
         */
        $this->generateDateGroup($manager, 1, null, null);
        $this->generateDateGroup($manager, 2, null, null);
        $this->generateDateGroup($manager, 3, null, null);

        /*
         * Es para el torneo 3 -> 'Torneo tipo liga con todas fechas'
         * Para los date con id = 4, 5 y 6
         * partidos= 7, 8, 9, 10, 11 y 12
         * id = 4, 5 y 6
         * Es una liga - Cantidad de grupos = 1
         * Todos los partidos con resultados
         */
        $this->generateDateGroup($manager, 4, null, null);
        $this->generateDateGroup($manager, 5, null, null);
        $this->generateDateGroup($manager, 6, null, null);

        /*
         * Es para el torneo 4 -> 'Torneo tipo liga y eliminacion sin todas fechas'
         * Para los date con id = 7, 8 y 9
         * id = 7, 8, 9, 10, 11 y 12
         * partidos = 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 y 24
         * Es una liga - Cantidad de grupos = 2
         * El primer Date Todos los partidos con resultados el resto no
         */
        $this->generateDateGroup($manager, 7, null, null);
        $this->generateDateGroup($manager, 7, null, null);
        $this->generateDateGroup($manager, 8, null, null);
        $this->generateDateGroup($manager, 8, null, null);
        $this->generateDateGroup($manager, 9, null, null);
        $this->generateDateGroup($manager, 9, null, null);

        /*
         * Es para el torneo 4 -> 'Torneo tipo liga y eliminacion sin todas fechas'
         * Para los date con id = 10, 11 y 12
         * id = 13, 14 y 15
         * partidos = 25, 26, 27, 28, 29 y 30
         * Es una eliminacion - Cantidad de grupos = 1
         * El primer Date Todos los partidos con resultados el resto no
         */
        $this->generateDateGroup($manager, 10, null, null);
        $this->generateDateGroup($manager, 11, null, null);
        $this->generateDateGroup($manager, 12, null, null);

        /*
         * Es para el torneo 5 -> 'Torneo tipo liga y eliminacion con todas fechas'
         * Para los date con id = 13, 14 y 15
         * id = 16, 17, 18, 19, 20 y 21
         * Es una liga - Cantidad de grupos = 2
         * El primer Date Todos los partidos con resultados el resto no
         */
        $this->generateDateGroup($manager, 13, null, null);
        $this->generateDateGroup($manager, 13, null, null);
        $this->generateDateGroup($manager, 14, null, null);
        $this->generateDateGroup($manager, 14, null, null);
        $this->generateDateGroup($manager, 15, null, null);
        $this->generateDateGroup($manager, 15, null, null);

        /*
         * Es para el torneo 5 -> 'Torneo tipo liga y eliminacion con todas fechas'
         * Para los date con id = 16, 17
         * id = 22 y 23
         * Es una eliminacion - Cantidad de grupos = 1
         * El primer Date Todos los partidos con resultados el resto no
         */
        $this->generateDateGroup($manager, 16, null, null);
        $this->generateDateGroup($manager, 17, null, null);

        /*
         * Es para el torneo 6 -> 'Torneo tipo eliminacion sin todas fechas'
         * Para los date con id = 18 y 19
         * id = 24 y 25
         * Es una eliminacion - Cantidad de grupos = 1
         * El primer Date Todos los partidos con resultados el resto no
         */
        $this->generateDateGroup($manager, 18, null, null);
        $this->generateDateGroup($manager, 19, null, null);

        /*
         * Es para el torneo 7 → 'Torneo tipo eliminación con todas fechas'
         * Para los date con id = 20 y 21
         * id = 26 y 27
         * Es una eliminacion - Cantidad de grupos = 1
         * El primer Date Todos los partidos con resultados el resto no
         */
        $this->generateDateGroup($manager, 20, null, null);
        $this->generateDateGroup($manager, 21, null, null);

        /*
         * Es para el torneo 8 → 'Torneo tipo eliminación con todas fechas'
         * Para los date con ID = 22 y 23
         * id = 28, 29, 30, 31, 32 y 33 - Es una liga - Cantidad de grupos = 3
         * El primer Date Todos los partidos con resultados el resto no
         */
        $this->generateDateGroup($manager, 22, null, null);
        $this->generateDateGroup($manager, 22, null, null);
        $this->generateDateGroup($manager, 22, null, null);
        $this->generateDateGroup($manager, 23, null, null);
        $this->generateDateGroup($manager, 23, null, null);
        $this->generateDateGroup($manager, 23, null, null);

        $manager->flush();
    }

    private function generateDateGroup(ObjectManager $manager, int $date_id,
                                       ?Team         $freeTeam, ?Group $group): void
    {
        $dateGroup = new DateGroup();
        $date = $manager->find(Date::class, $date_id);
        $dateGroup->setDate($date);
        $dateGroup->setFreeTeam($freeTeam);
        $dateGroup->setGroupName($group);
        $manager->persist($dateGroup);
    }
}