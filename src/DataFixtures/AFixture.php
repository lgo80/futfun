<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AFixture extends Fixture implements DependentFixtureInterface
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        // TODO: Implement load() method.
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            TeamFixtures::class,
            TournamentFixtures::class,
            RoundFixtures::class,
            RoundLeagueFixtures::class,
            RoundEliminationFixtures::class,
            DateFixtures::class,
            DateGroupFixtures::class,
            GameFixtures::class,
            BetFixtures::class,
            BetDateFixtures::class,
            BetGameFixtures::class
        ];
    }
}