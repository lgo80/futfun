<?php

namespace App\DataFixtures;

use App\Entity\Bet;
use App\Entity\Tournament;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BetFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        /*
         * Es para el torneo 1 → 'Torneo sin fechas'
         * id = 1 - Se puede modificar por comienzo de la fecha
         */
        $this->generateBet($manager, "Apuesta 1", 1, 1,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        /*
         * Es para el torneo 2 → 'Torneo tipo liga con fechas, pero no todas'
         * id = 2 - Se puede modificar por comienzo de la fecha
         */
        $this->generateBet($manager, "Apuesta 2", 2, 1,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        /*
         * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
         * id = 3 - Se puede modificar por comienzo de la fecha
         */
        $this->generateBet($manager, "Apuesta 3a", 3, 1,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        /*
         * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
         * id = 4 - Se puede modificar por comienzo de los partidos
         */
        $this->generateBet($manager, "Apuesta 3b", 3, 2,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        /*
         * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * id = 5 - Se puede modificar por comienzo de la fecha
         */
        $this->generateBet($manager, "Apuesta 5", 4, 1,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        /*
         * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
         * id = 6 - Se puede modificar por comienzo de los partidos
         */
        $this->generateBet($manager, "Apuesta 6", 5, 2,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        /*
         * Es para el torneo 6 → 'Torneo tipo eliminación sin todas fechas'
         * id = 7 - Se puede modificar por comienzo de las fechas
         */
        $this->generateBet($manager, "Apuesta 7", 6, 1,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        /*
         * Es para el torneo 7 → 'Torneo tipo eliminación sin todas fechas'
         * id = 8 - Se puede modificar por comienzo de los partidos
         */
        $this->generateBet($manager, "Apuesta 8", 7, 2,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        /*
         * Es para el torneo 8 → 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * id = 9 - Se puede modificar por comienzo de la Fecha
         */
        $this->generateBet($manager, "Apuesta 9", 8, 1,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        /*
         * Es para el torneo 8 → 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * id = 10 - Se puede modificar por comienzo de los partidos
         */
        $this->generateBet($manager, "Apuesta 10", 8, 2,
            null, new DateTime(), 3, 1,
            0, 1, 100);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param string $name
     * @param int $tournament_id
     * @param int $expirationRate
     * @param User|null $champion
     * @param DateTime $dateExtraLimitAt
     * @param int $pointsForExactGames
     * @param int $pointsByResultOnly
     * @param int $pointsPerMissedGame
     * @param int $state
     * @param $symbolicBet
     * @return void
     */
    private function generateBet(ObjectManager $manager, string $name, int $tournament_id,
                                 int           $expirationRate, ?User $champion,
                                 DateTime      $dateExtraLimitAt, int $pointsForExactGames,
                                 int           $pointsByResultOnly, int $pointsPerMissedGame,
                                 int           $state, $symbolicBet): void
    {
        $bet = new Bet();
        $bet->setName($name);
        $tournament = $manager->find(Tournament::class, $tournament_id);
        $bet->setTournament($tournament);
        $bet->setExpirationRate($expirationRate);
        $bet->setChampion($champion);
        $bet->setDateExtraLimitAt($dateExtraLimitAt);
        $bet->setPointsForExactGames($pointsForExactGames);
        $bet->setPointsByResultOnly($pointsByResultOnly);
        $bet->setPointsPerMissedGame($pointsPerMissedGame);
        $bet->setState($state);
        $bet->setSymbolicBet($symbolicBet);
        $manager->persist($bet);
    }
}