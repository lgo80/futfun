<?php

namespace App\DataFixtures;

use App\Entity\Team;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TeamFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create 20 products! Bam!
        for ($i = 1; $i < 21; $i++) {
            $team = new Team();
            $team->setName('equipo ' . $i);
            $manager->persist($team);
        }

        $manager->flush();
    }
}
