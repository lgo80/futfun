<?php

namespace App\DataFixtures;

use App\Entity\Date;
use App\Entity\Round;
use App\Entity\Tournament;
use App\Service\CalendarService;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;

class DateFixtures extends Fixture
{

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {

        $calendar = new CalendarService();
        /*
         * Es para la ronda con el torneo 2 ≥ 'Torneo tipo liga con fechas, pero no todas'
         * id = 1(Con fecha no vencida), 2(Con fecha no vencida) y 3 (Con fecha vencida)
         * date-group = 1, 2 y 3 - Es una liga - Cantidad de grupos = 1
         * partidos = 1 (Con resultado), 2(Con resultado), 3(Con resultado), 4(Sin resultado),
         * 5(sin resultado) y 6(sin resultado)
         */
        $this->generateDate($manager, '1', 1, 2,
            $calendar->getSumHour(1), Date::TYPE_TOURNAMENT, 1, 2);
        $this->generateDate($manager, '2', 2, 2,
            $calendar->getSumHour(1), Date::TYPE_TOURNAMENT, 1, 2);
        $this->generateDate($manager, '3', 3, 2,
            $calendar->getSubtractHour(1), Date::TYPE_TOURNAMENT, 1, 2);

        /*
         * Es para el torneo 3 ≥ 'Torneo tipo liga con todas fechas'
         * id = 4(Sin fecha), 5(Con fecha vencida) y 6 (Sin fecha)
         * date-group = 4, 5 y 6 - Es una liga - Cantidad de grupos = 1
         * partidos = 7 (Con resultado), 8(Con resultado), 9(Con resultado), 10(Sin resultado),
         * 11(sin resultado) y 12(sin resultado)
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 2, 3);
        $this->generateDate($manager, '2', 2, 2,
            $calendar->getSumHour(1), Date::TYPE_TOURNAMENT, 2, 3);
        $this->generateDate($manager, '3', 3, 2,
            null, Date::TYPE_TOURNAMENT, 2, 3);

        /*
         * Es para el torneo 4 ≥ 'Torneo tipo liga y eliminación sin todas fechas'
         * id = 7(Sin fecha), 8(fecha no vencida) y 9(Sin fecha)
         * date-group = 7, 8, 9, 10, 11 y 12 - Es una liga - Cantidad de grupos = 2
         * partidos = 13(Con resultado), 14(Con resultado), 15(Con resultado), 16(Con resultado),
         * 17(Con resultado), 18(Con resultado), 19(Con resultado), 20(sin resultado),
         * 21(sin resultado), 22(sin resultado), 23(sin resultado), 24(sin resultado)
         */
        $this->generateDate($manager, '1', 1, 4,
            null, Date::TYPE_TOURNAMENT, 3, 4);
        $this->generateDate($manager, '2', 2, 4,
            $calendar->getSumHour(1), Date::TYPE_TOURNAMENT, 3, 4);
        $this->generateDate($manager, '3', 3, 4,
            null, Date::TYPE_TOURNAMENT, 3, 4);

        /*
         * Es para el torneo 4 ≥ 'Torneo tipo liga y eliminación sin todas fechas'
         * id = 10(Sin fecha), 11(Sin fecha) y 12(Sin fecha)
         * date-group = 13, 14 y 15 - Es una eliminación - Cantidad de grupos = 1
         * partidos = 25(Con resultado), 26(Con resultado), 27(Con resultado),
         * 28(Con resultado), 29(Con resultado), 30(Sin resultado)
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 4, 4);
        $this->generateDate($manager, '2', 2, 2,
            null, Date::TYPE_TOURNAMENT, 4, 4);
        $this->generateDate($manager, '3', 3, 2,
            null, Date::TYPE_TOURNAMENT, 4, 4);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * id = 13(Sin fecha), 14(Sin fecha) y 15(Sin fecha)
         * date-group = 16, 17, 18, 19, 20 y 21 - Es una liga - Cantidad de grupos = 2
         * partidos = 31(Con resultado), 32(Con resultado), 33(Con resultado),
         * 34(Con resultado), 35(Con resultado), 36(Con resultado),37(Con resultado),
         * 38(Con resultado), 39(Con resultado), 40(Con resultado), 41(Con resultado),
         * 42(Con resultado)
         */
        $this->generateDate($manager, '1', 1, 4,
            null, Date::TYPE_TOURNAMENT, 5, 5);
        $this->generateDate($manager, '2', 2, 4,
            null, Date::TYPE_TOURNAMENT, 5, 5);
        $this->generateDate($manager, '3', 3, 4,
            null, Date::TYPE_TOURNAMENT, 5, 5);

        /*
         * Es para el torneo 5 ≥ 'Torneo tipo liga y eliminación con todas fechas'
         * id = 16(Sin fecha), 17(Sin fecha)
         * date-group = 22 y 23 - Es una eliminación - Cantidad de grupos = 1
         * partidos = 43(Con resultado), 44(Con resultado), 45(Con resultado)
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 6, 5);
        $this->generateDate($manager, '2', 2, 1,
            null, Date::TYPE_TOURNAMENT, 6, 5);

        /*
         * Es para el torneo 6 ≥ 'Torneo tipo eliminación sin todas fechas'
         * id = 18(Sin fecha), 19(Sin fecha)
         * date-group = 24 y 25 - Es una eliminación - Cantidad de grupos = 1
         * partidos = 46(Con resultado), 47(Con resultado), 48(Con resultado),
         * 49(Sin resultado)
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 7, 6);
        $this->generateDate($manager, '2', 2, 2,
            null, Date::TYPE_TOURNAMENT, 7, 6);

        /*
         * Es para el torneo 7 ≥ 'Torneo tipo eliminación con todas fechas'
         * id = 20(Sin fecha), 21(Sin fecha)
         * date-group = 26 y 27 - Es una eliminación - Cantidad de grupos = 1
         * partidos = 50(Con resultado), 51(Con resultado), 52(Con resultado)
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 8, 7);
        $this->generateDate($manager, '2', 2, 1,
            null, Date::TYPE_TOURNAMENT, 8, 7);

        /*
         * Es para el torneo 8 ≥ 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * id = 22(Sin fecha), 23(Sin fecha), 24(Sin Fecha) y  25(sin fecha)
         * date-group = 28, 29, 30, 31, 32 y 33 - Es una liga - Cantidad de grupos = 3
         * partidos = 53(Con resultado), 54(Con resultado), 55(Con resultado)
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 9, 8);
        $this->generateDate($manager, '2', 2, 1,
            null, Date::TYPE_TOURNAMENT, 9, 8);
        $this->generateDate($manager, '3', 3, 2,
            null, Date::TYPE_TOURNAMENT, 9, 8);
        $this->generateDate($manager, '4', 4, 1,
            null, Date::TYPE_TOURNAMENT, 9, 8);

        /*
         * Es para el torneo 8 ≥ 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * id = 26(Sin fecha), 27(Sin fecha), 28(Sin fecha)
         * date-group = 28, 29, 30, 31, 32 y 33 - Es una liga - Cantidad de grupos = 3
         * partidos = 53(Con resultado), 54(Con resultado), 55(Con resultado)
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 10, 8);
        $this->generateDate($manager, '2', 2, 2,
            null, Date::TYPE_TOURNAMENT, 10, 8);
        $this->generateDate($manager, '3', 3, 2,
            null, Date::TYPE_TOURNAMENT, 10, 8);

        /*
         * Es para el torneo 8 ≥ 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * id = 29(Sin fecha), 30(Sin fecha), 31(Sin fecha)
         * date-group =  - Es una Eliminacion
         * partidos =
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 15, 8);
        $this->generateDate($manager, '2', 2, 2,
            null, Date::TYPE_TOURNAMENT, 15, 8);
        $this->generateDate($manager, '3', 3, 2,
            null, Date::TYPE_TOURNAMENT, 15, 8);

        /*
         * Es para el torneo 8 ≥ 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * id = 32(Sin fecha), 33(Sin fecha), 34(Sin fecha)
         * date-group =  - Es una Eliminacion
         * partidos =
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 16, 8);
        $this->generateDate($manager, '2', 2, 2,
            null, Date::TYPE_TOURNAMENT, 16, 8);
        $this->generateDate($manager, '3', 3, 2,
            null, Date::TYPE_TOURNAMENT, 16, 8);

        /*
         * Es para el torneo 8 ≥ 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
         * id = 35(Sin fecha), 36(Sin fecha), 37(Sin fecha) y 38(sin fecha)
         * date-group =  - Es una Eliminacion
         * partidos =
         */
        $this->generateDate($manager, '1', 1, 2,
            null, Date::TYPE_TOURNAMENT, 17, 8);
        $this->generateDate($manager, '2', 2, 2,
            null, Date::TYPE_TOURNAMENT, 17, 8);
        $this->generateDate($manager, '3', 3, 2,
            null, Date::TYPE_TOURNAMENT, 17, 8);
        $this->generateDate($manager, '4', 4, 2,
            null, Date::TYPE_TOURNAMENT, 17, 8);


        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param string $name
     * @param int $number
     * @param int $numberOfGames
     * @param DateTime|null $startDate
     * @param string $type
     * @param int $round_id
     * @param int $tournament_id
     * @return void
     */
    private function generateDate(ObjectManager $manager, string $name, int $number,
                                  int           $numberOfGames, ?DateTime $startDate,
                                  string        $type, int $round_id, int $tournament_id): void
    {
        $date = new Date();
        $date->setName($name);
        $date->setNumber($number);
        $date->setNumberOfGames($numberOfGames);
        $date->setStartDate($startDate);
        $tournament = $manager->find(Tournament::class, $tournament_id);
        $date->setTournament($tournament);
        $date->setType($type);
        $round = $manager->find(Round::class, $round_id);
        $date->setRound($round);
        $manager->persist($date);
    }
}