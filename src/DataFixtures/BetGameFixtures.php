<?php

namespace App\DataFixtures;

use App\Entity\BetDate;
use App\Entity\BetGame;
use App\Entity\Game;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BetGameFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        /*
        * Es para el torneo 2 → 'Torneo tipo liga con fechas, pero no todas'
        * La bet = 2 (X fecha) - Date = 1 (Fecha 1)
        * Es para betDate = 1, 2 y 3 - game = 1 y 2
        * id = 1, 2, 3, 4, 5 y 6
        */
        $this->generateBetGame($manager, 1, 2, 0, 1, 1);
        $this->generateBetGame($manager, 3, 0, 0, 1, 2);
        $this->generateBetGame($manager, 0, 0, 0, 2, 1);
        $this->generateBetGame($manager, 0, 1, 0, 2, 2);
        $this->generateBetGame($manager, 4, 3, 0, 3, 1);
        $this->generateBetGame($manager, 0, 2, 0, 3, 2);

        /*
        * Es para el torneo 2 → 'Torneo tipo liga con fechas, pero no todas'
        * La bet = 2 (X fecha) - Date = 2 (Fecha 2)
        * Es para betDate = 4, 5 y 6 - game = 3 y 4
        * id = 7, 8, 9, 10, 11 y 12
        */
        $this->generateBetGame($manager, 1, 2, 0, 4, 3);
        $this->generateBetGame($manager, 3, 0, 0, 4, 4);
        $this->generateBetGame($manager, 0, 0, 0, 5, 3);
        $this->generateBetGame($manager, 0, 1, 0, 5, 4);
        $this->generateBetGame($manager, 4, 3, 0, 6, 3);
        $this->generateBetGame($manager, 0, 2, 0, 6, 4);

        /*
        * Es para el torneo 2 → 'Torneo tipo liga con fechas, pero no todas'
        * La bet = 2 (X fecha) - Date = 3 (Fecha 3)
        * Es para betDate = 7, 8 y 9 - game = 5 y 6
        * id = 13, 14, 15, 16, 17 y 18
        */
        $this->generateBetGame($manager, 1, 2, 0, 7, 5);
        $this->generateBetGame($manager, 3, 2, 0, 7, 6);
        $this->generateBetGame($manager, 1, 2, 0, 8, 5);
        $this->generateBetGame($manager, 3, 2, 0, 8, 6);
        $this->generateBetGame($manager, 1, 2, 0, 9, 5);
        $this->generateBetGame($manager, 3, 2, 0, 9, 6);

        /*
        * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
        * La bet = 3 (X fecha) - Date = 4 (Fecha 1)
        * Es para betDate = 10 y 11 - game = 7 y 8
        * id = 19, 20, 21 y 22
        */
        $this->generateBetGame($manager, 1, 2, 0, 10, 7);
        $this->generateBetGame($manager, 3, 2, 0, 10, 8);
        $this->generateBetGame($manager, 1, 2, 0, 11, 7);
        $this->generateBetGame($manager, 3, 2, 0, 11, 8);

        /*
        * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
        * La bet = 3 (X fecha) - Date = 5 (Fecha 2)
        * Es para betDate = 12 y 13 - game = 9 y 10
        * id = 23, 24, 25 y 26
        */
        $this->generateBetGame($manager, 1, 2, 0, 12, 9);
        $this->generateBetGame($manager, 3, 2, 0, 12, 10);
        $this->generateBetGame($manager, 1, 2, 0, 13, 9);
        $this->generateBetGame($manager, 3, 2, 0, 13, 10);

        /*
        * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
        * La bet = 3 (X fecha) - Date = 6 (Fecha 3)
        * Es para betDate = 14 y 15 - game = 11 y 12
        * id = 27, 28, 29 y 30
        */
        $this->generateBetGame($manager, 1, 2, 0, 14, 11);
        $this->generateBetGame($manager, 3, 2, 0, 14, 12);
        $this->generateBetGame($manager, 1, 2, 0, 15, 11);
        $this->generateBetGame($manager, 3, 2, 0, 15, 12);

        /*
        * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
        * La bet = 4 (X Partido) - Date = 4 (Fecha 1)
        * Es para betDate = 16, 17 y 18 - game = 7 y 8
        * id = 31, 32, 33, 34, 35 y 36
        */
        $this->generateBetGame($manager, 1, 2, 0, 16, 7);
        $this->generateBetGame($manager, 3, 2, 0, 16, 8);
        $this->generateBetGame($manager, 1, 2, 0, 17, 7);
        $this->generateBetGame($manager, 3, 2, 0, 17, 8);
        $this->generateBetGame($manager, 3, 2, 0, 18, 7);
        $this->generateBetGame($manager, 3, 2, 0, 18, 8);

        /*
        * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
        * La bet = 4 (X Partido) - Date = 5 (Fecha 2)
        * Es para betDate = 19, 20 y 21 - game = 9 y 10
        * id = 37, 38, 39, 40, 41 y 42
        */
        $this->generateBetGame($manager, 1, 2, 0, 19, 9);
        $this->generateBetGame($manager, 3, 2, 0, 19, 10);
        $this->generateBetGame($manager, 1, 2, 0, 20, 9);
        $this->generateBetGame($manager, 3, 2, 0, 20, 10);
        $this->generateBetGame($manager, 3, 2, 0, 21, 9);
        $this->generateBetGame($manager, 3, 2, 0, 21, 10);

        /*
        * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
        * La bet = 4 (X Partido) - Date = 6 (Fecha 3)
        * Es para betDate = 22, 23 y 24 - game = 11 y 12
        * id = 43, 44, 45, 46, 47 y 48
        */
        $this->generateBetGame($manager, 1, 2, 0, 22, 11);
        $this->generateBetGame($manager, 3, 2, 0, 22, 12);
        $this->generateBetGame($manager, 1, 2, 0, 23, 11);
        $this->generateBetGame($manager, 3, 2, 0, 23, 12);
        $this->generateBetGame($manager, 3, 2, 0, 24, 11);
        $this->generateBetGame($manager, 3, 2, 0, 24, 12);

        /*
        * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
        * La bet = 5 (X fecha) - Date = 7 (Fecha 1)
        * Es para betDate = 25, 26 y 27 - game = 13, 14, 15 Y 16
        * id = 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59 y 60
        */
        $this->generateBetGame($manager, 1, 2, 0, 25, 13);
        $this->generateBetGame($manager, 3, 2, 0, 25, 14);
        $this->generateBetGame($manager, 1, 2, 0, 25, 15);
        $this->generateBetGame($manager, 3, 2, 0, 25, 16);
        $this->generateBetGame($manager, 1, 2, 0, 26, 13);
        $this->generateBetGame($manager, 3, 2, 0, 26, 14);
        $this->generateBetGame($manager, 1, 2, 0, 26, 15);
        $this->generateBetGame($manager, 3, 2, 0, 26, 16);
        $this->generateBetGame($manager, 3, 2, 0, 27, 13);
        $this->generateBetGame($manager, 3, 2, 0, 27, 14);
        $this->generateBetGame($manager, 1, 2, 0, 27, 15);
        $this->generateBetGame($manager, 3, 2, 0, 27, 16);

        /*
        * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * * La bet = 5 (X fecha) - Date = 8 (Fecha 2)
        * Es para betDate = 28, 29 y 30 - game = 17, 18, 19 y 20
        * id = 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71 Y 72
        */
        $this->generateBetGame($manager, 1, 2, 0, 28, 17);
        $this->generateBetGame($manager, 1, 2, 0, 28, 18);
        $this->generateBetGame($manager, 1, 2, 0, 28, 19);
        $this->generateBetGame($manager, 3, 2, 0, 28, 20);
        $this->generateBetGame($manager, 1, 2, 0, 29, 17);
        $this->generateBetGame($manager, 1, 2, 0, 29, 18);
        $this->generateBetGame($manager, 1, 2, 0, 29, 19);
        $this->generateBetGame($manager, 3, 2, 0, 29, 20);
        $this->generateBetGame($manager, 1, 2, 0, 30, 17);
        $this->generateBetGame($manager, 1, 2, 0, 30, 18);
        $this->generateBetGame($manager, 1, 2, 0, 30, 19);
        $this->generateBetGame($manager, 3, 2, 0, 30, 20);


        /*
        * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
        * La bet = 5 (X fecha) - Date = 9 (Fecha 3)
        * Es para betDate = 31, 32 y 33 - game = 21, 22, 23 Y 24
        * id = 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83 Y 84
        */
        $this->generateBetGame($manager, 1, 2, 0, 31, 21);
        $this->generateBetGame($manager, 3, 2, 0, 31, 22);
        $this->generateBetGame($manager, 1, 2, 0, 31, 23);
        $this->generateBetGame($manager, 3, 2, 0, 31, 24);
        $this->generateBetGame($manager, 1, 2, 0, 32, 21);
        $this->generateBetGame($manager, 3, 2, 0, 32, 22);
        $this->generateBetGame($manager, 1, 2, 0, 32, 23);
        $this->generateBetGame($manager, 3, 2, 0, 32, 24);
        $this->generateBetGame($manager, 1, 2, 0, 33, 21);
        $this->generateBetGame($manager, 3, 2, 0, 33, 22);
        $this->generateBetGame($manager, 1, 2, 0, 33, 23);
        $this->generateBetGame($manager, 3, 2, 0, 33, 24);

        /*
        * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
        * La bet = 5 (X fecha) - Date = 10 (Fecha semifinal ida)
        * Es para betDate = 34, 35 y 36 - game = 25, Y 26
        * id = 85, 86, 87, 88, 89 Y 90
        */
        $this->generateBetGame($manager, 1, 2, 0, 34, 25);
        $this->generateBetGame($manager, 3, 2, 0, 34, 26);
        $this->generateBetGame($manager, 1, 2, 0, 35, 25);
        $this->generateBetGame($manager, 3, 2, 0, 35, 26);
        $this->generateBetGame($manager, 1, 2, 0, 36, 25);
        $this->generateBetGame($manager, 3, 2, 0, 36, 26);

        /*
        * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
        * La bet = 5 (X fecha) - Date = 11 (Fecha semifinal vuelta)
        * Es para betDate = 37, 38 y 39 - game = 27 Y 28
        * id = 91, 92, 93, 94, 95 Y 96
        */
        $this->generateBetGame($manager, 1, 2, 0, 37, 27);
        $this->generateBetGame($manager, 3, 2, 0, 37, 28);
        $this->generateBetGame($manager, 1, 2, 0, 38, 27);
        $this->generateBetGame($manager, 3, 2, 0, 38, 28);
        $this->generateBetGame($manager, 1, 2, 0, 39, 27);
        $this->generateBetGame($manager, 3, 2, 0, 39, 28);

        /*
        * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
        * La bet = 5 (X fecha) - Date = 12 (Fecha final ida)
        * Es para betDate = 40, 41 y 42 - game = 29 Y 30
        * id = 97, 98, 99, 100, 101 Y 102
        */
        $this->generateBetGame($manager, 1, 2, 0, 40, 29);
        $this->generateBetGame($manager, 3, 2, 0, 40, 30);
        $this->generateBetGame($manager, 1, 2, 0, 41, 29);
        $this->generateBetGame($manager, 3, 2, 0, 41, 30);
        $this->generateBetGame($manager, 1, 2, 0, 42, 29);
        $this->generateBetGame($manager, 3, 2, 0, 42, 30);

        /*
        * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
        * La bet = 6 (X Partidos) - Date = 13 (Fecha 1)
        * Es para betDate = 43, 44 y 45 - game = 31, 32, 33 Y 34
        * id = 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113 y 114
        */
        $this->generateBetGame($manager, 1, 2, 0, 43, 31);
        $this->generateBetGame($manager, 3, 2, 0, 43, 32);
        $this->generateBetGame($manager, 1, 2, 0, 43, 33);
        $this->generateBetGame($manager, 3, 2, 0, 43, 34);
        $this->generateBetGame($manager, 1, 2, 0, 44, 31);
        $this->generateBetGame($manager, 3, 2, 0, 44, 32);
        $this->generateBetGame($manager, 1, 2, 0, 44, 33);
        $this->generateBetGame($manager, 3, 2, 0, 44, 34);
        $this->generateBetGame($manager, 3, 2, 0, 45, 31);
        $this->generateBetGame($manager, 3, 2, 0, 45, 32);
        $this->generateBetGame($manager, 1, 2, 0, 45, 33);
        $this->generateBetGame($manager, 3, 2, 0, 45, 34);

        /*
        * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
        * La bet = 6 (X Partidos) - Date = 14 (Fecha 2)
        * Es para betDate = 46, 47 y 48 - game = 35, 36, 37 Y 38
        * id = 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125 y 126
        */
        $this->generateBetGame($manager, 1, 2, 0, 46, 35);
        $this->generateBetGame($manager, 3, 2, 0, 46, 36);
        $this->generateBetGame($manager, 1, 2, 0, 46, 37);
        $this->generateBetGame($manager, 3, 2, 0, 46, 38);
        $this->generateBetGame($manager, 1, 2, 0, 47, 35);
        $this->generateBetGame($manager, 3, 2, 0, 47, 36);
        $this->generateBetGame($manager, 1, 2, 0, 47, 37);
        $this->generateBetGame($manager, 3, 2, 0, 47, 38);
        $this->generateBetGame($manager, 3, 2, 0, 48, 35);
        $this->generateBetGame($manager, 3, 2, 0, 48, 36);
        $this->generateBetGame($manager, 1, 2, 0, 48, 37);
        $this->generateBetGame($manager, 3, 2, 0, 48, 38);

        /*
        * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
        * La bet = 6 (X Partidos) - Date = 15 (Fecha 3)
        * Es para betDate = 49, 50 y 51 - game = 39, 40, 41 Y 42
        * id = 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137 y 138
        */
        $this->generateBetGame($manager, 1, 2, 0, 49, 39);
        $this->generateBetGame($manager, 3, 2, 0, 49, 40);
        $this->generateBetGame($manager, 1, 2, 0, 49, 41);
        $this->generateBetGame($manager, 3, 2, 0, 49, 42);
        $this->generateBetGame($manager, 1, 2, 0, 50, 39);
        $this->generateBetGame($manager, 3, 2, 0, 50, 40);
        $this->generateBetGame($manager, 1, 2, 0, 50, 41);
        $this->generateBetGame($manager, 3, 2, 0, 50, 42);
        $this->generateBetGame($manager, 3, 2, 0, 51, 39);
        $this->generateBetGame($manager, 3, 2, 0, 51, 40);
        $this->generateBetGame($manager, 1, 2, 0, 51, 41);
        $this->generateBetGame($manager, 3, 2, 0, 51, 42);

        /*
        * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
        * La bet = 6 (X Partidos) - Date = 16 (Fecha semifinal solo ida)
        * Es para betDate = 52, 53 y 54 - game = 43 Y 44
        * id = 139, 140, 141, 142, 143, y 144
        */
        $this->generateBetGame($manager, 1, 2, 0, 52, 43);
        $this->generateBetGame($manager, 1, 2, 0, 52, 44);
        $this->generateBetGame($manager, 1, 2, 0, 53, 43);
        $this->generateBetGame($manager, 1, 2, 0, 53, 44);
        $this->generateBetGame($manager, 1, 2, 0, 54, 43);
        $this->generateBetGame($manager, 1, 2, 0, 54, 44);

        /*
        * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
        * La bet = 6 (X Partidos) - Date = 17 (Fecha final solo ida)
        * Es para betDate = 55, 56 y 57 - game = 45
        * id = 145, 146 y 147
        */
        $this->generateBetGame($manager, 1, 2, 0, 55, 45);
        $this->generateBetGame($manager, 1, 2, 0, 56, 45);
        $this->generateBetGame($manager, 1, 2, 0, 57, 45);

        /*
        * Es para el torneo 6 → 'Torneo tipo eliminación sin todas fechas'
        * La bet = 7 (X Fechas) - Date = 18 (Fecha semifinal ida)
        * Es para betDate = 58, 59 y 60 - game = 46 y 47
        * id = 146, 147, 148, 149, 150 y 151
        */
        $this->generateBetGame($manager, 1, 2, 0, 58, 46);
        $this->generateBetGame($manager, 1, 2, 0, 58, 47);
        $this->generateBetGame($manager, 1, 2, 0, 59, 46);
        $this->generateBetGame($manager, 1, 2, 0, 59, 47);
        $this->generateBetGame($manager, 1, 2, 0, 60, 46);
        $this->generateBetGame($manager, 1, 2, 0, 60, 47);

        /*
        * Es para el torneo 6 → 'Torneo tipo eliminación sin todas fechas'
        * La bet = 7 (X Fechas) - Date = 19 (Fecha semifinal vuelta)
        * Es para betDate = 61, 62 y 63 - game = 48 y 49
        * id = 152, 153, 154, 155, 156 y 157
        */
        $this->generateBetGame($manager, 1, 2, 0, 61, 48);
        $this->generateBetGame($manager, 1, 2, 0, 61, 49);
        $this->generateBetGame($manager, 1, 2, 0, 62, 48);
        $this->generateBetGame($manager, 1, 2, 0, 62, 49);
        $this->generateBetGame($manager, 1, 2, 0, 63, 48);
        $this->generateBetGame($manager, 1, 2, 0, 63, 49);

        /*
        * Es para el torneo 7 → 'Torneo tipo eliminación con todas fechas'
        * La bet = 8 (X Partidos) - Date = 20 (Fecha semifinal solo ida)
        * Es para betDate = 64, 65 y 66 - game = 50 y 51
        * id = 158, 159, 160, 161, 162 y 163
        */
        $this->generateBetGame($manager, 1, 2, 0, 64, 50);
        $this->generateBetGame($manager, 1, 2, 0, 64, 51);
        $this->generateBetGame($manager, 1, 2, 0, 65, 50);
        $this->generateBetGame($manager, 1, 2, 0, 65, 51);
        $this->generateBetGame($manager, 1, 2, 0, 66, 50);
        $this->generateBetGame($manager, 1, 2, 0, 66, 51);

        /*
        * Es para el torneo 7 → 'Torneo tipo eliminación con todas fechas'
        * La bet = 8 (X Partidos) - Date = 21 (Fecha final solo ida)
        * Es para betDate = 67, 68 y 69 - game = 52
        * id = 164, 165 y 166
        */
        $this->generateBetGame($manager, 1, 2, 0, 67, 52);
        $this->generateBetGame($manager, 1, 2, 0, 68, 52);
        $this->generateBetGame($manager, 1, 2, 0, 69, 52);

        /*
        * Es para el torneo 8 → 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
        * La bet = 9 (X Fecha) - Date = 22 (Fecha 1)
        * Es para betDate = 70, 71 y 72 - game = 53, 54, 55, 56, 57 y 58
        * id = 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183 y 184
        */
        $this->generateBetGame($manager, 1, 2, 0, 70, 53);
        $this->generateBetGame($manager, 1, 2, 0, 70, 54);
        $this->generateBetGame($manager, 1, 2, 0, 70, 55);
        $this->generateBetGame($manager, 1, 2, 0, 70, 56);
        $this->generateBetGame($manager, 1, 2, 0, 70, 57);
        $this->generateBetGame($manager, 1, 2, 0, 70, 58);
        $this->generateBetGame($manager, 1, 2, 0, 71, 53);
        $this->generateBetGame($manager, 1, 2, 0, 71, 54);
        $this->generateBetGame($manager, 1, 2, 0, 71, 55);
        $this->generateBetGame($manager, 1, 2, 0, 71, 56);
        $this->generateBetGame($manager, 1, 2, 0, 71, 57);
        $this->generateBetGame($manager, 1, 2, 0, 71, 58);
        $this->generateBetGame($manager, 1, 2, 0, 72, 53);
        $this->generateBetGame($manager, 1, 2, 0, 72, 54);
        $this->generateBetGame($manager, 1, 2, 0, 72, 55);
        $this->generateBetGame($manager, 1, 2, 0, 72, 56);
        $this->generateBetGame($manager, 1, 2, 0, 72, 57);
        $this->generateBetGame($manager, 1, 2, 0, 72, 58);

        $manager->flush();
    }

    private function generateBetGame(ObjectManager $manager, int $localResult,
                                     int           $awayResult, int $points,
                                     int           $betDate_id, int $game_id): void
    {
        $betGame = new BetGame();
        $betGame->setLocalResult($localResult);
        $betGame->setAwayResult($awayResult);
        $betDate = $manager->find(BetDate::class, $betDate_id);
        $betGame->setBetDate($betDate);
        $game = $manager->find(Game::class, $game_id);
        $betGame->setGame($game);
        $betGame->setPoints($points);
        $manager->persist($betGame);
    }
}