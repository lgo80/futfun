<?php

namespace App\DataFixtures;

use App\Entity\Round;
use App\Entity\RoundElimination;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoundEliminationFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        //Es para la ronda con el torneo -> 'Torneo tipo liga y eliminacion sin todas fechas'
        $this->generateRoundElimination($manager, 2,
            true, true, true, true, true,
            4, 4);

        //Es para la ronda con el torneo -> 'Torneo tipo liga y eliminacion con todas fechas'
        $this->generateRoundElimination($manager, 2,
            true, true, false, false, false, 4, 6);

        //Es para la ronda con el torneo -> 'Torneo tipo eliminacion sin todas fechas'
        $this->generateRoundElimination($manager, 2,
            true, true, true, true, true,
            4, 7);

        //Es para la ronda con el torneo -> 'Torneo tipo liga y eliminacion con todas fechas'
        $this->generateRoundElimination($manager, 2,
            true, true, false, false, false, 4, 8);

        //Es para la ronda con el torneo -> 'Torneo tipo liga y eliminacion con todas fechas'
        $this->generateRoundElimination($manager, 2,
            true, true, false, true, false, 3, 15);

        //Es para la ronda con el torneo -> 'Torneo tipo liga y eliminacion con todas fechas'
        $this->generateRoundElimination($manager, 2,
            true, true, false, false, true, 3, 16);

        //Es para la ronda con el torneo -> 'Torneo tipo liga y eliminacion con todas fechas'
        $this->generateRoundElimination($manager, 2,
            true, true, false, true, true, 4, 17);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param int $beginningOfElimination
     * @param bool $hasEnd
     * @param bool $isAwayGoal
     * @param bool $isMatchForThirdPlace
     * @param bool $isRoundTrip
     * @param bool $isRoundTripEnd
     * @param int $numberOfEliminations
     * @param int $round_id
     * @return void
     */
    private function generateRoundElimination(ObjectManager $manager, int $beginningOfElimination, bool $hasEnd,
                                              bool          $isAwayGoal, bool $isMatchForThirdPlace,
                                              bool          $isRoundTrip, bool $isRoundTripEnd,
                                              int           $numberOfEliminations, int $round_id): void
    {
        $roundElimination = new RoundElimination();
        $roundElimination->setBeginningOfElimination($beginningOfElimination);
        $roundElimination->setHasEnd($hasEnd);
        $roundElimination->setIsAwayGoal($isAwayGoal);
        $roundElimination->setIsMatchForThirdPlace($isMatchForThirdPlace);
        $roundElimination->setIsRoundTrip($isRoundTrip);
        $roundElimination->setIsRoundTripEnd($isRoundTripEnd);
        $roundElimination->setNumberOfEliminations($numberOfEliminations);
        $round = $manager->find(Round::class, $round_id);
        $roundElimination->setRound($round);
        $manager->persist($roundElimination);
    }
}