<?php

namespace App\DataFixtures;

use App\Entity\Round;
use App\Entity\Tournament;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoundFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //Es para el torneo 2 → 'Torneo tipo liga con fechas pero no todas' - id = 1
        $this->generateRound($manager, 1, 4, 2, Round::TYPE_TOURNAMENT);
        //Es para el torneo 3 → 'Torneo tipo liga con todas fechas' - id = 2
        $this->generateRound($manager, 1, 4, 3, Round::TYPE_TOURNAMENT);
        //Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas' - id = 3
        $this->generateRound($manager, 1, 8, 4, Round::TYPE_TOURNAMENT);
        //Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas' - id = 4
        $this->generateRound($manager, 2, 4, 4, Round::TYPE_TOURNAMENT);
        //Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas' - id = 5
        $this->generateRound($manager, 1, 8, 5, Round::TYPE_TOURNAMENT);
        //Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas' - id = 6
        $this->generateRound($manager, 2, 4, 5, Round::TYPE_TOURNAMENT);
        //Es para el torneo 6 → 'Torneo tipo eliminación sin todas fechas' - id = 7
        $this->generateRound($manager, 1, 4, 6, Round::TYPE_TOURNAMENT);
        //Es para el torneo 7 → 'Torneo tipo eliminación con todas fechas' - id = 8
        $this->generateRound($manager, 1, 4, 7, Round::TYPE_TOURNAMENT);
        //Es para el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' - id = 9
        $this->generateRound($manager, 1, 12, 8, Round::TYPE_TOURNAMENT);
        //Es para el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' - id = 10
        $this->generateRound($manager, 2, 4, 8, Round::TYPE_TOURNAMENT);
        //Es para el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' - id = 11
        $this->generateRound($manager, 3, 5, 8, Round::TYPE_TOURNAMENT);
        //Es para el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' - id = 12
        $this->generateRound($manager, 4, 5, 8, Round::TYPE_TOURNAMENT);
        //Es para el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' - id = 13
        $this->generateRound($manager, 5, 5, 8, Round::TYPE_TOURNAMENT);
        //Es para el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' - id = 14
        $this->generateRound($manager, 6, 5, 8, Round::TYPE_TOURNAMENT);
        //Es para el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' - id = 15
        $this->generateRound($manager, 7, 4, 8, Round::TYPE_TOURNAMENT);
        //Es para el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' - id = 16
        $this->generateRound($manager, 8, 4, 8, Round::TYPE_TOURNAMENT);
        //Es para el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' - id = 17
        $this->generateRound($manager, 9, 4, 8, Round::TYPE_TOURNAMENT);

        $manager->flush();

    }

    /**
     * @param ObjectManager $manager
     * @param int $number
     * @param int $numberOfEquipament
     * @param int $tournament_id
     * @param string $type
     * @return void
     */
    private function generateRound(ObjectManager $manager, int $number,
                                   int           $numberOfEquipament, int $tournament_id,
                                   string        $type): void
    {
        $round = new Round();
        $round->setNumber($number);
        $round->setNumberOfEquipament($numberOfEquipament);
        $tournament = $manager->find(Tournament::class, $tournament_id);
        $round->setTournament($tournament);
        $round->setType($type);
        $manager->persist($round);
    }

}
