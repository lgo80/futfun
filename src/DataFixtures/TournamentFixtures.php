<?php

namespace App\DataFixtures;

use App\Entity\Tournament;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TournamentFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $this->generateTournament($manager, 'Torneo sin fechas',
            1, false); //id = 1
        $this->generateTournament($manager, 'Torneo tipo liga con fechas pero no todas',
            1, false); //id = 2
        $this->generateTournament($manager, 'Torneo tipo liga con todas fechas',
            1, false); //id = 3
        $this->generateTournament($manager, 'Torneo tipo liga y eliminación sin todas fechas',
            2, false); //id = 4
        $this->generateTournament($manager, 'Torneo tipo liga y eliminación con todas fechas',
            2, false); //id = 5
        $this->generateTournament($manager, 'Torneo tipo eliminación sin todas fechas',
            3, false); //id = 6
        $this->generateTournament($manager, 'Torneo tipo eliminación con todas fechas',
            3, false); //id = 7
        $this->generateTournament($manager, 'Torneo tipo liga y eliminación con fechas pero con rondas especiales',
            2, false); //id = 8

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param string $name
     * @param int $type
     * @param bool $isTiebreaker
     * @return void
     */
    private function generateTournament(ObjectManager $manager, string $name,
                                        int           $type, bool $isTiebreaker): void
    {
        $tournament = new Tournament();
        $tournament->setName($name);
        $tournament->setType($type);
        $tournament->setIsTiebreaker($isTiebreaker);
        $manager->persist($tournament);
    }
}
