<?php

namespace App\DataFixtures;

use App\Entity\Bet;
use App\Entity\BetDate;
use App\Entity\Date;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BetDateFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        /*
         * Es para el torneo 2 → 'Torneo tipo liga con fechas, pero no todas'
         * La bet = 2 (X fecha) - Date = 1 (Fecha 1)
         * id = 1, 2 y 3
         */
        $this->generateBetDate($manager, 2, 1, false, 1);
        $this->generateBetDate($manager, 2, 1, false, 2);
        $this->generateBetDate($manager, 2, 1, false, 3);

        /*
         * Es para el torneo 2 → 'Torneo tipo liga con fechas, pero no todas'
         * La bet = 2 (X fecha) - Date = 2 (Fecha 2)
         * id = 4, 5 y 6
         */
        $this->generateBetDate($manager, 2, 2, false, 1);
        $this->generateBetDate($manager, 2, 2, false, 2);
        $this->generateBetDate($manager, 2, 2, false, 3);

        /*
         * Es para el torneo 2 ⇾ 'Torneo tipo liga con fechas, pero no todas'
         * La bet = 2 (X fecha) - Date = 3 (Fecha 3)
         * id = 7, 8 y 9
         */
        $this->generateBetDate($manager, 2, 3, false, 1);
        $this->generateBetDate($manager, 2, 3, false, 2);
        $this->generateBetDate($manager, 2, 3, false, 3);

        /*
         * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
         * La bet = 3 (X fecha) - Date = 4 (Fecha 1)
         * id = 10 y 11
         */
        $this->generateBetDate($manager, 3, 4, true, 1);
        $this->generateBetDate($manager, 3, 4, false, 2);

        /*
         * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
         * La bet = 3 (X fecha) - Date = 5 (Fecha 2)
         * id = 12 y 13
         */
        $this->generateBetDate($manager, 3, 5, true, 1);
        $this->generateBetDate($manager, 3, 5, false, 2);

        /*
         * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
         * La bet = 3 (X fecha) - Date = 6 (Fecha 3)
         * id = 14 y 15
         */
        $this->generateBetDate($manager, 3, 6, true, 1);
        $this->generateBetDate($manager, 3, 6, false, 2);

        /*
         * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
         * La bet = 4 (X Partido) - Date = 4 (Fecha 1)
         * id = 16, 17 y 18
         */
        $this->generateBetDate($manager, 4, 4, true, 6);
        $this->generateBetDate($manager, 4, 4, false, 7);
        $this->generateBetDate($manager, 4, 4, false, 8);

        /*
         * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
         * La bet = 4 (X Partido) - Date = 5 (Fecha 2)
         * id = 19, 20 y 21
         */
        $this->generateBetDate($manager, 4, 5, true, 6);
        $this->generateBetDate($manager, 4, 5, false, 7);
        $this->generateBetDate($manager, 4, 5, false, 8);

        /*
         * Es para el torneo 3 → 'Torneo tipo liga con todas fechas'
         * La bet = 4 (X Partido) - Date = 6 (Fecha 3)
         * id = 22, 23 y 24
         */
        $this->generateBetDate($manager, 4, 6, true, 6);
        $this->generateBetDate($manager, 4, 6, false, 7);
        $this->generateBetDate($manager, 4, 6, false, 8);

        /*
         * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * La bet = 5 (X fecha) - Date = 7 (Fecha 1)
         * id = 25, 26 y 27
         */
        $this->generateBetDate($manager, 5, 7, true, 9);
        $this->generateBetDate($manager, 5, 7, false, 10);
        $this->generateBetDate($manager, 5, 7, false, 11);

        /*
         * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * La bet = 5 (X fecha) - Date = 8 (Fecha 2)
         * id = 28, 29 y 30
         */
        $this->generateBetDate($manager, 5, 8, false, 9);
        $this->generateBetDate($manager, 5, 8, true, 10);
        $this->generateBetDate($manager, 5, 8, false, 11);

        /*
         * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * La bet = 5 (X fecha) - Date = 9 (Fecha 3)
         * id = 31, 32 y 33
         */
        $this->generateBetDate($manager, 5, 9, false, 9);
        $this->generateBetDate($manager, 5, 9, false, 10);
        $this->generateBetDate($manager, 5, 9, false, 11);

        /*
         * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * La bet = 5 (X fecha) - Date = 10 (Fecha semifinal ida)
         * id = 34, 35 y 36
         */
        $this->generateBetDate($manager, 5, 10, false, 9);
        $this->generateBetDate($manager, 5, 10, false, 10);
        $this->generateBetDate($manager, 5, 10, false, 11);

        /*
         * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * La bet = 5 (X fecha) - Date = 11 (Fecha semifinal vuelta)
         * id = 37, 38 y 39
         */
        $this->generateBetDate($manager, 5, 11, false, 9);
        $this->generateBetDate($manager, 5, 11, false, 10);
        $this->generateBetDate($manager, 5, 11, false, 11);

        /*
         * Es para el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas'
         * La bet = 5 (X fecha) - Date = 12 (Fecha final ida)
         * id = 40, 41 y 42
         */
        $this->generateBetDate($manager, 5, 12, false, 9);
        $this->generateBetDate($manager, 5, 12, false, 10);
        $this->generateBetDate($manager, 5, 12, false, 11);

        /*
         * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
         * La bet = 6 (X Partidos) - Date = 13 (Fecha 1)
         * id = 43, 44 y 45
         */
        $this->generateBetDate($manager, 6, 13, false, 9);
        $this->generateBetDate($manager, 6, 13, false, 10);
        $this->generateBetDate($manager, 6, 13, false, 11);

        /*
         * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
         * La bet = 6 (X Partidos) - Date = 14 (Fecha 2)
         * id = 46, 47 y 48
         */
        $this->generateBetDate($manager, 6, 14, false, 9);
        $this->generateBetDate($manager, 6, 14, false, 10);
        $this->generateBetDate($manager, 6, 14, false, 11);

        /*
         * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
         * La bet = 6 (X Partidos) - Date = 15 (Fecha 3)
         * id = 49, 50 y 51
         */
        $this->generateBetDate($manager, 6, 15, false, 9);
        $this->generateBetDate($manager, 6, 15, false, 10);
        $this->generateBetDate($manager, 6, 15, false, 11);

        /*
        * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
        * La bet = 6 (X Partidos) - Date = 16 (Fecha semifinal solo ida)
        * id = 52, 53 y 54
        */
        $this->generateBetDate($manager, 6, 16, false, 9);
        $this->generateBetDate($manager, 6, 16, false, 10);
        $this->generateBetDate($manager, 6, 16, false, 11);

        /*
        * Es para el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas'
        * La bet = 6 (X Partidos) - Date = 17 (Fecha final solo ida)
        * id = 55, 56 y 57
        */
        $this->generateBetDate($manager, 6, 17, false, 9);
        $this->generateBetDate($manager, 6, 17, false, 10);
        $this->generateBetDate($manager, 6, 17, false, 11);

        /*
        * Es para el torneo 6 → 'Torneo tipo eliminación sin todas fechas'
        * La bet = 7 (X Fechas) - Date = 18 (Fecha semifinal ida)
        * id = 58, 59 y 60
        */
        $this->generateBetDate($manager, 7, 18, false, 9);
        $this->generateBetDate($manager, 7, 18, false, 10);
        $this->generateBetDate($manager, 7, 18, false, 11);

        /*
        * Es para el torneo 6 → 'Torneo tipo eliminación sin todas fechas'
        * La bet = 7 (X Fechas) - Date = 19 (Fecha semifinal vuelta)
        * id = 61, 62 y 63
        */
        $this->generateBetDate($manager, 7, 19, false, 9);
        $this->generateBetDate($manager, 7, 19, false, 10);
        $this->generateBetDate($manager, 7, 19, false, 11);

        /*
        * Es para el torneo 7 → 'Torneo tipo eliminación con todas fechas'
        * La bet = 8 (X Partidos) - Date = 20 (Fecha semifinal solo ida)
        * id = 64, 65 y 66
        */
        $this->generateBetDate($manager, 8, 20, false, 9);
        $this->generateBetDate($manager, 8, 20, false, 10);
        $this->generateBetDate($manager, 8, 20, false, 11);

        /*
        * Es para el torneo 7 → 'Torneo tipo eliminación con todas fechas'
        * La bet = 8 (X Partidos) - Date = 21 (Fecha final solo ida)
        * id = 67, 68 y 69
        */
        $this->generateBetDate($manager, 8, 21, false, 9);
        $this->generateBetDate($manager, 8, 21, false, 10);
        $this->generateBetDate($manager, 8, 21, false, 11);

        /*
        * Es para el torneo 8 → 'Torneo tipo liga y eliminación con fechas, pero con rondas especiales'
        * La bet = 9 (X Fecha) - Date = 22 (Fecha 1)
        * id = 70, 71 y 72
        */
        $this->generateBetDate($manager, 9, 22, false, 1);
        $this->generateBetDate($manager, 9, 22, false, 2);
        $this->generateBetDate($manager, 9, 22, false, 3);


        $manager->flush();
    }

    private function generateBetDate(ObjectManager $manager, int $bet_id,
                                     int           $date_id, bool $isDateWin,
                                     int           $user_id): void
    {
        $betDate = new BetDate();
        $bet = $manager->find(Bet::class, $bet_id);
        $betDate->setBet($bet);
        $date = $manager->find(Date::class, $date_id);
        $betDate->setDate($date);
        $betDate->setIsDateWin($isDateWin);
        $user = $manager->find(User::class, $user_id);
        $betDate->setUser($user);
        $manager->persist($betDate);
    }

}