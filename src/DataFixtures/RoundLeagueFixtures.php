<?php

namespace App\DataFixtures;

use App\Entity\Round;
use App\Entity\RoundLeague;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoundLeagueFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {

        //Es para la ronda con el torneo 2 → 'Torneo tipo liga con fechas pero no todas' --> id = 1
        $this->generateRoundLeague($manager, 0, 1, 4,
            true, 3, 1, 0, 1,
            1, null, null, null);

        //Es para la ronda con el torneo 3 → 'Torneo tipo liga con todas fechas' --> id = 2
        $this->generateRoundLeague($manager, 0, 1, 4,
            false, 3, 1, 0, 1,
            2, null, null, null);

        //Es para la ronda con el torneo 4 → 'Torneo tipo liga y eliminación sin todas fechas' --> id = 3
        $this->generateRoundLeague($manager, 0, 2, 4,
            true, 3, 1, 0, 3,
            3, 0, 2, 1);

        //Es para la ronda con el torneo 5 → 'Torneo tipo liga y eliminación con todas fechas' --> id = 4
        $this->generateRoundLeague($manager, 0, 2, 4,
            false, 3, 1, 0, 1,
            5, 0, 2, 1);

        //Es para la ronda con el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' --> id = 5
        $this->generateRoundLeague($manager, 1, 3, 4,
            true, 3, 1, 0, 2,
            9, 0, 2, 1);

        //Es para la ronda con el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' --> id = 6
        $this->generateRoundLeague($manager, 1, 1, 4,
            false, 3, 1, 0, 1,
            10, 0, 2, 1);

        //Es para la ronda con el torneo 8 → 'Torneo tipo liga con fechas pero con rondas especiales' --> id = 7
        $this->generateRoundLeague($manager, 0, 1, 5,
            true, 3, 1, 0, 1,
            11, 0, 2, RoundLeague::FORM_IGUAL_IDA);

        //Es para la ronda con el torneo → 'Torneo tipo liga con fechas pero con rondas especiales' --> id = 8
        $this->generateRoundLeague($manager, 0, 1, 5,
            false, 3, 1, 0, 1,
            12, 0, 2, RoundLeague::FORM_IGUAL_IDA);

        //Es para la ronda con el torneo → 'Torneo tipo liga con fechas pero con rondas especiales' --> id = 9
        $this->generateRoundLeague($manager, 1, 1, 5,
            true, 3, 1, 0, 1,
            13, 0, 2, RoundLeague::FORM_IGUAL_IDA);

        //Es para la ronda con el torneo → 'Torneo tipo liga con fechas pero con rondas especiales' --> id = 10
        $this->generateRoundLeague($manager, 1, 1, 5,
            false, 3, 1, 0, 1,
            14, 0, 2, RoundLeague::FORM_IGUAL_IDA);

        $manager->flush();

    }

    /**
     * @param ObjectManager $manager
     * @param int $additionalDate
     * @param int $amountOfGroups
     * @param int $amountOfTeamsByGroups
     * @param bool $isRoundTrip
     * @param int $pointsForGamesWon
     * @param int $pointsForTiedGames
     * @param int $pointsForGamesLost
     * @param int $returnOfTheGroup
     * @param int $round_id
     * @param int|null $additionalClassifieds
     * @param int|null $classifiedByGroup
     * @param string|null $typeClassifieds
     * @return void
     */
    private function generateRoundLeague(ObjectManager $manager, int $additionalDate, int $amountOfGroups,
                                         int           $amountOfTeamsByGroups, bool $isRoundTrip,
                                         int           $pointsForGamesWon, int $pointsForTiedGames,
                                         int           $pointsForGamesLost, int $returnOfTheGroup,
                                         int           $round_id, ?int $additionalClassifieds,
                                         ?int          $classifiedByGroup, ?string $typeClassifieds): void
    {
        $roundLeague = new RoundLeague();
        $roundLeague->setAdditionalDate($additionalDate);
        $roundLeague->setAmountOfGroups($amountOfGroups);
        $roundLeague->setAmountOfTeamsByGroups($amountOfTeamsByGroups);
        $roundLeague->setIsRoundTrip($isRoundTrip);
        $roundLeague->setPointsForGamesWon($pointsForGamesWon);
        $roundLeague->setPointsForTiedGames($pointsForTiedGames);
        $roundLeague->setPointsForGamesLost($pointsForGamesLost);
        $roundLeague->setReturnOfTheGroup($returnOfTheGroup);
        $round = $manager->find(Round::class, $round_id);
        $roundLeague->setRound($round);
        $roundLeague->setAdditionalClassifieds($additionalClassifieds);
        $roundLeague->setClassifiedByGroup($classifiedByGroup);
        $roundLeague->setTypeClassifieds($typeClassifieds);
        $manager->persist($roundLeague);
    }

}