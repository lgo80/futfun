# FutFun

Aplicación web para jugar online resultados de los campeonatos deportivos publicos o privados.

## Comenzando
Esta aplicacion esta diseñada en un framework de PHP llamado Symfony. 
Mostrare algunas instrucciones que te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

## Pre-requisitos 📋
Se necesita tener instalado:
- PHP 7.2.5 o superior
- Y las siguientes librerias: 
  * Ctype
  * iconv
  * JSON
  * PCRE
  * Session
  * SimpleXML
  * Tokenizer
- MySQL
- Composer
- Git
- Preferentemente tener instalado tambien Symfony CLI

## Instalación 🔧
1. Abrir una terminal e ir a la ubicacion deseada donde quiera instalar el proyecto
2. Clonar el repositorio con:
> git clone https://gitlab.com/lgo80/futfun.git
3. Entrar al directorio creado con:
> cd futfun\
4. Ejecutar:
> composer install
5. Verificar los datos de conexión a la base datos y otras posibles en el archivo .env
6. Crear la base de datos con:
> php bin/console doctrine:database:create
7. Crear las migraciones con:
> php bin/console make:migration
8. Para volcar las migraciones a la base de datos con:
> php bin/console doctrine:migrations:migrate

## Librerias instaladas
- EasyAdmin
- Doctrine

## Test Unit
PHPUnit version xxx

- Para correr los test hacer en la terminal en la raiz del proyecto
> php ./vendor/bin/phpunit
