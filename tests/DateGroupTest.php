<?php

namespace App\Tests;

use App\Entity\Date;
use App\Entity\DateGroup;
use App\Entity\Game;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DateGroupTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSomethings(): void
    {
        $dateGroup = new DateGroup();
        $this->assertFalse($dateGroup->isThereAnyPartyEntered());
        $this->assertCount(0, $dateGroup->listGamesActives());
    }

    /**
     * @dataProvider getDateForIsThereAnyPartyEntered
     */
    public function testIsThereAnyPartyEntered(int $expected, int $dateGroup_id): void
    {
        $dateGroupRepository = $this->entityManager
            ->getRepository(DateGroup::class);
        $dateGroup = $dateGroupRepository->find($dateGroup_id);

        if ($expected == 1)
            $this->assertTrue($dateGroup->isThereAnyPartyEntered());
        else
            $this->assertFalse($dateGroup->isThereAnyPartyEntered());
    }

    public function getDateForIsThereAnyPartyEntered(): iterable
    {
        yield 'dateGroupWithOneGame' => [1, 23];
        yield 'dateGroupWithTwoGame' => [1, 1];
    }

    /**
     * @dataProvider getDateForListGamesActives
     */
    public function testListGamesActives(int $expected, int $dateGroup_id): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(DateGroup::class);
        $dateGroup = $gameRepository->find($dateGroup_id);

        $this->assertCount($expected, $dateGroup->listGamesActives());
    }

    public function getDateForListGamesActives(): iterable
    {
        yield 'dateGroupWithOneGameWithResult' => [1, 23];
        yield 'dateGroupWithOneGameWithoutResult' => [0, 31];
        yield 'dateGroupWithTwoGameWithoutResult' => [0, 18];
        yield 'dateGroupWithTwoGameWithOneResult' => [1, 15];
        yield 'dateGroupWithTwoGameWithTwoResult' => [2, 19];
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        // $this->entityManager = null;
    }
}
