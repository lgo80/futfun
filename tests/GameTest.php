<?php

namespace App\Tests;

use App\Entity\Game;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\ORM\EntityManager;

class GameTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearchByName()
    {
        $gameRepository = $this->entityManager
            ->getRepository(Game::class);

        $game = $gameRepository->find(2);
        $this->assertTrue($game->isComplete());

        $game = $gameRepository->find(4);
        $this->assertFalse($game->isComplete());
    }

    public function testIsGameTheDate(): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(Game::class);
        $game = $gameRepository->find(37);
        $this->assertTrue($game->getDateMatchAt() != null);
        $game = $gameRepository->find(38);
        $this->assertTrue($game->getDateMatchAt() != null);
        $game = $gameRepository->find(39);
        $this->assertFalse($game->getDateMatchAt() != null);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        // $this->entityManager = null;
    }
}
