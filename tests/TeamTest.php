<?php

namespace App\Tests;

use App\Entity\Team;
use PHPUnit\Framework\TestCase;

class TeamTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }

    public function generateTeam(string $name): Team
    {
        $team = new Team();
        $team->setName($name);
        return $team;
    }
}
