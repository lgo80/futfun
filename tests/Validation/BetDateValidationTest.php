<?php

namespace App\Tests\Validation;

use App\Entity\BetDate;
use App\Entity\User;
use App\Validation\BetDateValidation;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BetDateValidationTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;
    private ?object $betDateValidation;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $container = static::getContainer();
        $this->betDateValidation = $container->get(BetDateValidation::class);
    }

    /**
     * @dataProvider getBetGameForValidateUser
     */
    public function testValidateUser(int $expected, int $betGame_id, int $user_id): void
    {
        $betGameRepository = $this->entityManager
            ->getRepository(BetDate::class);
        $betDate = $betGameRepository->find($betGame_id);
        $userRepository = $this->entityManager
            ->getRepository(User::class);
        $user = $userRepository->find($user_id);
        if ($expected)
            $this->assertTrue($this->betDateValidation
                ->validateUser($betDate->getUser(), $user));
        else
            $this->assertFalse($this->betDateValidation
                ->validateUser($betDate->getUser(), $user));
    }

    public function getBetGameForValidateUser(): iterable
    {
        yield 'betForDateWithDateUnexpiredWithoutResultSameUser' => [1, 4, 1];
        yield 'betForDateWithDateUnexpiredWithoutResultOtherUser' => [0, 4, 2];
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }
}
