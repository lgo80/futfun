<?php

namespace App\Tests;

use App\Entity\Date;
use App\Entity\DateGroup;
use App\Entity\Round;
use Doctrine\ORM\EntityManager;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DateTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @dataProvider getDateForIsThereAnyPartyEntered
     * @throws Exception
     */
    public function testIsThereAnyPartyEntered(int $expected, Date $date): void
    {
        if ($expected)
            $this->assertTrue($date->isThereAnyPartyEntered());
        else
            $this->assertFalse($date->isThereAnyPartyEntered());
    }

    public function getDateForIsThereAnyPartyEntered(): iterable
    {
        $date = $this->dateEmpty();
        yield 'dateEmptyForIsThereAnyParty' => [0, $date];
        $date1 = $this->dateEmpty();
        $date1->addDateGroup(new DateGroup());
        yield 'dateEmptyWithDateGroupEmptyForIsThereAnyParty' => [1, $date1];
    }


    #[Pure] public function dateEmpty(): Date
    {
        return new Date();
    }

    public function dateWithDateGroupEmpty(): Date
    {
        $date = new Date();
        $date->addDateGroup(new DateGroup());
        return $date;
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }

}