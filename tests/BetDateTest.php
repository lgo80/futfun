<?php

namespace App\Tests;

use App\Entity\Bet;
use App\Entity\BetDate;
use App\Entity\Date;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BetDateTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSome(): void
    {
        $this->assertFalse(false);
    }

    public function betDateGenerate(Date $date, Bet $bet, User $user): BetDate
    {
        $betDate = new BetDate();
        $betDate->setDate($date);
        $betDate->setBet($bet);
        $betDate->setUser($user);
        return $betDate;
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }
}
