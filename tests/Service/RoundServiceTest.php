<?php

namespace App\Tests\Service;

use App\Entity\Date;
use App\Entity\Round;
use App\Service\RoundService;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManager;
use JetBrains\PhpStorm\Pure;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RoundServiceTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;
    private ?object $roundService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $container = static::getContainer();
        $this->roundService = $container->get(RoundService::class);
    }

    /**
     * @dataProvider getRoundsException
     */
    public function testException($function, $message, Round $round): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage($message);
        match ($function) {
            1 => $this->roundService->numberOfPartiesByGroup($round),
            2 => $this->roundService->numberOfGroup($round),
            3 => $this->roundService->quantityOfDates($round),
        };
    }

    public function getRoundsException(): iterable
    {
        $round = $this->roundEmpty();
        yield 'numberPartiesGroup' => [
            1, 'No se ingreso ni ronda de liga ni ronda de eliminacion', $round];
        yield 'numberGroup' => [
            2, 'No se ingreso ni ronda de liga ni ronda de eliminacion', $round];
        yield 'quantityDates' => [
            3, 'No se ingreso ni ronda de liga ni ronda de eliminacion', $round];
    }

    public function testException1(): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(Round::class);
        $round = $gameRepository->find(4);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('No se puede calcular los partidos de una eliminacion en la clase Round');
        $this->roundService->numberOfPartiesByGroup($round);
    }


    /**
     * @dataProvider getRoundsForQuantityOfDates
     * @throws Exception
     */
    public function testQuantityOfDates(int $expected, int $round_id): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(Round::class);
        $round = $gameRepository->find($round_id);
        $this->assertEquals($expected, $this->roundService->quantityOfDates($round));
    }

    public function getRoundsForQuantityOfDates(): iterable
    {
        yield 'numberGroupOne1' => [6, 1];
        yield 'numberGroupTwo2' => [3, 2];
        yield 'numberGroupOne3' => [10, 11];
        yield 'numberGroupTwo4' => [5, 12];
        yield 'numberGroupOne5' => [7, 9];
        yield 'numberGroupTwo6' => [4, 10];
        yield 'numberGroupOne7' => [11, 13];
        yield 'numberGroupTwo8' => [6, 14];
        yield 'numberGroupElimination' => [4, 4];
    }

    /**
     * @dataProvider getRoundsForNumberOfGroup
     * @throws Exception
     */
    public function testNumberOfGroup(int $expected, int $round_id): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(Round::class);
        $round = $gameRepository->find($round_id);
        $this->assertEquals($expected, $this->roundService->numberOfGroup($round));
    }

    public function getRoundsForNumberOfGroup(): iterable
    {
        yield 'numberGroupOne' => [1, 1];
        yield 'numberGroupTwo' => [2, 3];
        yield 'numberGroupElimination' => [1, 4];
    }


    /**
     * @dataProvider getRoundsForNumberOfGamesForGroup
     * @throws Exception
     */
    public function testNumberOfGamesForGroup(int $expected, int $round_id): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(Round::class);
        $round = $gameRepository->find($round_id);
        $this->assertEquals($expected, $this->roundService->numberOfPartiesByGroup($round));
    }

    public function getRoundsForNumberOfGamesForGroup(): iterable
    {
        yield 'numberPartiesGroup' => [2, 1];
    }

    /**
     * @dataProvider getDateForHasDateBack
     */
    public function testHasDateBack(int $expected, int $round_id, int $date_id): void
    {
        $roundRepository = $this->entityManager
            ->getRepository(Round::class);
        $round = $roundRepository->find($round_id);
        $dateRepository = $this->entityManager
            ->getRepository(Date::class);
        $date = $dateRepository->find($date_id);
        if ($expected)
            $this->assertTrue($this->roundService
                ->hasDateBack($round, $date));
        else
            $this->assertFalse($this->roundService
                ->hasDateBack($round, $date));
    }

    public function getDateForHasDateBack(): iterable
    {
        yield 'RoundLeagueWithRoundTripAndDateGo' => [1, 1, 1];
        yield 'RoundLeagueWithoutRoundTripAndDateGo' => [0, 2, 4];
        yield 'RoundLeagueWithRoundTripAndDateBack' => [0, 9, 25];
        yield 'RoundLeagueWithRoundTripAndDateGoButBackNotDefined' => [0, 3, 7];
        yield 'RoundEliminationWithRoundTripAndRoundTripEndAndDateNotEndAndAndDateGo' => [1, 4, 10];
        yield 'RoundEliminationWithRoundTripAndRoundTripEndAndDateNotEndAndAndDateBack' => [0, 4, 11];
        yield 'RoundEliminationWithRoundTripAndRoundTripEndAndDateEndAndAndDateGo' => [1, 4, 12];
        yield 'RoundEliminationWithRoundTripAndRoundTripEndAndDateEndAndAndDateBack' => [0, 17, 38]; /// <<<---------
        yield 'RoundEliminationWithoutRoundTripAndRoundTripEndAndDateNotEnd' => [0, 6, 16];
        yield 'RoundEliminationWithoutRoundTripAndRoundTripEndAndDateEnd' => [0, 6, 17];
        yield 'RoundEliminationWithRoundTripWithoutRoundTripEndAndDateNotEndAndDateGo' => [1, 15, 29];
        yield 'RoundEliminationWithRoundTripWithoutRoundTripEndAndDateNotEndAndDateBack' => [0, 15, 30];
        yield 'RoundEliminationWithRoundTripWithoutRoundTripEndAndDateEnd' => [0, 15, 31];
        yield 'RoundEliminationWithoutRoundTripWithRoundTripEndAndNotDateEndAndDateGo' => [0, 16, 32];
        yield 'RoundEliminationWithoutRoundTripWithRoundTripEndAndDateEndGo' => [1, 16, 33];
        yield 'RoundEliminationWithoutRoundTripWithRoundTripEndAndDateEndBack' => [0, 16, 34];
    }

    /**
     * @dataProvider getDateForGetNumberDateBack
     */
    public function testGetNumberDateBack(int $expected, int $round_id, int $numberDate): void
    {
        $roundRepository = $this->entityManager
            ->getRepository(Round::class);
        $round = $roundRepository->find($round_id);
        $this->assertEquals($expected, $this->roundService
            ->getNumberDateBack($round, $numberDate));
    }

    public function getDateForGetNumberDateBack(): iterable
    {
        yield 'RoundLeagueWithRoundTripAndReturnsEqualsGoDate1' => [4, 1, 1];
        yield 'RoundLeagueWithRoundTripAndReturnsEqualsGoDate2' => [5, 1, 2];
        yield 'RoundLeagueWithRoundTripAndReturnsEqualsGoDate3' => [6, 1, 3];
        yield 'RoundLeagueWithRoundTripAndReturnsInverseGoDate1' => [6, 9, 1];
        yield 'RoundLeagueWithRoundTripAndReturnsInverseGoDate2' => [5, 9, 2];
        yield 'RoundLeagueWithRoundTripAndReturnsInverseGoDate3' => [4, 9, 3];
        yield 'RoundEliminationWithRoundTripAndRoundTripEndAndDateNotEnd' => [2, 17, 1];
        yield 'RoundEliminationWithRoundTripAndRoundTripEndAndDateEnd' => [4, 17, 3];
        yield 'RoundEliminationWithoutRoundTripAndRoundTripEndAndDateEnd' => [3, 16, 2];
        yield 'RoundEliminationWithRoundTripAndWithoutRoundTripEndAndDateEnd' => [2, 15, 1];
    }


    #[Pure] public function roundEmpty(): Round
    {
        return new Round();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }
}
