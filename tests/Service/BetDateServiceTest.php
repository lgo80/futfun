<?php

namespace App\Tests\Service;

use App\Service\BetDateService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BetDateServiceTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;
    private ?object $betDateService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $container = static::getContainer();
        $this->betDateService = $container->get(BetDateService::class);
    }

    public function testIsModifiable()
    {
        $this->assertTrue(true);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }
}
