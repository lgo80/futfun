<?php

namespace App\Tests\Service;

use App\Entity\Game;
use App\Service\CalendarService;
use App\Service\GameService;
use Doctrine\ORM\EntityManager;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GameServiceTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;
    private ?object $gameService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $container = static::getContainer();
        $this->gameService = $container->get(GameService::class);
    }

    /**
     * @dataProvider getDateForIsDateGameUnExpired
     */
    public function testIsDateGameUnExpired(int $expected, int $game_id): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(Game::class);
        $game = $gameRepository->find($game_id);
        if ($expected)
            $this->assertTrue($this->gameService->isDateGameUnExpired($game));
        else
            $this->assertFalse($this->gameService->isDateGameUnExpired($game));
    }

    public function getDateForIsDateGameUnExpired(): iterable
    {
        yield 'OneGameWithDateUnExpired' => [1, 36];
        yield 'OneGameWithDateExpired' => [0, 35];
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }
}
