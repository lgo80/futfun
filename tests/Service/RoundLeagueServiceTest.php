<?php

namespace App\Tests\Service;

use App\Entity\Date;
use App\Entity\RoundLeague;
use App\Service\RoundLeagueService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RoundLeagueServiceTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;
    private ?object $roundLeagueService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $container = static::getContainer();
        $this->roundLeagueService = $container->get(RoundLeagueService::class);
    }

    public function testSome(): void
    {
        $this->assertTrue(true);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }
}
