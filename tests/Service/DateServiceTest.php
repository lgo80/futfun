<?php

namespace App\Tests\Service;

use App\Entity\Date;
use App\Entity\DateGroup;
use App\Entity\Round;
use App\Service\DateGroupService;
use App\Service\DateService;
use App\Tests\DateGroupTest;
use Exception;
use Doctrine\ORM\EntityManager;
use JetBrains\PhpStorm\Pure;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DateServiceTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;
    private ?object $dateService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $container = static::getContainer();
        $this->dateService = $container->get(DateService::class);
    }

    /**
     * @throws Exception
     */
    public function testIsNotEmptied(): void
    {
        $date = $this->dateEmpty();
        $this->assertCount(0, $this->dateService
            ->listGroupActiveWithSomeParty($date));
        $this->assertCount(0, $this->dateService
            ->listGroupActiveWithAllParty($date));
        $dateGroup = new DateGroup();
        $this->assertFalse($this->dateService->thereIsSomeCompletedPartyInDateGroup($dateGroup));
        $this->assertFalse($this->dateService->areAllPartiesCompletedInDateGroup($dateGroup));
    }

    /**
     * @dataProvider getDateExceptionIsAllGroupCompleted
     */
    public function testExceptionIsAllGroupCompleted(
        string $message, Date $date): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage($message);
//        $function($date);
        $this->dateService->isAllGroupCompleted($date, []);
    }

    public function getDateExceptionIsAllGroupCompleted(): iterable
    {
        $date = $this->dateEmpty();
        yield 'dateEmptyWithoutRoundAllCompletedGroup' => [
            'No se ingreso ninguna ronda',
            $date];
        $date = $this->dateWithDateGroupEmpty();
        $date->setRound(new Round());
        yield 'dateEmptyWithRoundEmptyAllCompletedGroup' => [
            'No se ingreso ni ronda de liga ni ronda de eliminacion',
            $date];
    }

    /**
     * @dataProvider getDateExceptionNumberOfGamesForGroup
     */
    public function testExceptionNumberOfGamesForGroup(string $message, Date $date): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage($message);
        $this->dateService->numberOfGamesForGroup($date);
    }

    public function getDateExceptionNumberOfGamesForGroup(): iterable
    {
        $date = $this->dateEmpty();
        yield 'dateEmptyWithoutRoundQuantityGamesForGroup' => [
            'No se ingreso ninguna ronda',
            $date];
        $date = $this->dateWithDateGroupEmpty();
        $date->setRound(new Round());
        yield 'dateEmptyWithRoundEmptyQuantityGamesForGroup' => [
            'No se ingreso ni ronda de liga ni ronda de eliminacion',
            $date];
    }

    /**
     * @dataProvider getDateExceptionAreAllTheCompleteGames
     */
    public function testExceptionAreAllTheCompleteGames(
        string $message, Date $date): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage($message);
        $this->dateService->areAllTheCompleteGames($date);
    }

    public function getDateExceptionAreAllTheCompleteGames(): iterable
    {
        $date = $this->dateWithDateGroupEmpty();
        yield 'dateEmptyWithoutRoundAllCompletedGame' => [
            'No se ingreso ninguna ronda',
            $date];
        $date = $this->dateWithDateGroupEmpty();
        $date->setRound(new Round());
        yield 'dateEmptyWithRoundEmptyAllCompletedGame' => [
            'No se ingreso ni ronda de liga ni ronda de eliminacion',
            $date];
    }

    /**
     * @dataProvider getDateForIsAllGroupCompleted
     * @throws Exception
     */
    public function testIsAllGroupCompleted(int $expected, int $date_id, array $array): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(Date::class);
        $date = $gameRepository->find($date_id);
        if ($expected)
            $this->assertTrue($this->dateService->isAllGroupCompleted($date, $array));
        else
            $this->assertFalse($this->dateService->isAllGroupCompleted($date, $array));
    }

    public function getDateForIsAllGroupCompleted(): iterable
    {
        yield 'dateWithRoundOneGroupForIsAllGroupCompletedEmpty' => [0, 1, []];
        yield 'dateWithRoundOneGroupForIsAllGroupCompletedWith' => [1, 1, ['Algo']];
        yield 'dateWithRoundTwoGroupForIsAllGroupCompletedEmpty' => [0, 7, []];
        yield 'dateWithRoundTwoGroupForIsAllGroupCompletedOneThing' => [0, 7, ['algo']];
        yield 'dateWithRoundTwoGroupForIsAllGroupCompletedAll' => [1, 7, ['algo', 'algo']];
    }

    /**
     * @dataProvider getDateForThereIsSomeCompletedParty
     * @throws Exception
     */
    public function testThereIsSomeCompletedParty(int $expected, int $date_id): void
    {

        $gameRepository = $this->entityManager
            ->getRepository(Date::class);
        $date = $gameRepository->find($date_id);
        if ($expected)
            $this->assertTrue($this->dateService->thereIsSomeCompletedParty($date));
        else
            $this->assertFalse($this->dateService->thereIsSomeCompletedParty($date));
    }

    public function getDateForThereIsSomeCompletedParty(): iterable
    {
        yield 'dateWithRoundOneGroupWithOneGameWithoutCompletedForSomePartyCompleted' => [0, 3];
        yield 'dateWithRoundOneGroupWithTwoGameOneCompletedForSomePartyCompleted' => [1, 2];
        yield 'dateWithRoundOneGroupWithTwoGameAllCompletedForSomePartyCompleted' => [1, 1];
        yield 'dateWithRoundTwoGroupWithOneGroupCompletedAndOtherNotForSomePartyCompleted' => [1, 8];
        yield 'dateWithRoundTwoGroupWithAllGroupCompletedForSomePartyCompleted' => [1, 7];
    }

    /**
     * @dataProvider getDateForAreAllTheCompleteGames
     * @throws Exception
     */
    public function testAreAllTheCompleteGames(int $expected, int $date_id): void
    {

        $gameRepository = $this->entityManager
            ->getRepository(Date::class);
        $date = $gameRepository->find($date_id);
        if ($expected)
            $this->assertTrue($this->dateService->areAllTheCompleteGames($date));
        else
            $this->assertFalse($this->dateService->areAllTheCompleteGames($date));
    }

    public function getDateForAreAllTheCompleteGames(): iterable
    {
        yield 'dateWithRoundOneGroupWithOneGameWithoutCompletedForAllPartyCompleted' => [0, 3];
        yield 'dateWithRoundOneGroupWithTwoGameOneCompletedForAllPartyCompleted' => [0, 2];
        yield 'dateWithRoundOneGroupWithTwoGameAllCompletedForAllPartyCompleted' => [1, 1];
        yield 'dateWithRoundTwoGroupWithOneGroupCompletedAndOtherNotForAllPartyCompleted' => [0, 8];
        yield 'dateWithRoundTwoGroupWithAllGroupCompletedForAllPartyCompleted' => [1, 7];
    }


    /**
     * @dataProvider getDateForIsDateUnExpired
     * @throws Exception
     */
    public function testIsDateUnExpired(int $expected, int $date_id): void
    {
        $dateRepository = $this->entityManager
            ->getRepository(Date::class);
        $date = $dateRepository->find($date_id);

        if ($expected)
            $this->assertTrue($this->dateService->isDateUnExpired($date));
        else
            $this->assertFalse($this->dateService->isDateUnExpired($date));
    }

    public function getDateForIsDateUnExpired(): iterable
    {
        yield 'dateWithDateUnexpired' => [1, 2];
        yield 'dateWithDateExpired' => [0, 3];
    }

    public function testIsGameTheDate(): void
    {
        $dateRepository = $this->entityManager
            ->getRepository(Date::class);
        $date = $dateRepository->find(2);
        $this->assertTrue($date->getStartDate() != null);
        $date = $dateRepository->find(3);
        $this->assertTrue($date->getStartDate() != null);
        $date = $dateRepository->find(4);
        $this->assertFalse($date->getStartDate() != null);
    }

    /**
     * @dataProvider getDateForThereIsSomeCompletedPartyInDateGroup
     */
    public function testThereIsSomeCompletedPartyInDateGroup(
        int $expected, int $dateGroup_id): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(DateGroup::class);
        $dateGroup = $gameRepository->find($dateGroup_id);

        if ($expected)
            $this->assertTrue($this->dateService
                ->thereIsSomeCompletedPartyInDateGroup($dateGroup));
        else
            $this->assertFalse($this->dateService
                ->thereIsSomeCompletedPartyInDateGroup($dateGroup));
    }

    public function getDateForThereIsSomeCompletedPartyInDateGroup(): iterable
    {
        yield 'OneGroupOneGameWithoutResult' => [0, 3];
        yield 'OneGroupTwoGameWithOneResult' => [1, 2];
        yield 'OneGroupTwoGameWithAllResult' => [1, 1];

    }

    /**
     * @dataProvider getDateForAreAllPartiesCompletedInDateGroup
     * @throws Exception
     */
    public function testAreAllPartiesCompletedInDateGroup(
        int $expected, int $dateGroup_id): void
    {
        $gameRepository = $this->entityManager
            ->getRepository(DateGroup::class);
        $dateGroup = $gameRepository->find($dateGroup_id);

        if ($expected)
            $this->assertTrue($this->dateService
                ->areAllPartiesCompletedInDateGroup($dateGroup));
        else
            $this->assertFalse($this->dateService
                ->areAllPartiesCompletedInDateGroup($dateGroup));
    }

    public function getDateForAreAllPartiesCompletedInDateGroup(): iterable
    {
        yield 'OneGroupOneGameWithoutResult' => [0, 3];
        yield 'OneGroupTwoGameWithOneResult' => [0, 2];
        yield 'OneGroupTwoGameWithAllResult' => [1, 1];
    }

    #[Pure] public function dateEmpty(): Date
    {
        return new Date();
    }

    public function dateWithDateGroupEmpty(): Date
    {
        $date = $this->dateEmpty();
        $date->addDateGroup(new DateGroup());
        return $date;
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }
}
