<?php

namespace App\Tests\Service;

use App\Entity\BetDate;
use App\Entity\BetGame;
use App\Entity\Game;
use App\Entity\User;
use App\Repository\GameRepository;
use App\Service\BetGameService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BetGameServiceTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;
    private ?object $betGameService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $container = static::getContainer();
        $this->betGameService = $container->get(BetGameService::class);
    }

    /**
     * @dataProvider getBetGameForCanBeModified
     */
    public function testCanBeModified(int $expected, int $betDate_id, int $user_id, int $game_id): void
    {
        $betDateRepository = $this->entityManager
            ->getRepository(BetDate::class);
        $betDate = $betDateRepository->find($betDate_id);

        $userRepository = $this->entityManager
            ->getRepository(User::class);
        $user = $userRepository->find($user_id);

        $gameRepository = $this->entityManager
            ->getRepository(Game::class);
        $game = $gameRepository->find($game_id);

        if ($expected)
            $this->assertTrue($this->betGameService->canBeModified($user, $betDate, $game));
        else
            $this->assertFalse($this->betGameService->canBeModified($user, $betDate, $game));
    }

    public function getBetGameForCanBeModified(): iterable
    {
        // Los casos que tengo que testear
        // 1- Con la apuesta que se cierra con la fecha del Date:
        //      a) Con Fecha ingresada:
        //          - El partido sin resultado y la fecha no esta vencida - Tiene que dar True
        yield 'betForDateWithDateUnexpiredWithoutResultSameUser' => [1, 4, 1, 4];
        //          - El partido sin resultado, la fecha no esta vencida y
        //               que haya un resultado en la fecha - Tiene que dar True
        yield 'betForDateWithDateUnexpiredWithoutResultSameUserWithResultInTheDate' => [1, 28, 9, 19];
        //          - El partido sin resultado y la fecha esta vencida - Tiene que dar False
        yield 'betForDateWithDateExpiredWithoutResultSameUser' => [0, 7, 1, 5];
        //          - El partido con resultado y la fecha esta vencida - Tiene que dar False
        yield 'betForDateWithDateExpiredWithResultSameUser' => [0, 20, 7, 9];
        //          - El partido con resultado y la fecha no esta vencida - Tiene que dar False
        yield 'betForDateWithDateUnexpiredWithResultSameUser' => [0, 4, 1, 3];
        //      b) Sin Fecha ingresada:
        //          - El partido sin resultado y sin ningún resultado en la fecha - Tiene que dar True
        yield 'betForDateWithoutDateWithoutResultSameUserWithoutResultInTheDate' => [1, 31, 9, 21];
        //          - El partido sin resultado y con algún resultado en la fecha - Tiene que dar False
        yield 'betForDateWithoutDateWithoutResultSameUserWithResultInTheDate' => [0, 70, 1, 55];
        //          - El partido con resultado y con algún resultado en la fecha - Tiene que dar False
        yield 'betForDateWithoutDateWithResultSameUserWithResultInTheDate' => [0, 70, 1, 53];
        // 2- Con la apuesta que se cierra con la fecha del Partido:
        //      a) Con Fecha ingresada:
        //          - El partido sin resultado y a fecha no esta vencida - Tiene que dar True
        yield 'betForGameWithDateUnexpiredWithoutResultSameUser' => [1, 46, 9, 36];
        //          - El partido sin resultado y a fecha esta vencida - Tiene que dar False
        yield 'betForGameWithDateExpiredWithoutResultSameUser' => [0, 46, 9, 35];
        //          - El partido con resultado y a fecha esta vencida - Tiene que dar False
        yield 'betForGameWithDateExpiredWithResultSameUser' => [0, 46, 9, 37];
        //          - El partido con resultado y a fecha no esta vencida - Tiene que dar False
        yield 'betForGameWithDateUnExpiredWithResultSameUser' => [0, 46, 9, 38];
        //      b) Sin Fecha ingresada:
        //          - El partido sin resultado - Tiene que dar True
        yield 'betForGameWithoutDateWithoutResultSameUser' => [1, 49, 9, 39];
        //          - El partido con resultado - Tiene que dar False
        yield 'betForGameWithoutDateWithResultSameUser' => [0, 49, 9, 40];
        // 3- Con usuario distinto: (todos estos tienen que dar False)
        //      - Todos los que dan true cambiando los usuarios
        yield 'betForDateWithDateUnexpiredWithoutResultOtherUser' => [0, 4, 2, 4];
        yield 'betForDateWithDateUnexpiredWithoutResultOtherUserWithResultInTheDate' => [0, 28, 2, 19];
        yield 'betForDateWithoutDateWithoutResultOtherUserWithoutResultInTheDate' => [0, 31, 2, 21];
        yield 'betForGameWithDateUnexpiredWithoutResultOtherUser' => [0, 46, 2, 35];
        yield 'betForGameWithoutDateWithoutResultOtherUser' => [0, 49, 2, 39];

    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }
}
