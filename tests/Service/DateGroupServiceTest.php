<?php

namespace App\Tests\Service;

use App\Entity\DateGroup;
use App\Service\DateGroupService;
use App\Service\DateService;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManager;
use JetBrains\PhpStorm\Pure;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DateGroupServiceTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;
    private ?object $dateGroupService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $container = static::getContainer();
        $this->dateGroupService = $container->get(DateGroupService::class);
    }

    public function testSomething(): void
    {
        $this->assertTrue(true);
    }

    /**
     * @return DateGroup
     */
    #[Pure] public function dateGroupEmpty(): DateGroup
    {
        return new DateGroup();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }
}
