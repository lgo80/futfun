<?php

namespace App\Tests;

use App\Entity\Round;
use App\Entity\RoundElimination;
use JetBrains\PhpStorm\Pure;
use PHPUnit\Framework\TestCase;

class RoundEliminationTest extends TestCase
{
    public function testIsRoundTripInSomeDate(): void
    {
        $roundElimination = new RoundElimination();
        $roundElimination->setIsRoundTrip(false);
        $roundElimination->setIsRoundTripEnd(false);
        $this->assertFalse($roundElimination->isRoundTripInSomeDate());
        $roundElimination->setIsRoundTrip(true);
        $this->assertTrue($roundElimination->isRoundTripInSomeDate());
        $roundElimination->setIsRoundTripEnd(true);
        $this->assertTrue($roundElimination->isRoundTripInSomeDate());
        $roundElimination->setIsRoundTrip(false);
        $this->assertTrue($roundElimination->isRoundTripInSomeDate());
    }

    /**
     * @return RoundElimination
     */
    #[Pure] public function roundEliminationEmpty(): RoundElimination
    {
        return new RoundElimination();
    }
}
