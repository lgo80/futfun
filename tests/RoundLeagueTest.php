<?php

namespace App\Tests;

use App\Entity\RoundLeague;
use JetBrains\PhpStorm\Pure;
use PHPUnit\Framework\TestCase;

class RoundLeagueTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }

    #[Pure] public function roundLeagueEmpty(): RoundLeague
    {
        return new RoundLeague();
    }

    public function roundLeagueWith(
        int $amountOfTeamsByGroups, int $AmountOfGroups, bool $isRoundTrip, int $additionalDate): RoundLeague
    {
        $roundLeague = new RoundLeague();
        $roundLeague->setAmountOfTeamsByGroups($amountOfTeamsByGroups);
        $roundLeague->setAmountOfGroups($AmountOfGroups);
        $roundLeague->setIsRoundTrip($isRoundTrip);
        $roundLeague->setAdditionalDate($additionalDate);
        return $roundLeague;
    }
}
