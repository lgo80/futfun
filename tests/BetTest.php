<?php

namespace App\Tests;

use App\Entity\Bet;
use PHPUnit\Framework\TestCase;

class BetTest extends TestCase
{
    public function testIsEditedByDate(): void
    {
        $bet = $this->betGenerate(2);
        $this->assertFalse($bet->isEditedByDate());
        $bet->setExpirationRate(1);
        $this->assertTrue($bet->isEditedByDate());
    }

    public function betGenerate(int $expirationRate): Bet
    {
        $bet = new Bet();
        $bet->setExpirationRate($expirationRate);
        return $bet;
    }
}
