<?php

namespace App\Tests;

use App\Entity\BetGame;
use App\Entity\Game;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BetGameTest extends KernelTestCase
{

    const NOW = '2022-02-07 15:03:01';
    const BEFORE = '2022-02-06 15:03:01';
    const AFTER = '2022-02-08 15:03:01';

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @dataProvider getBetGameForIsSomeGame
     */
    public function testIsSomeGame(int $expected, int $betGame_id, int $game_id): void
    {
        $betGameRepository = $this->entityManager
            ->getRepository(BetGame::class);
        $betGame = $betGameRepository->find($betGame_id);

        $gameRepository = $this->entityManager
            ->getRepository(Game::class);
        $game = $gameRepository->find($game_id);

        if ($expected)
            $this->assertTrue($betGame->isSomeGame($game));
        else
            $this->assertFalse($betGame->isSomeGame($game));
    }

    public function getBetGameForIsSomeGame(): iterable
    {
        yield 'betGameWithSameGame' => [1, 1, 1];
        yield 'betGameWithOtherGame' => [0, 63, 2];
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }

}
