<?php

namespace App\Tests;

use App\Entity\Round;
use Doctrine\ORM\EntityManager;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RoundTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSomething(): void
    {
        $this->assertTrue(true);
    }

    #[Pure] public function roundEmpty(): Round
    {
        return new Round();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
    }

}