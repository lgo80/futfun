<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210805130244 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bet CHANGE champion_id champion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bet_extra CHANGE value_detail value_detail VARCHAR(150) DEFAULT NULL');
        $this->addSql('ALTER TABLE bet_extra_user CHANGE is_correct is_correct TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE bet_game CHANGE points points INT DEFAULT NULL');
        $this->addSql('ALTER TABLE date CHANGE start_date start_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE date_group CHANGE free_team_id free_team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game CHANGE local_id local_id INT DEFAULT NULL, CHANGE away_id away_id INT DEFAULT NULL, CHANGE local_value local_value INT DEFAULT NULL, CHANGE away_value away_value INT DEFAULT NULL, CHANGE local_definition_value local_definition_value INT DEFAULT NULL, CHANGE away_definition_value away_definition_value INT DEFAULT NULL, CHANGE date_match_at date_match_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE league_positions CHANGE group_name_id group_name_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_elimination CHANGE beginning_of_elimination beginning_of_elimination INT DEFAULT NULL, CHANGE is_away_goal is_away_goal TINYINT(1) DEFAULT NULL, CHANGE is_round_trip_end is_round_trip_end TINYINT(1) DEFAULT NULL, CHANGE number_of_eliminations number_of_eliminations INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_league CHANGE amount_of_teams_by_groups amount_of_teams_by_groups INT DEFAULT NULL, CHANGE return_of_the_group return_of_the_group INT DEFAULT NULL, CHANGE classified_by_group classified_by_group INT DEFAULT NULL, CHANGE additional_classifieds additional_classifieds INT DEFAULT NULL, CHANGE type_classifieds type_classifieds VARCHAR(25) DEFAULT NULL, CHANGE additional_date additional_date INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament CHANGE champion_id champion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bet CHANGE champion_id champion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bet_extra CHANGE value_detail value_detail VARCHAR(150) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE bet_extra_user CHANGE is_correct is_correct TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE bet_game CHANGE points points INT DEFAULT NULL');
        $this->addSql('ALTER TABLE date CHANGE start_date start_date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE date_group CHANGE free_team_id free_team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game CHANGE local_id local_id INT DEFAULT NULL, CHANGE away_id away_id INT DEFAULT NULL, CHANGE local_value local_value INT DEFAULT NULL, CHANGE away_value away_value INT DEFAULT NULL, CHANGE local_definition_value local_definition_value INT DEFAULT NULL, CHANGE away_definition_value away_definition_value INT DEFAULT NULL, CHANGE date_match_at date_match_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE league_positions CHANGE group_name_id group_name_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_elimination CHANGE beginning_of_elimination beginning_of_elimination INT DEFAULT NULL, CHANGE is_away_goal is_away_goal TINYINT(1) DEFAULT \'NULL\', CHANGE is_round_trip_end is_round_trip_end TINYINT(1) DEFAULT \'NULL\', CHANGE number_of_eliminations number_of_eliminations INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_league CHANGE amount_of_teams_by_groups amount_of_teams_by_groups INT DEFAULT NULL, CHANGE return_of_the_group return_of_the_group INT DEFAULT NULL, CHANGE classified_by_group classified_by_group INT DEFAULT NULL, CHANGE additional_classifieds additional_classifieds INT DEFAULT NULL, CHANGE type_classifieds type_classifieds VARCHAR(25) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE additional_date additional_date INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament CHANGE champion_id champion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
