<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210725062058 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE round_elimination (id INT AUTO_INCREMENT NOT NULL, round_id INT NOT NULL, beginning_of_elimination VARCHAR(30) NOT NULL, form_of_disposal VARCHAR(25) NOT NULL, is_match_for_third_place TINYINT(1) NOT NULL, form_of_the_final VARCHAR(25) NOT NULL, is_away_goal TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_E34172CA6005CA0 (round_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE round_league (id INT AUTO_INCREMENT NOT NULL, round_id INT NOT NULL, amount_of_groups INT NOT NULL, amount_of_teams_by_groups INT DEFAULT NULL, shape_group VARCHAR(20) NOT NULL, return_of_the_group VARCHAR(30) DEFAULT NULL, classified_by_group INT DEFAULT NULL, additional_classifieds INT DEFAULT NULL, type_classifieds VARCHAR(25) DEFAULT NULL, additional_date INT DEFAULT NULL, points_for_games_won INT NOT NULL, points_for_tied_games INT NOT NULL, points_for_games_lost INT NOT NULL, UNIQUE INDEX UNIQ_FA03FAB4A6005CA0 (round_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE round_elimination ADD CONSTRAINT FK_E34172CA6005CA0 FOREIGN KEY (round_id) REFERENCES round (id)');
        $this->addSql('ALTER TABLE round_league ADD CONSTRAINT FK_FA03FAB4A6005CA0 FOREIGN KEY (round_id) REFERENCES round (id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE round_elimination');
        $this->addSql('DROP TABLE round_league');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
