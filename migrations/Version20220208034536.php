<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220208034536 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE administrator_bet (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, bet_id INT NOT NULL, role VARCHAR(80) NOT NULL, INDEX IDX_A2B7E102A76ED395 (user_id), INDEX IDX_A2B7E102D871DC26 (bet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE administrator_tournament (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, tournament_id INT NOT NULL, role VARCHAR(80) NOT NULL, INDEX IDX_27475C99A76ED395 (user_id), INDEX IDX_27475C9933D1A3E7 (tournament_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bet (id INT AUTO_INCREMENT NOT NULL, tournament_id INT NOT NULL, champion_id INT DEFAULT NULL, name VARCHAR(80) NOT NULL, expiration_rate INT NOT NULL, points_for_exact_games INT NOT NULL, points_by_result_only INT NOT NULL, points_per_missed_game INT NOT NULL, is_active TINYINT(1) NOT NULL, state INT NOT NULL, date_creation_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', date_extra_limit_at DATETIME NOT NULL, symbolic_bet DOUBLE PRECISION NOT NULL, INDEX IDX_FBF0EC9B33D1A3E7 (tournament_id), INDEX IDX_FBF0EC9BFA7FD7EB (champion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bet_date (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, date_id INT NOT NULL, bet_id INT NOT NULL, modification_date_at DATETIME NOT NULL, is_date_win TINYINT(1) NOT NULL, INDEX IDX_8F48E446A76ED395 (user_id), INDEX IDX_8F48E446B897366B (date_id), INDEX IDX_8F48E446D871DC26 (bet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bet_extra (id INT AUTO_INCREMENT NOT NULL, bet_id INT NOT NULL, detail VARCHAR(80) NOT NULL, description VARCHAR(255) NOT NULL, is_complete TINYINT(1) NOT NULL, value INT NOT NULL, value_detail VARCHAR(150) DEFAULT NULL, is_active TINYINT(1) NOT NULL, INDEX IDX_6275A731D871DC26 (bet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bet_extra_user (id INT AUTO_INCREMENT NOT NULL, participante_id INT NOT NULL, bet_extra_id INT NOT NULL, detail VARCHAR(150) DEFAULT NULL, is_correct TINYINT(1) DEFAULT NULL, is_active TINYINT(1) NOT NULL, INDEX IDX_99BAB546F6F50196 (participante_id), INDEX IDX_99BAB54682C1E568 (bet_extra_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bet_game (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, bet_date_id INT NOT NULL, local_result INT NOT NULL, away_result INT NOT NULL, points INT DEFAULT NULL, INDEX IDX_6FDE2B0E48FD905 (game_id), INDEX IDX_6FDE2B05043CB87 (bet_date_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE betting_positions (id INT AUTO_INCREMENT NOT NULL, participant_id INT NOT NULL, bet_id INT NOT NULL, points INT NOT NULL, results_embodies_exacts INT NOT NULL, results_embodies_only INT NOT NULL, results_failed INT NOT NULL, points_extras INT NOT NULL, is_active TINYINT(1) NOT NULL, number_of_dates_won INT NOT NULL, INDEX IDX_1BAE25789D1C3019 (participant_id), INDEX IDX_1BAE2578D871DC26 (bet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE date (id INT AUTO_INCREMENT NOT NULL, tournament_id INT NOT NULL, round_id INT NOT NULL, number INT NOT NULL, name VARCHAR(20) NOT NULL, start_date DATETIME DEFAULT NULL, type VARCHAR(20) NOT NULL, number_of_games INT DEFAULT NULL, INDEX IDX_AA9E377A33D1A3E7 (tournament_id), INDEX IDX_AA9E377AA6005CA0 (round_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE date_group (id INT AUTO_INCREMENT NOT NULL, date_id INT NOT NULL, group_name_id INT DEFAULT NULL, free_team_id INT DEFAULT NULL, INDEX IDX_D730AC49B897366B (date_id), INDEX IDX_D730AC49F717C8DA (group_name_id), INDEX IDX_D730AC493C809D0 (free_team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE elimination_match_data (id INT AUTO_INCREMENT NOT NULL, tournament_id INT NOT NULL, game_id INT NOT NULL, type_local VARCHAR(30) NOT NULL, local_match_number INT NOT NULL, type_away VARCHAR(30) NOT NULL, away_match_number INT NOT NULL, INDEX IDX_46779A4733D1A3E7 (tournament_id), INDEX IDX_46779A47E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, local_id INT DEFAULT NULL, away_id INT DEFAULT NULL, date_group_id INT NOT NULL, number INT NOT NULL, local_value INT DEFAULT NULL, away_value INT DEFAULT NULL, local_definition_value INT DEFAULT NULL, away_definition_value INT DEFAULT NULL, date_match_at DATETIME DEFAULT NULL, type INT NOT NULL, INDEX IDX_232B318C5D5A2101 (local_id), INDEX IDX_232B318C8DEF089F (away_id), INDEX IDX_232B318C3D61B0E1 (date_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `group` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE league_positions (id INT AUTO_INCREMENT NOT NULL, team_id INT NOT NULL, group_name_id INT DEFAULT NULL, tournament_id INT NOT NULL, round_id INT NOT NULL, points INT NOT NULL, goals_scored INT NOT NULL, goals_against INT NOT NULL, goal_difference INT NOT NULL, games_won INT NOT NULL, tied_games INT NOT NULL, games_lost INT NOT NULL, INDEX IDX_84D0BCA2296CD8AE (team_id), INDEX IDX_84D0BCA2F717C8DA (group_name_id), INDEX IDX_84D0BCA233D1A3E7 (tournament_id), INDEX IDX_84D0BCA2A6005CA0 (round_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE round (id INT AUTO_INCREMENT NOT NULL, tournament_id INT NOT NULL, type INT NOT NULL, number INT NOT NULL, number_of_equipament INT NOT NULL, INDEX IDX_C5EEEA3433D1A3E7 (tournament_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE round_elimination (id INT AUTO_INCREMENT NOT NULL, round_id INT NOT NULL, beginning_of_elimination INT DEFAULT NULL, is_round_trip TINYINT(1) NOT NULL, is_away_goal TINYINT(1) DEFAULT NULL, is_match_for_third_place TINYINT(1) NOT NULL, is_round_trip_end TINYINT(1) DEFAULT NULL, has_end TINYINT(1) NOT NULL, number_of_eliminations INT DEFAULT NULL, UNIQUE INDEX UNIQ_E34172CA6005CA0 (round_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE round_league (id INT AUTO_INCREMENT NOT NULL, round_id INT NOT NULL, amount_of_groups INT NOT NULL, amount_of_teams_by_groups INT DEFAULT NULL, is_round_trip TINYINT(1) NOT NULL, return_of_the_group INT DEFAULT NULL, classified_by_group INT DEFAULT NULL, additional_classifieds INT DEFAULT NULL, type_classifieds VARCHAR(25) DEFAULT NULL, additional_date INT DEFAULT NULL, points_for_games_won INT NOT NULL, points_for_tied_games INT NOT NULL, points_for_games_lost INT NOT NULL, UNIQUE INDEX UNIQ_FA03FAB4A6005CA0 (round_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(60) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament (id INT AUTO_INCREMENT NOT NULL, champion_id INT DEFAULT NULL, name VARCHAR(80) NOT NULL, class SMALLINT NOT NULL, state SMALLINT NOT NULL, type SMALLINT NOT NULL, is_active TINYINT(1) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', is_tiebreaker TINYINT(1) NOT NULL, INDEX IDX_BD5FB8D9FA7FD7EB (champion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(30) NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE administrator_bet ADD CONSTRAINT FK_A2B7E102A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE administrator_bet ADD CONSTRAINT FK_A2B7E102D871DC26 FOREIGN KEY (bet_id) REFERENCES bet (id)');
        $this->addSql('ALTER TABLE administrator_tournament ADD CONSTRAINT FK_27475C99A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE administrator_tournament ADD CONSTRAINT FK_27475C9933D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)');
        $this->addSql('ALTER TABLE bet ADD CONSTRAINT FK_FBF0EC9B33D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)');
        $this->addSql('ALTER TABLE bet ADD CONSTRAINT FK_FBF0EC9BFA7FD7EB FOREIGN KEY (champion_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bet_date ADD CONSTRAINT FK_8F48E446A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bet_date ADD CONSTRAINT FK_8F48E446B897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE bet_date ADD CONSTRAINT FK_8F48E446D871DC26 FOREIGN KEY (bet_id) REFERENCES bet (id)');
        $this->addSql('ALTER TABLE bet_extra ADD CONSTRAINT FK_6275A731D871DC26 FOREIGN KEY (bet_id) REFERENCES bet (id)');
        $this->addSql('ALTER TABLE bet_extra_user ADD CONSTRAINT FK_99BAB546F6F50196 FOREIGN KEY (participante_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bet_extra_user ADD CONSTRAINT FK_99BAB54682C1E568 FOREIGN KEY (bet_extra_id) REFERENCES bet_extra (id)');
        $this->addSql('ALTER TABLE bet_game ADD CONSTRAINT FK_6FDE2B0E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE bet_game ADD CONSTRAINT FK_6FDE2B05043CB87 FOREIGN KEY (bet_date_id) REFERENCES bet_date (id)');
        $this->addSql('ALTER TABLE betting_positions ADD CONSTRAINT FK_1BAE25789D1C3019 FOREIGN KEY (participant_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE betting_positions ADD CONSTRAINT FK_1BAE2578D871DC26 FOREIGN KEY (bet_id) REFERENCES bet (id)');
        $this->addSql('ALTER TABLE date ADD CONSTRAINT FK_AA9E377A33D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)');
        $this->addSql('ALTER TABLE date ADD CONSTRAINT FK_AA9E377AA6005CA0 FOREIGN KEY (round_id) REFERENCES round (id)');
        $this->addSql('ALTER TABLE date_group ADD CONSTRAINT FK_D730AC49B897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE date_group ADD CONSTRAINT FK_D730AC49F717C8DA FOREIGN KEY (group_name_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE date_group ADD CONSTRAINT FK_D730AC493C809D0 FOREIGN KEY (free_team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE elimination_match_data ADD CONSTRAINT FK_46779A4733D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)');
        $this->addSql('ALTER TABLE elimination_match_data ADD CONSTRAINT FK_46779A47E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C5D5A2101 FOREIGN KEY (local_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C8DEF089F FOREIGN KEY (away_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C3D61B0E1 FOREIGN KEY (date_group_id) REFERENCES date_group (id)');
        $this->addSql('ALTER TABLE league_positions ADD CONSTRAINT FK_84D0BCA2296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE league_positions ADD CONSTRAINT FK_84D0BCA2F717C8DA FOREIGN KEY (group_name_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE league_positions ADD CONSTRAINT FK_84D0BCA233D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)');
        $this->addSql('ALTER TABLE league_positions ADD CONSTRAINT FK_84D0BCA2A6005CA0 FOREIGN KEY (round_id) REFERENCES round (id)');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE round ADD CONSTRAINT FK_C5EEEA3433D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)');
        $this->addSql('ALTER TABLE round_elimination ADD CONSTRAINT FK_E34172CA6005CA0 FOREIGN KEY (round_id) REFERENCES round (id)');
        $this->addSql('ALTER TABLE round_league ADD CONSTRAINT FK_FA03FAB4A6005CA0 FOREIGN KEY (round_id) REFERENCES round (id)');
        $this->addSql('ALTER TABLE tournament ADD CONSTRAINT FK_BD5FB8D9FA7FD7EB FOREIGN KEY (champion_id) REFERENCES team (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE administrator_bet DROP FOREIGN KEY FK_A2B7E102D871DC26');
        $this->addSql('ALTER TABLE bet_date DROP FOREIGN KEY FK_8F48E446D871DC26');
        $this->addSql('ALTER TABLE bet_extra DROP FOREIGN KEY FK_6275A731D871DC26');
        $this->addSql('ALTER TABLE betting_positions DROP FOREIGN KEY FK_1BAE2578D871DC26');
        $this->addSql('ALTER TABLE bet_game DROP FOREIGN KEY FK_6FDE2B05043CB87');
        $this->addSql('ALTER TABLE bet_extra_user DROP FOREIGN KEY FK_99BAB54682C1E568');
        $this->addSql('ALTER TABLE bet_date DROP FOREIGN KEY FK_8F48E446B897366B');
        $this->addSql('ALTER TABLE date_group DROP FOREIGN KEY FK_D730AC49B897366B');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C3D61B0E1');
        $this->addSql('ALTER TABLE bet_game DROP FOREIGN KEY FK_6FDE2B0E48FD905');
        $this->addSql('ALTER TABLE elimination_match_data DROP FOREIGN KEY FK_46779A47E48FD905');
        $this->addSql('ALTER TABLE date_group DROP FOREIGN KEY FK_D730AC49F717C8DA');
        $this->addSql('ALTER TABLE league_positions DROP FOREIGN KEY FK_84D0BCA2F717C8DA');
        $this->addSql('ALTER TABLE date DROP FOREIGN KEY FK_AA9E377AA6005CA0');
        $this->addSql('ALTER TABLE league_positions DROP FOREIGN KEY FK_84D0BCA2A6005CA0');
        $this->addSql('ALTER TABLE round_elimination DROP FOREIGN KEY FK_E34172CA6005CA0');
        $this->addSql('ALTER TABLE round_league DROP FOREIGN KEY FK_FA03FAB4A6005CA0');
        $this->addSql('ALTER TABLE date_group DROP FOREIGN KEY FK_D730AC493C809D0');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C5D5A2101');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C8DEF089F');
        $this->addSql('ALTER TABLE league_positions DROP FOREIGN KEY FK_84D0BCA2296CD8AE');
        $this->addSql('ALTER TABLE tournament DROP FOREIGN KEY FK_BD5FB8D9FA7FD7EB');
        $this->addSql('ALTER TABLE administrator_tournament DROP FOREIGN KEY FK_27475C9933D1A3E7');
        $this->addSql('ALTER TABLE bet DROP FOREIGN KEY FK_FBF0EC9B33D1A3E7');
        $this->addSql('ALTER TABLE date DROP FOREIGN KEY FK_AA9E377A33D1A3E7');
        $this->addSql('ALTER TABLE elimination_match_data DROP FOREIGN KEY FK_46779A4733D1A3E7');
        $this->addSql('ALTER TABLE league_positions DROP FOREIGN KEY FK_84D0BCA233D1A3E7');
        $this->addSql('ALTER TABLE round DROP FOREIGN KEY FK_C5EEEA3433D1A3E7');
        $this->addSql('ALTER TABLE administrator_bet DROP FOREIGN KEY FK_A2B7E102A76ED395');
        $this->addSql('ALTER TABLE administrator_tournament DROP FOREIGN KEY FK_27475C99A76ED395');
        $this->addSql('ALTER TABLE bet DROP FOREIGN KEY FK_FBF0EC9BFA7FD7EB');
        $this->addSql('ALTER TABLE bet_date DROP FOREIGN KEY FK_8F48E446A76ED395');
        $this->addSql('ALTER TABLE bet_extra_user DROP FOREIGN KEY FK_99BAB546F6F50196');
        $this->addSql('ALTER TABLE betting_positions DROP FOREIGN KEY FK_1BAE25789D1C3019');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('DROP TABLE administrator_bet');
        $this->addSql('DROP TABLE administrator_tournament');
        $this->addSql('DROP TABLE bet');
        $this->addSql('DROP TABLE bet_date');
        $this->addSql('DROP TABLE bet_extra');
        $this->addSql('DROP TABLE bet_extra_user');
        $this->addSql('DROP TABLE bet_game');
        $this->addSql('DROP TABLE betting_positions');
        $this->addSql('DROP TABLE date');
        $this->addSql('DROP TABLE date_group');
        $this->addSql('DROP TABLE elimination_match_data');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE `group`');
        $this->addSql('DROP TABLE league_positions');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE round');
        $this->addSql('DROP TABLE round_elimination');
        $this->addSql('DROP TABLE round_league');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE tournament');
        $this->addSql('DROP TABLE user');
    }
}
