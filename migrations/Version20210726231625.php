<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210726231625 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE date (id INT AUTO_INCREMENT NOT NULL, tournament_id INT NOT NULL, number INT NOT NULL, name VARCHAR(20) NOT NULL, start_date DATETIME DEFAULT NULL, type INT NOT NULL, INDEX IDX_AA9E377A33D1A3E7 (tournament_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE date_group (id INT AUTO_INCREMENT NOT NULL, date_id INT NOT NULL, group_name_id INT NOT NULL, free_team_id INT DEFAULT NULL, INDEX IDX_D730AC49B897366B (date_id), INDEX IDX_D730AC49F717C8DA (group_name_id), INDEX IDX_D730AC493C809D0 (free_team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `group` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(60) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE date ADD CONSTRAINT FK_AA9E377A33D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)');
        $this->addSql('ALTER TABLE date_group ADD CONSTRAINT FK_D730AC49B897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE date_group ADD CONSTRAINT FK_D730AC49F717C8DA FOREIGN KEY (group_name_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE date_group ADD CONSTRAINT FK_D730AC493C809D0 FOREIGN KEY (free_team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE round_league CHANGE amount_of_teams_by_groups amount_of_teams_by_groups INT DEFAULT NULL, CHANGE return_of_the_group return_of_the_group VARCHAR(30) DEFAULT NULL, CHANGE classified_by_group classified_by_group INT DEFAULT NULL, CHANGE additional_classifieds additional_classifieds INT DEFAULT NULL, CHANGE type_classifieds type_classifieds VARCHAR(25) DEFAULT NULL, CHANGE additional_date additional_date INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE date_group DROP FOREIGN KEY FK_D730AC49B897366B');
        $this->addSql('ALTER TABLE date_group DROP FOREIGN KEY FK_D730AC49F717C8DA');
        $this->addSql('ALTER TABLE date_group DROP FOREIGN KEY FK_D730AC493C809D0');
        $this->addSql('DROP TABLE date');
        $this->addSql('DROP TABLE date_group');
        $this->addSql('DROP TABLE `group`');
        $this->addSql('DROP TABLE team');
        $this->addSql('ALTER TABLE round_league CHANGE amount_of_teams_by_groups amount_of_teams_by_groups INT DEFAULT NULL, CHANGE return_of_the_group return_of_the_group VARCHAR(30) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE classified_by_group classified_by_group INT DEFAULT NULL, CHANGE additional_classifieds additional_classifieds INT DEFAULT NULL, CHANGE type_classifieds type_classifieds VARCHAR(25) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE additional_date additional_date INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
