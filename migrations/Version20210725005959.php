<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210725005959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tournament DROP FOREIGN KEY FK_BD5FB8D937F5A13C');
        $this->addSql('CREATE TABLE round (id INT AUTO_INCREMENT NOT NULL, relation_id INT NOT NULL, type VARCHAR(15) NOT NULL, INDEX IDX_C5EEEA343256915B (relation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE round ADD CONSTRAINT FK_C5EEEA343256915B FOREIGN KEY (relation_id) REFERENCES tournament (id)');
        $this->addSql('DROP TABLE data');
        $this->addSql('DROP INDEX UNIQ_BD5FB8D937F5A13C ON tournament');
        $this->addSql('ALTER TABLE tournament DROP data_id');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE data (id INT AUTO_INCREMENT NOT NULL, number_of_teams INT NOT NULL, points_for_games_won INT NOT NULL, points_for_tied_games INT NOT NULL, points_for_games_lost INT NOT NULL, number_of_group_rounds INT NOT NULL, type INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE round');
        $this->addSql('ALTER TABLE tournament ADD data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament ADD CONSTRAINT FK_BD5FB8D937F5A13C FOREIGN KEY (data_id) REFERENCES data (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BD5FB8D937F5A13C ON tournament (data_id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
