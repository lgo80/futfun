<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210727050614 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, local_id INT NOT NULL, away_id INT NOT NULL, date_group_id INT NOT NULL, number INT NOT NULL, local_value INT DEFAULT NULL, away_value INT DEFAULT NULL, local_definition_value INT DEFAULT NULL, away_definition_value INT DEFAULT NULL, date_match_at DATETIME DEFAULT NULL, INDEX IDX_232B318C5D5A2101 (local_id), INDEX IDX_232B318C8DEF089F (away_id), INDEX IDX_232B318C3D61B0E1 (date_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C5D5A2101 FOREIGN KEY (local_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C8DEF089F FOREIGN KEY (away_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C3D61B0E1 FOREIGN KEY (date_group_id) REFERENCES date_group (id)');
        $this->addSql('ALTER TABLE date CHANGE start_date start_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE date_group CHANGE free_team_id free_team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_league CHANGE amount_of_teams_by_groups amount_of_teams_by_groups INT DEFAULT NULL, CHANGE return_of_the_group return_of_the_group VARCHAR(30) DEFAULT NULL, CHANGE classified_by_group classified_by_group INT DEFAULT NULL, CHANGE additional_classifieds additional_classifieds INT DEFAULT NULL, CHANGE type_classifieds type_classifieds VARCHAR(25) DEFAULT NULL, CHANGE additional_date additional_date INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE game');
        $this->addSql('ALTER TABLE date CHANGE start_date start_date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE date_group CHANGE free_team_id free_team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_league CHANGE amount_of_teams_by_groups amount_of_teams_by_groups INT DEFAULT NULL, CHANGE return_of_the_group return_of_the_group VARCHAR(30) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE classified_by_group classified_by_group INT DEFAULT NULL, CHANGE additional_classifieds additional_classifieds INT DEFAULT NULL, CHANGE type_classifieds type_classifieds VARCHAR(25) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE additional_date additional_date INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
