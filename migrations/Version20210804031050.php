<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210804031050 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bet_game (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, bet_id INT NOT NULL, local_result INT NOT NULL, away_result INT NOT NULL, points INT NOT NULL, INDEX IDX_6FDE2B0E48FD905 (game_id), INDEX IDX_6FDE2B0D871DC26 (bet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE betting_positions (id INT AUTO_INCREMENT NOT NULL, bet_id INT NOT NULL, participant_id INT NOT NULL, points INT NOT NULL, results_embodies_exacts INT NOT NULL, results_embodies_only INT NOT NULL, results_failed INT NOT NULL, points_extras INT NOT NULL, is_active TINYINT(1) NOT NULL, number_of_dates_won INT NOT NULL, INDEX IDX_1BAE2578D871DC26 (bet_id), INDEX IDX_1BAE25789D1C3019 (participant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bet_game ADD CONSTRAINT FK_6FDE2B0E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE bet_game ADD CONSTRAINT FK_6FDE2B0D871DC26 FOREIGN KEY (bet_id) REFERENCES bet (id)');
        $this->addSql('ALTER TABLE betting_positions ADD CONSTRAINT FK_1BAE2578D871DC26 FOREIGN KEY (bet_id) REFERENCES bet (id)');
        $this->addSql('ALTER TABLE betting_positions ADD CONSTRAINT FK_1BAE25789D1C3019 FOREIGN KEY (participant_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bet CHANGE champion_id champion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE date CHANGE start_date start_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE date_group CHANGE free_team_id free_team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game CHANGE local_value local_value INT DEFAULT NULL, CHANGE away_value away_value INT DEFAULT NULL, CHANGE local_definition_value local_definition_value INT DEFAULT NULL, CHANGE away_definition_value away_definition_value INT DEFAULT NULL, CHANGE date_match_at date_match_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE league_positions CHANGE group_name_id group_name_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_elimination CHANGE beginning_of_elimination beginning_of_elimination INT DEFAULT NULL, CHANGE is_away_goal is_away_goal TINYINT(1) DEFAULT NULL, CHANGE is_round_trip_end is_round_trip_end TINYINT(1) DEFAULT NULL, CHANGE number_of_eliminations number_of_eliminations INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_league CHANGE amount_of_teams_by_groups amount_of_teams_by_groups INT DEFAULT NULL, CHANGE return_of_the_group return_of_the_group INT DEFAULT NULL, CHANGE classified_by_group classified_by_group INT DEFAULT NULL, CHANGE additional_classifieds additional_classifieds INT DEFAULT NULL, CHANGE type_classifieds type_classifieds VARCHAR(25) DEFAULT NULL, CHANGE additional_date additional_date INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament CHANGE champion_id champion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bet_game');
        $this->addSql('DROP TABLE betting_positions');
        $this->addSql('ALTER TABLE bet CHANGE champion_id champion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE date CHANGE start_date start_date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE date_group CHANGE free_team_id free_team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game CHANGE local_value local_value INT DEFAULT NULL, CHANGE away_value away_value INT DEFAULT NULL, CHANGE local_definition_value local_definition_value INT DEFAULT NULL, CHANGE away_definition_value away_definition_value INT DEFAULT NULL, CHANGE date_match_at date_match_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE league_positions CHANGE group_name_id group_name_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_elimination CHANGE beginning_of_elimination beginning_of_elimination INT DEFAULT NULL, CHANGE is_away_goal is_away_goal TINYINT(1) DEFAULT \'NULL\', CHANGE is_round_trip_end is_round_trip_end TINYINT(1) DEFAULT \'NULL\', CHANGE number_of_eliminations number_of_eliminations INT DEFAULT NULL');
        $this->addSql('ALTER TABLE round_league CHANGE amount_of_teams_by_groups amount_of_teams_by_groups INT DEFAULT NULL, CHANGE return_of_the_group return_of_the_group INT DEFAULT NULL, CHANGE classified_by_group classified_by_group INT DEFAULT NULL, CHANGE additional_classifieds additional_classifieds INT DEFAULT NULL, CHANGE type_classifieds type_classifieds VARCHAR(25) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE additional_date additional_date INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament CHANGE champion_id champion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
