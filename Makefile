SHELL := /bin/bash

tests:
	php bin/console --env=test doctrine:database:drop --force || true
	php bin/console --env=test doctrine:database:create
	php bin/console --env=test doctrine:schema:create
	php bin/console --env=test doctrine:fixtures:load -n
	php ./vendor/bin/phpunit
.PHONY: tests